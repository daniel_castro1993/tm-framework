STM := ~/projs/atomic_tm/include

CC       := gcc
CFLAGS   += -std=c++11 -g -w -pthread -fpermissive
CFLAGS   += -O0
CFLAGS   += -I $(LIB) -I $(STM)
CPP      := gcc
CPPFLAGS += $(CFLAGS)
LD       := g++
LIBS     += -lpthread

LIB := ../lib
