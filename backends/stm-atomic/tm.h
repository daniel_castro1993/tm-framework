#ifndef TM_H
#define TM_H 1

#  include <stdio.h>

#define MAIN(argc, argv)              int main (int argc, char** argv)
#define MAIN_RETURN(val)              return val

#define GOTO_SIM()                    /* nothing */
#define GOTO_REAL()                   /* nothing */
#define IS_IN_SIM()                   (0)

#define SIM_GET_NUM_CPU(var)          /* nothing */

#define TM_PRINTF                     printf
#define TM_PRINT0                     printf
#define TM_PRINT1                     printf
#define TM_PRINT2                     printf
#define TM_PRINT3                     printf

#define P_MEMORY_STARTUP(numThread)   /* nothing */
#define P_MEMORY_SHUTDOWN()           /* nothing */


#  include <string.h>
#  include <atomic_tm.h>
#ifndef REDUCED_TM_API
#  include "thread.h"
#endif
#  include <math.h>

#define AL_LOCK(idx)

#define TM_ARG                        /* nothing */
#define TM_ARG_ALONE                  /* nothing */
#define TM_ARGDECL                    /* nothing */
#define TM_ARGDECL_ALONE              /* nothing */
#define TM_CALLABLE                   /* nothing */

#ifdef REDUCED_TM_API
#define SPECIAL_THREAD_ID()         get_tid()
#else
#define SPECIAL_THREAD_ID()         thread_getId()
#endif

#define SETUP_NUMBER_TASKS(p);
#define SETUP_NUMBER_THREADS(p);
#define PRINT_STATS()

#define TM_STARTUP(numThread) ATM_init()
#define TM_SHUTDOWN()             ATM_destroy(); \
    printf(" >>> STATS:\n" \
        "    Nb. Aborts: %llu\n" \
        "   Nb. Commits: %llu\n" \
        "    Nb. rlocks: %llu\n" \
        "    Nb. wlocks: %llu\n" \
        "Nb. req_wlocks: %llu\n" \
        "   Nb. req_sgl: %llu\n", \
        ATM_tot_nb_aborts, ATM_tot_nb_commits, ATM_tot_nb_rlock, \
        ATM_tot_nb_wlock, ATM_tot_nb_req_wlock, ATM_tot_nb_req_sgl)

#define TM_THREAD_ENTER()         ATM_thr_enter()
#define TM_THREAD_EXIT()          ATM_thr_exit()

#define P_MALLOC(size)            malloc(size)
#define P_FREE(ptr)               free(ptr)
#define TM_MALLOC(size)           malloc(size)
#define TM_FREE(ptr)              free(ptr)
#define SLOW_PATH_FREE(ptr)       TM_FREE(ptr)


// TODO: pact version of STAMP does not has ro
#define TM_BEGIN()           TM_BEGIN_EXT(0,/*ro*/0)
#define TM_BEGIN_EXT(b,ro)   ATM_begin()

#define TM_END() ATM_commit()

#define TM_RESTART()                ATM_abort()
#define SLOW_PATH_RESTART()         ATM_abort()

#define TM_EARLY_RELEASE(var)       /* nothing */

// TODO: use __typeof__(var)
#define TM_SHARED_READ(var) ({ \
    __typeof__(var) res; \
    ATM_read(&(var), sizeof(__typeof__(var)), &(res)); \
    res; \
})
#define TM_SHARED_READ_P(var) TM_SHARED_READ(var)
#define TM_SHARED_READ_D(var) TM_SHARED_READ(var)
#define TM_SHARED_READ_F(var) TM_SHARED_READ(var)

#define TM_SHARED_WRITE(var, val) ({ \
    __typeof__(var) wbuf = val; \
    ATM_write(&(var), sizeof(__typeof__(var)), &(wbuf)); \
    val; \
})

// var is the pointer
#define TM_SHARED_WRITE_P(var, val) ({ \
    long long wbuf = (long long)val; \
    ATM_write(&(var), sizeof(__typeof__(var)), &(wbuf)); \
    val; \
})
#define TM_SHARED_WRITE_F(var, val) TM_SHARED_WRITE(var, val)
#define TM_SHARED_WRITE_D(var, val) TM_SHARED_WRITE(var, val)

#define SLOW_PATH_SHARED_READ(var)           TM_SHARED_READ(var)
#define SLOW_PATH_SHARED_READ_P(var)         TM_SHARED_READ_P(var)
#define SLOW_PATH_SHARED_READ_F(var)         TM_SHARED_READ_F(var)
#define SLOW_PATH_SHARED_READ_D(var)         TM_SHARED_READ_D(var)
#define SLOW_PATH_SHARED_WRITE(var, val)     TM_SHARED_WRITE(var, val)
#define SLOW_PATH_SHARED_WRITE_P(var, val)   TM_SHARED_WRITE_P(var, val)
#define SLOW_PATH_SHARED_WRITE_F(var, val)   TM_SHARED_WRITE_F(var, val)
#define SLOW_PATH_SHARED_WRITE_D(var, val)   TM_SHARED_WRITE_D(var, val)

// TODO: need to roll-back the local modifications (also need TM_LOCAL_READ)
// TM_SHARED_WRITE(var, val)
#define TM_LOCAL_WRITE(var, val)      ({var = val; var;})
#define TM_LOCAL_WRITE_P(var, val)    ({var = val; var;})
#define TM_LOCAL_WRITE_D(var, val)    ({var = val; var;})

#endif /* TM_H */
