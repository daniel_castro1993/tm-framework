CC       := gcc
CFLAGS   += -std=c++11 -g -w -pthread -mhtm -fpermissive
CFLAGS   += -O2
CFLAGS   += -I$(LIB) -I../../../rapl-power/ -I ../../../stms/norec/
CPP      := gcc
CPPFLAGS += $(CFLAGS)
LD       := gcc
LIBS     += -lpthread

# Remove these files when doing clean
OUTPUT +=

LIB := ../lib
