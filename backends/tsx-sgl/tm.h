#ifndef TM_H
#define TM_H 1

#  include <stdio.h>

#ifndef REDUCED_TM_API

#  define MAIN(argc, argv)              int main (int argc, char** argv)
#  define MAIN_RETURN(val)              return val

#  define GOTO_SIM()                    /* nothing */
#  define GOTO_REAL()                   /* nothing */
#  define IS_IN_SIM()                   (0)

#  define SIM_GET_NUM_CPU(var)          /* nothing */

#  define TM_PRINTF                     printf
#  define TM_PRINT0                     printf
#  define TM_PRINT1                     printf
#  define TM_PRINT2                     printf
#  define TM_PRINT3                     printf

#  define P_MEMORY_STARTUP(numThread)   /* nothing */
#  define P_MEMORY_SHUTDOWN()           /* nothing */

#  include <assert.h>
#  include "thread.h"
#  include "types.h"
#  include "thread.h"
#  include <math.h>

#  define TM_ARG                        /* nothing */
#  define TM_ARG_ALONE                  /* nothing */
#  define TM_ARGDECL                    /* nothing */
#  define TM_ARGDECL_ALONE              /* nothing */
#  define TM_CALLABLE                   /* nothing */

#  define TM_BEGIN_WAIVER()
#  define TM_END_WAIVER()

#  define P_MALLOC(size)                malloc(size)
#  define P_FREE(ptr)                   free(ptr)
#  define TM_MALLOC(size)               malloc(size)
#  define TM_FREE(ptr)            free(ptr)
#  define SLOW_PATH_FREE(ptr)             free(ptr)

# define SETUP_NUMBER_TASKS(n)
# define SETUP_NUMBER_THREADS(n)
# define PRINT_STATS()
# define AL_LOCK(idx)

#endif

#ifdef REDUCED_TM_API
#    define SPECIAL_THREAD_ID()         get_tid()
#else
#    define SPECIAL_THREAD_ID()         thread_getId()
#endif

#  include <htm_retry_template.h>

#  define TM_STARTUP(numThread) HTM_init(numThread)
#  define TM_SHUTDOWN() HTM_exit()

#  define TM_THREAD_ENTER() HTM_thr_init();
#  define TM_THREAD_EXIT() HTM_thr_exit()

# define TM_BEGIN(b) TM_BEGIN_EXT(b,0)
# define TM_BEGIN_EXT(b,ro) HTM_SGL_begin()


# define TM_END() HTM_SGL_commit()

#    define TM_BEGIN_RO()                 TM_BEGIN(0)
#    define TM_RESTART()                  HTM_abort();
#    define TM_EARLY_RELEASE(var)

# define TM_RESTART() HTM_abort();
# define TM_SHARED_READ(var) (var)
# define TM_SHARED_READ_P(var) (var)
# define TM_SHARED_READ_D(var) (var)
# define TM_SHARED_WRITE(var, val) ({var = val; var;})
# define TM_SHARED_WRITE_P(var, val) ({var = val; var;})
# define TM_SHARED_WRITE_D(var, val) ({var = val; var;})

# define SLOW_PATH_RESTART() TM_RESTART()
# define SLOW_PATH_SHARED_READ(var)           TM_SHARED_READ(var)
# define SLOW_PATH_SHARED_READ_P(var)         TM_SHARED_READ_P(var)
# define SLOW_PATH_SHARED_READ_F(var)         TM_SHARED_READ_D(var)
# define SLOW_PATH_SHARED_READ_D(var)         TM_SHARED_READ_D(var)
# define SLOW_PATH_SHARED_WRITE(var, val)     TM_SHARED_WRITE(var, val)
# define SLOW_PATH_SHARED_WRITE_P(var, val)   TM_SHARED_WRITE_P(var, val)
# define SLOW_PATH_SHARED_WRITE_D(var, val)   TM_SHARED_WRITE_D(var, val)

#  define TM_LOCAL_WRITE(var, val)      ({var = val; var;})
#  define TM_LOCAL_WRITE_P(var, val)    ({var = val; var;})
#  define TM_LOCAL_WRITE_D(var, val)    ({var = val; var;})


#endif
