STM := ../../../stms/norec

CC       := gcc
CFLAGS   += -std=c++11 -g -w -pthread -fpermissive
CFLAGS   += -O2
CFLAGS   += -I$(LIB) -I$(STM)
CPP      := gcc
CPPFLAGS += $(CFLAGS)
LD       := gcc
LIBS     += -lpthread

LIB := ../lib
