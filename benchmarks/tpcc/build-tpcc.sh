backend=$1 # e.g: herwl
htm_retries=$2 # e.g.: 5
rot_retries=$3 # e.g.: 2

cp ../../backends/$backend/tm.h code/
cp ../../backends/$backend/thread.c code/
cp ../../backends/$backend/thread.h code/
cp ../../backends/$backend/Makefile .
cp ../../backends/$backend/Makefile.common .
cp ../../backends/$backend/Makefile.flags .
cp ../../backends/$backend/Defines.common.mk .

cd code;
rm tpcc

if [[ $backend == htm-sgl-nvm ]] ; then
	PATH_NH=~/projs/nh
	cd $PATH_NH
	make clean && make $MAKEFILE_ARGS
	cd -
fi

if [[ $backend == stm-tinystm ]] ; then
	PATH_TINY=~/projs/tinystm
	cd $PATH_TINY
	make clean
  make $MAKEFILE_ARGS
	cd -
fi

make_command="make -j8 -f Makefile $MAKEFILE_ARGS"
echo " ==========> $make_command"
$make_command
