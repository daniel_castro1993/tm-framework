#ifndef TM_H
#define TM_H 1

#  include <stdio.h>
#  include <stdint.h>
#  include "libnvmhtm.h"

#ifndef REDUCED_TM_API

#  define MAIN(argc, argv)              int main (int argc, char** argv)
#  define MAIN_RETURN(val)              return val

#  define GOTO_SIM()                    /* nothing */
#  define GOTO_REAL()                   /* nothing */
#  define IS_IN_SIM()                   (0)

#  define SIM_GET_NUM_CPU(var)          /* nothing */

#  define TM_PRINTF                     printf
#  define TM_PRINT0                     printf
#  define TM_PRINT1                     printf
#  define TM_PRINT2                     printf
#  define TM_PRINT3                     printf

#  define P_MEMORY_STARTUP(numThread)   /* nothing */
#  define P_MEMORY_SHUTDOWN()           /* nothing */

#  include <assert.h>
#  include "memory.h"
#  include "thread.h"
#  include "types.h"
#  include "thread.h"
#  include <math.h>

#  define TM_ARG                        /* nothing */
#  define TM_ARG_ALONE                  /* nothing */
#  define TM_ARGDECL                    /* nothing */
#  define TM_ARGDECL_ALONE              /* nothing */
#  define TM_CALLABLE                   /* nothing */

#  define TM_BEGIN_WAIVER()
#  define TM_END_WAIVER()

// do not track private memory allocation
#  define P_MALLOC(size)                NVHTM_malloc(size) /* NVHTM_alloc("alloc.dat", size, 0) */
#  define P_FREE(ptr)                   

// TODO: free(ptr)
// the patched benchmarks are broken -> intruder gives me double free or corruption

// NVHTM_alloc("alloc.dat", size, 0)
#  define TM_MALLOC(size)               NVHTM_malloc(size) /* NVHTM_alloc("alloc.dat", size, 0) */
#  define FAST_PATH_FREE(ptr)           
#  define SLOW_PATH_FREE(ptr)           FAST_PATH_FREE(ptr)

# define SETUP_NUMBER_TASKS(n)
# define SETUP_NUMBER_THREADS(n)
# define PRINT_STATS()
# define AL_LOCK(idx)

#endif /* REDUCED_TM_API */

#ifdef REDUCED_TM_API
#    define SPECIAL_THREAD_ID()         get_tid()
#else /* REDUCED_TM_API */
#    define SPECIAL_THREAD_ID()         thread_getId()
#endif /* REDUCED_TM_API */

#ifndef USE_P8
#  include <immintrin.h>
#  include <rtmintrin.h>
#else /* USE_P8 */
#  include <htmxlintrin.h>
#endif /* USE_P8 */

// TODO:

#  define TM_STARTUP(numThread)   NVHTM_init(numThread)
#  define TM_SHUTDOWN()                PRINT_STATS_FILE("THREADS", "%i", NVHTM_nb_thrs()); NVHTM_shutdown()

#  define TM_THREAD_ENTER()            NVHTM_thr_init()
#  define TM_THREAD_EXIT()             NVHTM_thr_exit()

// leave local_exec_mode = 0 to use the FAST_PATH
# define TM_BEGIN(b)                   NVHTM_start_tx()

# define TM_BEGIN_EXT(b,a)             TM_BEGIN(b)

# define TM_END()                      NVHTM_commit_tx()

#    define TM_BEGIN_RO()              TM_BEGIN(0)
#    define TM_RESTART()               NVHTM_abort_tx()
#    define TM_EARLY_RELEASE(var)

// TODO: check incompatible types
# define FAST_PATH_RESTART()          NVHTM_abort_tx()
# define FAST_PATH_SHARED_READ(var)   (var)
# define FAST_PATH_SHARED_READ_P(var) (var)
# define FAST_PATH_SHARED_READ_D(var) (var)

// int
# define FAST_PATH_SHARED_WRITE(var, val)   ({ NVHTM_write(&(var), val); var;})

// pointer
# define FAST_PATH_SHARED_WRITE_P(var, val) ({ NVHTM_write_P(&(var), val); var;})

// double
# define FAST_PATH_SHARED_WRITE_D(var, val) ({ NVHTM_write_D(&(var), val); var;})

// not needed
# define SLOW_PATH_RESTART()                  FAST_PATH_RESTART()
# define SLOW_PATH_SHARED_READ(var)           FAST_PATH_SHARED_READ(var)
# define SLOW_PATH_SHARED_READ_P(var)         FAST_PATH_SHARED_READ_P(var)
# define SLOW_PATH_SHARED_READ_F(var)         FAST_PATH_SHARED_READ_D(var)
# define SLOW_PATH_SHARED_READ_D(var)         FAST_PATH_SHARED_READ_D(var)
# define SLOW_PATH_SHARED_WRITE(var, val)     FAST_PATH_SHARED_WRITE(var, val)
# define SLOW_PATH_SHARED_WRITE_P(var, val)   FAST_PATH_SHARED_WRITE_P(var, val)
# define SLOW_PATH_SHARED_WRITE_D(var, val)   FAST_PATH_SHARED_WRITE_D(var, val)

// local is not tracked
#  define TM_LOCAL_WRITE(var, val)      ({var = val; var;})
#  define TM_LOCAL_WRITE_P(var, val)    ({var = val; var;})
#  define TM_LOCAL_WRITE_D(var, val)    ({var = val; var;})

#ifndef STATS_FILE
#define STATS_FILE "stats_file.txt"
#endif

// statistics
#define PRINT_STATS_FILE(header, pattern, ...) ({ \
	FILE *gp_fp = fopen(STATS_FILE, "a"); \
	printf("see results in " STATS_FILE " ptr=%p\n", gp_fp); \
	fflush(stdout); \
	if (ftell(gp_fp) < 8) { \
		fprintf(gp_fp, "#" header "\tTIME\tNB_TXS\tNB_HTM_COMMITS\tNB_SGL_COMMITS\tABORTS" \
		"\tCAPACITY\tCONFLICTS\n"); \
	} \
	fprintf(gp_fp, pattern, __VA_ARGS__); \
	fprintf(gp_fp, "\t%f\t%i\t%i\t%i\t%i\t%i\t%i\n", NVHTM_elapsed_time(), \
	NVHTM_nb_transactions(), NVHTM_nb_htm_commits(), \
	NVHTM_nb_sgl_commits(), NVHTM_nb_aborts_aborts(), \
	NVHTM_nb_aborts_capacity(), NVHTM_nb_aborts_conflicts()); \
	fclose(gp_fp); \
})
	
#endif

// TODO:
#ifndef __RDTSC_H_DEFINED__
#define __RDTSC_H_DEFINED__


#if defined(__i386__)

static __inline__ unsigned long long rdtsc(void) {
	unsigned long long int x;
	__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
	return x;
}
#elif defined(__x86_64__)

static __inline__ unsigned long long rdtsc(void) {
	unsigned hi, lo;
	__asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
	return ( (unsigned long long) lo) | ( ((unsigned long long) hi) << 32 );
}

#elif defined(__powerpc__)

static __inline__ unsigned long long rdtsc(void) {
	unsigned long long int result = 0;
	unsigned long int upper, lower, tmp;
	__asm__ volatile(
			"0:                  \n"
			"\tmftbu   %0           \n"
			"\tmftb    %1           \n"
			"\tmftbu   %2           \n"
			"\tcmpw    %2,%0        \n"
			"\tbne     0b         \n"
			: "=r"(upper), "=r"(lower), "=r"(tmp)
			);
	result = upper;
	result = result << 32;
	result = result | lower;

	return (result);
}

#else

#error "No tick counter is available!"

#endif


/*  $RCSfile:  $   $Author: kazutomo $
 *  $Revision: 1.6 $  $Date: 2005/04/13 18:49:58 $
 */

#endif


