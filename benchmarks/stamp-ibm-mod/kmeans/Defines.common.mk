# ==============================================================================
#
# Defines.common.mk
#
# ==============================================================================
# Copyright (c) IBM Corp. 2014, and others.


hostname := $(shell hostname)

CFLAGS += -DUSE_TLH
CFLAGS += -DOUTPUT_TO_STDOUT

ifeq ($(enable_IBM_optimizations),yes)
CFLAGS += -DALIGNED_ALLOC_MEMORY
#CFLAGS += -DLARGE_ALIGNMENT
endif

PROG := kmeans

SRCS += \
	cluster.c \
	common.c \
	kmeans.c \
	normal.c \
	$(LIB)/mt19937ar.c \
	$(LIB)/random.c \
	$(LIB)/thread.c \
	$(LIB)/memory.c
#
OBJS := ${SRCS:.c=.o}

RUNPARAMSLOW := -m40 -n40 -t0.00001 -i inputs/random-n65536-d32-c16.txt
RUNPARAMSHIGH := -m15 -n15 -t0.00001 -i inputs/random-n65536-d32-c16.txt

# ==============================================================================
#
# End of Defines.common.mk
#
# ==============================================================================
