# ==============================================================================
#
# Defines.common.mk
#
# ==============================================================================
# Copyright (c) IBM Corp. 2014, and others.


hostname := $(shell hostname)

CFLAGS += -DUSE_TLH
CFLAGS += -DLIST_NO_DUPLICATES

ifeq ($(enable_IBM_optimizations),yes)
CFLAGS += -DMAP_USE_CONCUREENT_HASHTABLE -DHASHTABLE_SIZE_FIELD -DHASHTABLE_RESIZABLE
else
CFLAGS += -DMAP_USE_RBTREE
endif

PROG := vacation

SRCS += \
	client.c \
	customer.c \
	manager.c \
	reservation.c \
	vacation.c \
	$(LIB)/list.c \
	$(LIB)/pair.c \
	$(LIB)/mt19937ar.c \
	$(LIB)/random.c \
	$(LIB)/rbtree.c \
	$(LIB)/hashtable.c \
	$(LIB)/conc_hashtable.c \
	$(LIB)/thread.c \
	$(LIB)/memory.c
#
OBJS := ${SRCS:.c=.o}

#RUNPARAMSLOW := -n2 -q90 -u98 -r16384 -t4096
RUNPARAMSLOW := -n2 -q90 -u98 -r1048576 -t4194304
#RUNPARAMSHIGH := -n4 -q60 -u90 -r16384 -t4096
RUNPARAMSHIGH := -n4 -q60 -u90 -r1048576 -t4194304

# ==============================================================================
#
# End of Defines.common.mk
#
# ==============================================================================
