set term postscript color eps enhanced 22 size 15cm,8.5cm
# ARG1 - PATH_TO_DATA
# ARG2 - BENCHMARK NAME
# ARG3 - BENCHMARK
set output sprintf("|ps2pdf -dEPSCrop - test_%s_X.pdf", ARG2)

set grid ytics
set grid xtics

set format y "%.1t{/*0.8 x10}^{%T}"
#set logscale x 2
set ytics offset 0.8,0 font ",20"
set mxtics
set xtics rotate by -45 left font ",30" offset -0.8,0.3

set lmargin 9.2
set rmargin 3.3
set tmargin 1
set bmargin 4.5

### TODO:

set key font ",16" outside right Left reverse width 4
#set nokey
set ylabel "Throughput (TXs/s)" font ",34" offset 1.4,-0.0
set xlabel "Number of Threads" font ",34" offset -0.0,-0.5
#set title font ",18" sprintf("%s", ARG3)

STM  =sprintf("%s/test_%s_STM.txt", ARG1, ARG2)
PSTM =sprintf("%s/test_%s_PSTM.txt", ARG1, ARG2)
HTM  =sprintf("%s/test_%s_HTM.txt", ARG1, ARG2)
PHTM =sprintf("%s/test_%s_PHTM.txt", ARG1, ARG2)
NVHTM_F_LC=sprintf("%s/test_%s_NVHTM_F_LC.txt", ARG1, ARG2)
NVHTM_B=sprintf("%s/test_%s_NVHTM_B.txt", ARG1, ARG2)
NVHTM_B_10=sprintf("%s/test_%s_NVHTM_B_10.txt", ARG1, ARG2)
NVHTM_F=sprintf("%s/test_%s_NVHTM_F.txt", ARG1, ARG2)
NVHTM_W=sprintf("%s/test_%s_NVHTM_W.txt", ARG1, ARG2)

plot \
STM     u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
STM     u 1:($6)                             notitle      w lines      lc 6 lw 4, \
HTM     u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
HTM     u 1:($8)                             notitle      w lines      lc 1 lw 4, \
PSTM    u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
PSTM    u 1:($6)                             notitle      w lines      lc 7 lw 4, \
PHTM    u ($1 + 0.05):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 2 lw 4 ps 2, \
PHTM    u 1:($8)                             notitle      w lines      lc 2 lw 4, \
NVHTM_B u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
NVHTM_B u 1:($8)                             notitle      w lines      lc 4 lw 4, \
NVHTM_B_10 u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM_{SL}' w yerrorbars lc 4 lw 4 ps 2, \
NVHTM_B_10 u 1:($8)                         notitle     w lines dt "." lc 4 lw 4, \
#NVHTM_W u ($1 + 0.00):($8):($8-$16):($8+$16) title 'NV-HTM_W' w yerrorbars lc 5, \
#NVHTM_W u 1:($8)                             notitle        w lines      lc 5, \
