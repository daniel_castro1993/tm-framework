set term postscript color eps enhanced 22 size 15cm,8.5cm
# ARG1 - PATH_TO_DATA
# ARG2 - BENCHMARK NAME
# ARG3 - BENCHMARK
set output sprintf("|ps2pdf -dEPSCrop - test_%s_PA.pdf", ARG2)

set grid ytics
set xtics norangelimit font ",12pt"
set xtics border in scale 0,0 offset -0.2,0.3 nomirror rotate by -45  autojustify
set ytics offset 0.4,0 font ",22"

set yrange [0:1]

set style fill pattern 1 border lt -1
set style histogram rowstacked title font ",18" \
    textcolor lt -1 offset character 0.5, 0.1
set style data histograms

set key outside horizontal Left top left reverse samplen 1 width 1 maxrows 1 maxcols 12
#set nokey
#set title font ",18" sprintf("%s", ARG3)

set ylabel "Probability of Abort" font ",34" offset 1.4,-0.0
set xlabel "\n{/*2 Number of Threads}" offset 0,0 font ",16"

### TODO:

STM  =sprintf("%s/AVG_STM.txt", ARG1, ARG2)
PSTM =sprintf("%s/AVG_PSTM.txt", ARG1, ARG2)
HTM  =sprintf("%s/test_%s_HTM.txt", ARG1, ARG2)
PHTM =sprintf("%s/test_%s_PHTM.txt", ARG1, ARG2)
NVHTM_F_LC=sprintf("%s/test_%s_NVHTM_F_LC.txt", ARG1, ARG2)
NVHTM_B=sprintf("%s/test_%s_NVHTM_B.txt", ARG1, ARG2)
NVHTM_B_10=sprintf("%s/test_%s_NVHTM_B_10.txt", ARG1, ARG2)
NVHTM_F=sprintf("%s/test_%s_NVHTM_F.txt", ARG1, ARG2)
NVHTM_W=sprintf("%s/test_%s_NVHTM_W.txt", ARG1, ARG2)

# set yrange [ 0.00000 : 900000. ] noreverse nowriteback
#
plot \
    newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   using 3:xtic(1) notitle lw 2 lc 1, \
        ''    u     4     notitle lw 2 lc 2, \
        ''    u     7     notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
        PHTM  using 3:xtic(1) title 'CONFLICT'  lw 2 lc 1, \
        ''    u     4         title 'CAPACITY'  lw 2 lc 2, \
        ''    u     6         title 'UNSPECIFIED' lw 2 lc 3, \
        ''    u     5         title 'EXPLICIT'  lw 2 lc 7, \
	newhistogram 'NV-HTM' lt 1 fs pattern 1, \
        NVHTM_B using 3:xtic(1) notitle lw 2 lc 1, \
        ''      u     4         notitle lw 2 lc 2, \
        ''      u     6         notitle lw 2 lc 3, \
        ''      u     5         notitle lw 2 lc 7, \
	newhistogram 'NV-HTM_{SL}' lt 1 fs pattern 1, \
        NVHTM_B_10 using 3:xtic(1) notitle lw 2 lc 1, \
        ''      u     4         notitle lw 2 lc 2, \
        ''      u     6         notitle lw 2 lc 3, \
        ''      u     5         notitle lw 2 lc 7, \
  newhistogram 'STM' lt 1 fs pattern 1, \
      STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram 'PSTM' lt 1 fs pattern 1, \
      PSTM using 2:xtic(1) notitle lw 2 lc 1, \
      #newhistogram 'NVHTM_W' lt 1 fs pattern 1, \
      #NVHTM_W using 3:xtic(1) notitle, \
      #''      u     4         notitle, \
      #''      u     6         notitle, \
      #''      u     5         notitle, \
#
