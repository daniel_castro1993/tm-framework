set term postscript color eps enhanced 22 size 20cm,5cm
# ARG1 - PATH_TO_DATA
# ARG2 - BENCHMARK NAME
# ARG3 - BENCHMARK
set output sprintf("|ps2pdf -dEPSCrop - STAMP_PA_MULT.pdf", ARG2)

#set grid ytics
set xtics norangelimit font ",12pt"
set xtics border in scale 0,0 offset -0.2,0.3 nomirror rotate by -45  autojustify
set ytics offset 0.4,0 font ",22"

set yrange [0:1]

set style fill pattern 1 border lt -1
set style histogram rowstacked title font ",16" \
    textcolor lt -1 offset character 0.0,0.4
set style data histograms

#set key outside horizontal Left top left reverse samplen 1 width 1 maxrows 1 maxcols 12
set nokey
#set title font ",18" sprintf("%s", ARG3)

set ylabel "Probability of Abort" font ",26" offset 1.6,0.0

### TODO:

REMOVE_LINES="<(sed '3d;6d;7d;8d;10d;11d;13d;14d'"

GENOME_STM  =sprintf("%s %s/test_GENOME_STM.txt)",     REMOVE_LINES, ARG1)
GENOME_PSTM =sprintf("%s %s/test_GENOME_PSTM.txt)",    REMOVE_LINES, ARG1)
GENOME_HTM  =sprintf("%s %s/test_GENOME_HTM.txt)",     REMOVE_LINES, ARG1)
GENOME_PHTM =sprintf("%s %s/test_GENOME_PHTM.txt)",    REMOVE_LINES, ARG1)
GENOME_HTPM =sprintf("%s %s/test_GENOME_NVHTM_B.txt)", REMOVE_LINES, ARG1)

INTRUD_STM  =sprintf("%s %s/test_INTRUDER_STM.txt)",     REMOVE_LINES, ARG1)
INTRUD_PSTM =sprintf("%s %s/test_INTRUDER_PSTM.txt)",    REMOVE_LINES, ARG1)
INTRUD_HTM  =sprintf("%s %s/test_INTRUDER_HTM.txt)",     REMOVE_LINES, ARG1)
INTRUD_PHTM =sprintf("%s %s/test_INTRUDER_PHTM.txt)",    REMOVE_LINES, ARG1)
INTRUD_HTPM =sprintf("%s %s/test_INTRUDER_NVHTM_B.txt)", REMOVE_LINES, ARG1)

KMEANS_H_STM  =sprintf("%s %s/test_KMEANS_HIGH_STM.txt)",     REMOVE_LINES, ARG1)
KMEANS_H_PSTM =sprintf("%s %s/test_KMEANS_HIGH_PSTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_H_HTM  =sprintf("%s %s/test_KMEANS_HIGH_HTM.txt)",     REMOVE_LINES, ARG1)
KMEANS_H_PHTM =sprintf("%s %s/test_KMEANS_HIGH_PHTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_H_HTPM =sprintf("%s %s/test_KMEANS_HIGH_NVHTM_B.txt)", REMOVE_LINES, ARG1)
KMEANS_H_HTPMS=sprintf("%s %s/test_KMEANS_HIGH_NVHTM_B_10.txt)", REMOVE_LINES, ARG1)

KMEANS_L_STM  =sprintf("%s %s/test_KMEANS_LOW_STM.txt)",     REMOVE_LINES, ARG1)
KMEANS_L_PSTM =sprintf("%s %s/test_KMEANS_LOW_PSTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_L_HTM  =sprintf("%s %s/test_KMEANS_LOW_HTM.txt)",     REMOVE_LINES, ARG1)
KMEANS_L_PHTM =sprintf("%s %s/test_KMEANS_LOW_PHTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_L_HTPM =sprintf("%s %s/test_KMEANS_LOW_NVHTM_B.txt)", REMOVE_LINES, ARG1)

VACAT_H_STM  =sprintf("%s %s/test_VACATION_HIGH_STM.txt)",     REMOVE_LINES, ARG1)
VACAT_H_PSTM =sprintf("%s %s/test_VACATION_HIGH_PSTM.txt)",    REMOVE_LINES, ARG1)
VACAT_H_HTM  =sprintf("%s %s/test_VACATION_HIGH_HTM.txt)",     REMOVE_LINES, ARG1)
VACAT_H_PHTM =sprintf("%s %s/test_VACATION_HIGH_PHTM.txt)",    REMOVE_LINES, ARG1)
VACAT_H_HTPM =sprintf("%s %s/test_VACATION_HIGH_NVHTM_B.txt)", REMOVE_LINES, ARG1)
VACAT_H_HTPMS=sprintf("%s %s/test_VACATION_HIGH_NVHTM_B_10.txt)", REMOVE_LINES, ARG1)

VACAT_L_STM  =sprintf("%s %s/test_VACATION_LOW_STM.txt)",     REMOVE_LINES, ARG1)
VACAT_L_PSTM =sprintf("%s %s/test_VACATION_LOW_PSTM.txt)",    REMOVE_LINES, ARG1)
VACAT_L_HTM  =sprintf("%s %s/test_VACATION_LOW_HTM.txt)",     REMOVE_LINES, ARG1)
VACAT_L_PHTM =sprintf("%s %s/test_VACATION_LOW_PHTM.txt)",    REMOVE_LINES, ARG1)
VACAT_L_HTPM =sprintf("%s %s/test_VACATION_LOW_NVHTM_B.txt)", REMOVE_LINES, ARG1)

LABYR_STM  =sprintf("%s %s/test_LABYRINTH_STM.txt)",     REMOVE_LINES, ARG1)
LABYR_PSTM =sprintf("%s %s/test_LABYRINTH_PSTM.txt)",    REMOVE_LINES, ARG1)
LABYR_HTM  =sprintf("%s %s/test_LABYRINTH_HTM.txt)",     REMOVE_LINES, ARG1)
LABYR_PHTM =sprintf("%s %s/test_LABYRINTH_PHTM.txt)",    REMOVE_LINES, ARG1)
LABYR_HTPM =sprintf("%s %s/test_LABYRINTH_NVHTM_B.txt)", REMOVE_LINES, ARG1)

YADA_STM  =sprintf("%s %s/test_YADA_STM.txt)",     REMOVE_LINES, ARG1)
YADA_PSTM =sprintf("%s %s/test_YADA_PSTM.txt)",    REMOVE_LINES, ARG1)
YADA_HTM  =sprintf("%s %s/test_YADA_HTM.txt)",     REMOVE_LINES, ARG1)
YADA_PHTM =sprintf("%s %s/test_YADA_PHTM.txt)",    REMOVE_LINES, ARG1)
YADA_HTPM =sprintf("%s %s/test_YADA_NVHTM_B.txt)", REMOVE_LINES, ARG1)
YADA_HTPMS=sprintf("%s %s/test_YADA_NVHTM_B_10.txt)", REMOVE_LINES, ARG1)

SSCA2_STM  =sprintf("%s %s/test_SSCA2_STM.txt)",     REMOVE_LINES, ARG1)
SSCA2_PSTM =sprintf("%s %s/test_SSCA2_PSTM.txt)",    REMOVE_LINES, ARG1)
SSCA2_HTM  =sprintf("%s %s/test_SSCA2_HTM.txt)",     REMOVE_LINES, ARG1)
SSCA2_PHTM =sprintf("%s %s/test_SSCA2_PHTM.txt)",    REMOVE_LINES, ARG1)
SSCA2_HTPM =sprintf("%s %s/test_SSCA2_NVHTM_B.txt)", REMOVE_LINES, ARG1)

AVG_STM  =sprintf("%s %s/AVG_STM.txt)",     REMOVE_LINES, ARG1)
AVG_PSTM =sprintf("%s %s/AVG_PSTM.txt)",    REMOVE_LINES, ARG1)
AVG_HTM  =sprintf("%s %s/AVG_HTM.txt)",     REMOVE_LINES, ARG1)
AVG_PHTM =sprintf("%s %s/AVG_PHTM.txt)",    REMOVE_LINES, ARG1)
AVG_HTPM =sprintf("%s %s/AVG_NVHTM_B.txt)", REMOVE_LINES, ARG1)
AVG_HTPMS=sprintf("%s %s/AVG_NVHTM_B_10.txt)", REMOVE_LINES, ARG1)

set multiplot layout 1, 3 margins 0.08,0.99,0.30,0.82 spacing 0.005,0.15
unset xtics

#set title "\n{/*2 Kmeans (high)}" offset 0,2.8
set xlabel "{/*1.8 Kmeans (high)}" offset 0,-0.6 font ",16"

set key at graph 0.0,1.25 horizontal Left left reverse samplen 4 width 100 \
maxrows 1 maxcols 12 font ",24" spacing 0

#set xlabel  "Number of Threads\n(1, 4, 8, 16, 26)" center offset 27,-0.8 font ",26"

HTM  =KMEANS_H_HTM
STM  =KMEANS_H_STM
PHTM =KMEANS_H_PHTM
HTPM =KMEANS_H_HTPM
HTPMS=KMEANS_H_HTPMS
PSTM =KMEANS_H_PSTM
plot \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*0.9 Conflict}'             lw 2 lc 1, \
    ''   u 4         notitle                              lw 2 lc 2, \
    ''   u 6         notitle                              lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
	newhistogram '  NV-HTM_{NLP}' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
	newhistogram '      NV-HTM_{10x}' lt 1 fs pattern 1, \
    HTPMS u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '   PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

#set style histogram rowstacked title font ",16" \
#    textcolor lt -1 offset character 0.0,-1.0

set key at graph -0.4,1.34 spacing 1
unset ylabel
unset ytics
#unset key
set xlabel "{/*1.8 Vacation (high)}"

HTM  =VACAT_H_HTM
STM  =VACAT_H_STM
PHTM =VACAT_H_PHTM
HTPMS=VACAT_H_HTPMS
PSTM =VACAT_H_PSTM
plot \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) notitle                             lw 2 lc 1, \
    ''   u 4         title '{/*0.9 Capacity}'            lw 2 lc 2, \
    ''   u 6         title '{/*0.9 Unspecified}'         lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
	newhistogram '  NV-HTM_{NLP}' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
	newhistogram '          NV-HTM_{10x}' lt 1 fs pattern 1, \
    HTPMS u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '   PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

unset ytics

set xlabel "{/*1.8 Yada}"
set key at graph -0.78,1.25 spacing 1

set label  "Number of Threads (1, 4, 8, 16, 28)" at -31.0,-0.48 font ",24"

HTM  =YADA_HTM
STM  =YADA_STM
PHTM =YADA_PHTM
HTPM =YADA_HTPM
HTPMS=YADA_HTPMS
PSTM =YADA_PSTM
plot \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) notitle                                 lw 2 lc 1, \
    ''   u 4         notitle                                 lw 2 lc 2, \
    ''   u 6         notitle             lw 2 lc 3, \
    ''   u 5         title "{/*0.8 Log full (NV-HTM)}\n{/*0.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  NV-HTM_{NLP}' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
	newhistogram '      NV-HTM_{10x}' lt 1 fs pattern 1, \
    HTPMS u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '   PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

set key at graph -1.64,1.2
set title "\n{/*2 Average of All}"
set xlabel ""
#set label  "Number of Threads\n(1, 4, 8, 16, 28)" center at graph 1.53,0.1 font ",26"

HTM  =AVG_HTM
STM  =AVG_STM
PHTM =AVG_PHTM
HTPM =AVG_HTPM
HTPMS=AVG_HTPMS
PSTM =AVG_PSTM
#plot\
#	newhistogram 'PHTM' lt 1 fs pattern 1, \
#    PHTM u 3:xtic(1) notitle                              lw 2 lc 1, \
#    ''   u 4         notitle                              lw 2 lc 2, \
#    ''   u 6         notitle                              lw 2 lc 3, \
#    ''   u 5         title "{/*11.8 Log full (NV-HTM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
#	newhistogram ' HTPM_{NLP}' lt 1 fs pattern 1, \
#    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
#    ''   u 4         notitle lw 2 lc 2, \
#    ''   u 6         notitle lw 2 lc 3, \
#    ''   u 5         notitle lw 2 lc 7, \
#	newhistogram ' HTPM_{10x}' lt 1 fs pattern 1, \
#    HTPMS u 3:xtic(1) notitle lw 2 lc 1, \
#    ''   u 4         notitle lw 2 lc 2, \
#    ''   u 6         notitle lw 2 lc 3, \
#    ''   u 5         notitle lw 2 lc 7, \
#  newhistogram ' PSTM' lt 1 fs pattern 1, \
#    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

unset multiplot
