set term postscript color eps enhanced 22 size 30cm,8cm
set output sprintf("|ps2pdf -dEPSCrop - STAMP_PA_MULT.pdf", ARG2)

#set grid ytics
set xtics norangelimit font ",12pt"
set xtics border in scale 0,0 offset -0.2,0.3 nomirror rotate by -45  autojustify
set ytics offset 0.4,0 font ",22"

set yrange [0:1]

set style fill pattern 1 border lt -1
set style histogram rowstacked title font ",16" \
    textcolor lt -1 offset character 0.0,-0.2
set style data histograms

#set key outside horizontal Left top left reverse samplen 1 width 1 maxrows 1 maxcols 12
set nokey
#set title font ",18" sprintf("%s", ARG3)

set ylabel "Probability of Abort" font ",34" offset 1.4,-5.5

### TODO:

REMOVE_LINES="<(sed '3d;6d;7d;8d;10d;11d;12d;13d'"

GENOME_STM  =sprintf("%s %s/test_GENOME_STM.txt)",     REMOVE_LINES, ARG1)
GENOME_PSTM =sprintf("%s %s/test_GENOME_PSTM.txt)",    REMOVE_LINES, ARG1)
GENOME_HTM  =sprintf("%s %s/test_GENOME_HTM.txt)",     REMOVE_LINES, ARG1)
GENOME_PHTM =sprintf("%s %s/test_GENOME_PHTM.txt)",    REMOVE_LINES, ARG1)
GENOME_HTPM =sprintf("%s %s/test_GENOME_NVHTM_B.txt)", REMOVE_LINES, ARG1)

INTRUD_STM  =sprintf("%s %s/test_INTRUDER_STM.txt)",     REMOVE_LINES, ARG1)
INTRUD_PSTM =sprintf("%s %s/test_INTRUDER_PSTM.txt)",    REMOVE_LINES, ARG1)
INTRUD_HTM  =sprintf("%s %s/test_INTRUDER_HTM.txt)",     REMOVE_LINES, ARG1)
INTRUD_PHTM =sprintf("%s %s/test_INTRUDER_PHTM.txt)",    REMOVE_LINES, ARG1)
INTRUD_HTPM =sprintf("%s %s/test_INTRUDER_NVHTM_B.txt)", REMOVE_LINES, ARG1)

KMEANS_H_STM  =sprintf("%s %s/test_KMEANS_HIGH_STM.txt)",     REMOVE_LINES, ARG1)
KMEANS_H_PSTM =sprintf("%s %s/test_KMEANS_HIGH_PSTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_H_HTM  =sprintf("%s %s/test_KMEANS_HIGH_HTM.txt)",     REMOVE_LINES, ARG1)
KMEANS_H_PHTM =sprintf("%s %s/test_KMEANS_HIGH_PHTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_H_HTPM =sprintf("%s %s/test_KMEANS_HIGH_NVHTM_B.txt)", REMOVE_LINES, ARG1)

KMEANS_L_STM  =sprintf("%s %s/test_KMEANS_LOW_STM.txt)",     REMOVE_LINES, ARG1)
KMEANS_L_PSTM =sprintf("%s %s/test_KMEANS_LOW_PSTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_L_HTM  =sprintf("%s %s/test_KMEANS_LOW_HTM.txt)",     REMOVE_LINES, ARG1)
KMEANS_L_PHTM =sprintf("%s %s/test_KMEANS_LOW_PHTM.txt)",    REMOVE_LINES, ARG1)
KMEANS_L_HTPM =sprintf("%s %s/test_KMEANS_LOW_NVHTM_B.txt)", REMOVE_LINES, ARG1)

VACAT_H_STM  =sprintf("%s %s/test_VACATION_HIGH_STM.txt)",     REMOVE_LINES, ARG1)
VACAT_H_PSTM =sprintf("%s %s/test_VACATION_HIGH_PSTM.txt)",    REMOVE_LINES, ARG1)
VACAT_H_HTM  =sprintf("%s %s/test_VACATION_HIGH_HTM.txt)",     REMOVE_LINES, ARG1)
VACAT_H_PHTM =sprintf("%s %s/test_VACATION_HIGH_PHTM.txt)",    REMOVE_LINES, ARG1)
VACAT_H_HTPM =sprintf("%s %s/test_VACATION_HIGH_NVHTM_B.txt)", REMOVE_LINES, ARG1)

VACAT_L_STM  =sprintf("%s %s/test_VACATION_LOW_STM.txt)",     REMOVE_LINES, ARG1)
VACAT_L_PSTM =sprintf("%s %s/test_VACATION_LOW_PSTM.txt)",    REMOVE_LINES, ARG1)
VACAT_L_HTM  =sprintf("%s %s/test_VACATION_LOW_HTM.txt)",     REMOVE_LINES, ARG1)
VACAT_L_PHTM =sprintf("%s %s/test_VACATION_LOW_PHTM.txt)",    REMOVE_LINES, ARG1)
VACAT_L_HTPM =sprintf("%s %s/test_VACATION_LOW_NVHTM_B.txt)", REMOVE_LINES, ARG1)

LABYR_STM  =sprintf("%s %s/test_LABYRINTH_STM.txt)",     REMOVE_LINES, ARG1)
LABYR_PSTM =sprintf("%s %s/test_LABYRINTH_PSTM.txt)",    REMOVE_LINES, ARG1)
LABYR_HTM  =sprintf("%s %s/test_LABYRINTH_HTM.txt)",     REMOVE_LINES, ARG1)
LABYR_PHTM =sprintf("%s %s/test_LABYRINTH_PHTM.txt)",    REMOVE_LINES, ARG1)
LABYR_HTPM =sprintf("%s %s/test_LABYRINTH_NVHTM_B.txt)", REMOVE_LINES, ARG1)

YADA_STM  =sprintf("%s %s/test_YADA_STM.txt)",     REMOVE_LINES, ARG1)
YADA_PSTM =sprintf("%s %s/test_YADA_PSTM.txt)",    REMOVE_LINES, ARG1)
YADA_HTM  =sprintf("%s %s/test_YADA_HTM.txt)",     REMOVE_LINES, ARG1)
YADA_PHTM =sprintf("%s %s/test_YADA_PHTM.txt)",    REMOVE_LINES, ARG1)
YADA_HTPM =sprintf("%s %s/test_YADA_NVHTM_B.txt)", REMOVE_LINES, ARG1)

SSCA2_STM  =sprintf("%s %s/test_SSCA2_STM.txt)",     REMOVE_LINES, ARG1)
SSCA2_PSTM =sprintf("%s %s/test_SSCA2_PSTM.txt)",    REMOVE_LINES, ARG1)
SSCA2_HTM  =sprintf("%s %s/test_SSCA2_HTM.txt)",     REMOVE_LINES, ARG1)
SSCA2_PHTM =sprintf("%s %s/test_SSCA2_PHTM.txt)",    REMOVE_LINES, ARG1)
SSCA2_HTPM =sprintf("%s %s/test_SSCA2_NVHTM_B.txt)", REMOVE_LINES, ARG1)

set multiplot layout 2, 5 margins 0.06,0.99,0.15,0.96 spacing 0.005,0.15
unset xtics

set xlabel "\n{/*2 Genome}" offset 0,0 font ",16"

HTM  =GENOME_HTM
STM  =GENOME_STM
PHTM =GENOME_PHTM
HTPM =GENOME_HTPM
PSTM =GENOME_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

unset ytics
unset ylabel

set xlabel "\n{/*2 Intruder}"

HTM  =INTRUD_HTM
STM  =INTRUD_STM
PHTM =INTRUD_PHTM
HTPM =INTRUD_HTPM
PSTM =INTRUD_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

set xlabel "\n{/*2 Kmeans (high)}"

HTM  =KMEANS_H_HTM
STM  =KMEANS_H_STM
PHTM =KMEANS_H_PHTM
HTPM =KMEANS_H_HTPM
PSTM =KMEANS_H_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

set xlabel "\n{/*2 Kmeans (low)}"

HTM  =KMEANS_L_HTM
STM  =KMEANS_L_STM
PHTM =KMEANS_L_PHTM
HTPM =KMEANS_L_HTPM
PSTM =KMEANS_L_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

set xlabel "\n{/*2 SSCA2}"

HTM  =SSCA2_HTM
STM  =SSCA2_STM
PHTM =SSCA2_PHTM
HTPM =SSCA2_HTPM
PSTM =SSCA2_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

set xlabel "\n{/*2 Vacation (high)}"

set ytics offset 0.4,0 font ",22"

HTM  =VACAT_H_HTM
STM  =VACAT_H_STM
PHTM =VACAT_H_PHTM
HTPM =VACAT_H_HTPM
PSTM =VACAT_H_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

unset ytics

set xlabel "\n{/*2 Vacation (low)}"

HTM  =VACAT_L_HTM
STM  =VACAT_L_STM
PHTM =VACAT_L_PHTM
HTPM =VACAT_L_HTPM
PSTM =VACAT_L_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

set xlabel "\n{/*2 Yada}"

HTM  =YADA_HTM
STM  =YADA_STM
PHTM =YADA_PHTM
HTPM =YADA_HTPM
PSTM =YADA_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

set xlabel "\n{/*2 Labyrinth}"
set key at graph 1.05,1.0 Left left reverse samplen 3 width -9 \
  maxrows 2 maxcols 12 font ",10" spacing 4.5
set label  "Number of Threads\n(1, 4, 8, 16, 28)" center at graph 1.53,0.1 font ",26"

HTM  =LABYR_HTM
STM  =LABYR_STM
PHTM =LABYR_PHTM
HTPM =LABYR_HTPM
PSTM =LABYR_PSTM
plot \
  newhistogram 'HTM' lt 1 fs pattern 1, \
		HTM   u 3:xtic(1) notitle lw 2 lc 1, \
    ''    u 4         notitle lw 2 lc 2, \
    ''    u 7         notitle lw 2 lc 3, \
	newhistogram 'PHTM' lt 1 fs pattern 1, \
    PHTM u 3:xtic(1) title '{/*2.5 Conflict}'                                 lw 2 lc 1, \
    ''   u 4         title '{/*2.5 Capacity}'                                 lw 2 lc 2, \
    ''   u 6         title '{/*2.5 Unspecified}'                              lw 2 lc 3, \
    ''   u 5         title "{/*1.8 Log full (HTPM)}\n\n{/*1.8 Locked (PHTM)}" lw 2 lc 7, \
	newhistogram '  HTPM' lt 1 fs pattern 1, \
    HTPM u 3:xtic(1) notitle lw 2 lc 1, \
    ''   u 4         notitle lw 2 lc 2, \
    ''   u 6         notitle lw 2 lc 3, \
    ''   u 5         notitle lw 2 lc 7, \
  newhistogram '  STM' lt 1 fs pattern 1, \
    STM using 2:xtic(1) notitle lw 2 lc 1, \
  newhistogram ' PSTM' lt 1 fs pattern 1, \
    PSTM using 2:xtic(1) notitle lw 2 lc 1, \
#

unset multiplot
