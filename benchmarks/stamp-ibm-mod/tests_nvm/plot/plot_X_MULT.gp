set term postscript color eps enhanced 22 size 30cm,8cm
# ARG1 - PATH_TO_DATA
# ARG2 - BENCHMARK NAME
# ARG3 - BENCHMARK
set output sprintf("|ps2pdf -dEPSCrop - STAMP_X_MULT.pdf", ARG2)

#set grid ytics
#set grid xtics

set format y "%.1t{/*0.8 x10}^{%T}"
#set logscale x 2
set ytics offset 0.2,0 font ",16"
set mxtics
set xtics rotate by -45 left font ",30" offset -0.8,0.3

set lmargin 9.2
set rmargin 3.3
set tmargin 1
set bmargin 4.5

### TODO:

#set key font ",16" outside right Left reverse width 4
set nokey
set ylabel "Throughput (TXs/s)" font ",34" offset 1.4,-5.8
#set title font ",18" sprintf("%s", ARG3)

GENOME_STM  =sprintf("%s/test_GENOME_STM.txt",     ARG1)
GENOME_PSTM =sprintf("%s/test_GENOME_PSTM.txt",    ARG1)
GENOME_HTM  =sprintf("%s/test_GENOME_HTM.txt",     ARG1)
GENOME_PHTM =sprintf("%s/test_GENOME_PHTM.txt",    ARG1)
GENOME_HTPM =sprintf("%s/test_GENOME_NVHTM_B.txt", ARG1)

INTRUD_STM  =sprintf("%s/test_INTRUDER_STM.txt",     ARG1)
INTRUD_PSTM =sprintf("%s/test_INTRUDER_PSTM.txt",    ARG1)
INTRUD_HTM  =sprintf("%s/test_INTRUDER_HTM.txt",     ARG1)
INTRUD_PHTM =sprintf("%s/test_INTRUDER_PHTM.txt",    ARG1)
INTRUD_HTPM =sprintf("%s/test_INTRUDER_NVHTM_B.txt", ARG1)

KMEANS_H_STM  =sprintf("%s/test_KMEANS_HIGH_STM.txt",     ARG1)
KMEANS_H_PSTM =sprintf("%s/test_KMEANS_HIGH_PSTM.txt",    ARG1)
KMEANS_H_HTM  =sprintf("%s/test_KMEANS_HIGH_HTM.txt",     ARG1)
KMEANS_H_PHTM =sprintf("%s/test_KMEANS_HIGH_PHTM.txt",    ARG1)
KMEANS_H_HTPM =sprintf("%s/test_KMEANS_HIGH_NVHTM_B.txt", ARG1)
KMEANS_H_HTPMS=sprintf("%s/test_KMEANS_HIGH_NVHTM_B_10.txt", ARG1)

KMEANS_L_STM  =sprintf("%s/test_KMEANS_LOW_STM.txt",     ARG1)
KMEANS_L_PSTM =sprintf("%s/test_KMEANS_LOW_PSTM.txt",    ARG1)
KMEANS_L_HTM  =sprintf("%s/test_KMEANS_LOW_HTM.txt",     ARG1)
KMEANS_L_PHTM =sprintf("%s/test_KMEANS_LOW_PHTM.txt",    ARG1)
KMEANS_L_HTPM =sprintf("%s/test_KMEANS_LOW_NVHTM_B.txt", ARG1)

VACAT_H_STM  =sprintf("%s/test_VACATION_HIGH_STM.txt",     ARG1)
VACAT_H_PSTM =sprintf("%s/test_VACATION_HIGH_PSTM.txt",    ARG1)
VACAT_H_HTM  =sprintf("%s/test_VACATION_HIGH_HTM.txt",     ARG1)
VACAT_H_PHTM =sprintf("%s/test_VACATION_HIGH_PHTM.txt",    ARG1)
VACAT_H_HTPM =sprintf("%s/test_VACATION_HIGH_NVHTM_B.txt", ARG1)
VACAT_H_HTPMS=sprintf("%s/test_VACATION_HIGH_NVHTM_B_10.txt", ARG1)

VACAT_L_STM  =sprintf("%s/test_VACATION_LOW_STM.txt",     ARG1)
VACAT_L_PSTM =sprintf("%s/test_VACATION_LOW_PSTM.txt",    ARG1)
VACAT_L_HTM  =sprintf("%s/test_VACATION_LOW_HTM.txt",     ARG1)
VACAT_L_PHTM =sprintf("%s/test_VACATION_LOW_PHTM.txt",    ARG1)
VACAT_L_HTPM =sprintf("%s/test_VACATION_LOW_NVHTM_B.txt", ARG1)

LABYR_STM  =sprintf("%s/test_LABYRINTH_STM.txt",     ARG1)
LABYR_PSTM =sprintf("%s/test_LABYRINTH_PSTM.txt",    ARG1)
LABYR_HTM  =sprintf("%s/test_LABYRINTH_HTM.txt",     ARG1)
LABYR_PHTM =sprintf("%s/test_LABYRINTH_PHTM.txt",    ARG1)
LABYR_HTPM =sprintf("%s/test_LABYRINTH_NVHTM_B.txt", ARG1)

YADA_STM  =sprintf("%s/test_YADA_STM.txt",     ARG1)
YADA_PSTM =sprintf("%s/test_YADA_PSTM.txt",    ARG1)
YADA_HTM  =sprintf("%s/test_YADA_HTM.txt",     ARG1)
YADA_PHTM =sprintf("%s/test_YADA_PHTM.txt",    ARG1)
YADA_HTPM =sprintf("%s/test_YADA_NVHTM_B.txt", ARG1)
YADA_HTPMS=sprintf("%s/test_YADA_NVHTM_B_10.txt", ARG1)

SSCA2_STM  =sprintf("%s/test_SSCA2_STM.txt",     ARG1)
SSCA2_PSTM =sprintf("%s/test_SSCA2_PSTM.txt",    ARG1)
SSCA2_HTM  =sprintf("%s/test_SSCA2_HTM.txt",     ARG1)
SSCA2_PHTM =sprintf("%s/test_SSCA2_PHTM.txt",    ARG1)
SSCA2_HTPM =sprintf("%s/test_SSCA2_NVHTM_B.txt", ARG1)

SSCA2_STM  =sprintf("%s/AVG_SSCA2_STM.txt",     ARG1)
SSCA2_PSTM =sprintf("%s/AVG_SSCA2_PSTM.txt",    ARG1)
SSCA2_HTM  =sprintf("%s/AVG_SSCA2_HTM.txt",     ARG1)
SSCA2_PHTM =sprintf("%s/AVG_SSCA2_PHTM.txt",    ARG1)
SSCA2_HTPM =sprintf("%s/AVG_SSCA2_NVHTM_B.txt", ARG1)
SSCA2_HTPMS=sprintf("%s/AVG_SSCA2_NVHTM_B_10.txt", ARG1)

set multiplot layout 2, 5 margins 0.07,0.99,0.10,0.96 spacing 0.05,0.13

set xlabel "Genome" font ",34" offset -0.0,0.1

unset xtics

nytics = 4

plot \
GENOME_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
GENOME_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
GENOME_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
GENOME_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
GENOME_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
GENOME_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
GENOME_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
GENOME_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
GENOME_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
GENOME_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "Intruder"
unset ylabel

plot \
INTRUD_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
INTRUD_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
INTRUD_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
INTRUD_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
INTRUD_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
INTRUD_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
INTRUD_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
INTRUD_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
INTRUD_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
INTRUD_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "Kmeans (high)"

plot \
KMEANS_H_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
KMEANS_H_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
KMEANS_H_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
KMEANS_H_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
KMEANS_H_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
KMEANS_H_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
KMEANS_H_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
KMEANS_H_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
KMEANS_H_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
KMEANS_H_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "Kmeans (low)"

plot \
KMEANS_L_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
KMEANS_L_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
KMEANS_L_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
KMEANS_L_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
KMEANS_L_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
KMEANS_L_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
KMEANS_L_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
KMEANS_L_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
KMEANS_L_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
KMEANS_L_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "SSCA2"

plot \
SSCA2_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
SSCA2_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
SSCA2_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
SSCA2_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
SSCA2_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
SSCA2_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
SSCA2_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
SSCA2_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
SSCA2_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
SSCA2_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "Vacation (high)"

plot \
VACAT_H_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
VACAT_H_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
VACAT_H_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
VACAT_H_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
VACAT_H_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
VACAT_H_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
VACAT_H_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
VACAT_H_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
VACAT_H_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
VACAT_H_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "Vacation (low)"

plot \
VACAT_L_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
VACAT_L_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
VACAT_L_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
VACAT_L_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
VACAT_L_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
VACAT_L_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
VACAT_L_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
VACAT_L_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
VACAT_L_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
VACAT_L_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "Yada"

plot \
YADA_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
YADA_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
YADA_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
YADA_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
YADA_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
YADA_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
YADA_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
YADA_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
YADA_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
YADA_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

set xlabel "Labyrith"
set key font ",24" at graph 2.38,0.99 right Left reverse maxcols 3 maxrows 3 \
  width 1 spacing 1

set label  "Number of Threads\n(1, 2, 4, 8, 12 16, 20 24, 28)" center at graph 1.7,0.1 font ",26"

plot \
LABYR_STM  u ($1 + 0.05):($6):($6-$12):($6+$12) title "STM"  w yerrorbars lc 6 lw 4 ps 2, \
LABYR_STM  u 1:($6)                             notitle      w lines      lc 6 lw 4, \
LABYR_HTM  u ($1 - 0.15):($8):($8-$16):($8+$16) title "HTM"  w yerrorbars lc 1 lw 4 ps 2, \
LABYR_HTM  u 1:($8)                             notitle      w lines      lc 1 lw 4, \
LABYR_PSTM u ($1 - 0.05):($6):($6-$12):($6+$12) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
LABYR_PSTM u 1:($6)                             notitle      w lines      lc 7 lw 4, \
LABYR_PHTM u ($1 + 0.05):($8):($8-$16):($8+$16) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
LABYR_PHTM u 1:($8)                             notitle      w lines      lc 2 lw 4, \
LABYR_HTPM u ($1 - 0.20):($8):($8-$16):($8+$16) title 'NV-HTM' w yerrorbars lc 4 lw 4 ps 2, \
LABYR_HTPM u 1:($8)                             notitle      w lines      lc 4 lw 4, \
#

unset multiplot
