set term postscript color eps enhanced 22 size 15cm,8.5cm
# ARG1 - PATH_TO_DATA
# ARG2 - BENCHMARK NAME
# ARG3 - BENCHMARK
set output sprintf("|ps2pdf -dEPSCrop - test_AVG_X.pdf", ARG2)

set grid ytics
set grid xtics

#set logscale x 2
set ytics offset 0.8,0 font ",30"
set mxtics
set xtics rotate by -45 left font ",30" offset -0.8,0.3

set lmargin 8.8
set rmargin 3.3
set tmargin 1
set bmargin 5

### TODO:

set key font ",16" outside right Left reverse width 4
set ylabel "Throughput (x10^6 TXs/s)" font ",34" offset -0.8,-0.0
set xlabel "Number of Threads" font ",34" offset -0.0,-0.5
#set title font ",18" sprintf("%s", ARG3)

STM  =sprintf("%s/AVG_STM.txt", ARG1, ARG2)
PSTM =sprintf("%s/AVG_PSTM.txt", ARG1, ARG2)
HTM  =sprintf("%s/AVG_HTM.txt", ARG1, ARG2)
PHTM =sprintf("%s/AVG_PHTM.txt", ARG1, ARG2)
NVHTM_F_LC=sprintf("%s/AVG_NVHTM_F_LC.txt", ARG1, ARG2)
NVHTM_B=sprintf("%s/AVG_NVHTM_B.txt", ARG1, ARG2)
NVHTM_F=sprintf("%s/AVG_NVHTM_F.txt", ARG1, ARG2)
NVHTM_W=sprintf("%s/AVG_NVHTM_W.txt", ARG1, ARG2)

plot \
  HTM     u ($1 - 0.15):($8/1e6):($8/1e6-$16/1e6):($8/1e6+$16/1e6) title "HTM"    w yerrorbars lc 1, \
  HTM     u 1:($8/1e6)                                             notitle        w lines      lc 1, \
  PHTM    u ($1 + 0.05):($8/1e6):($8/1e6-$16/1e6):($8/1e6+$16/1e6) title 'PHTM'   w yerrorbars lc 2, \
  PHTM    u 1:($8/1e6)                                             notitle        w lines      lc 2, \
  NVHTM_B u ($1 - 0.20):($8/1e6):($8/1e6-$16/1e6):($8/1e6+$16/1e6) title 'NV-HTM_B' w yerrorbars lc 4, \
  NVHTM_B u 1:($8/1e6)                                             notitle        w lines      lc 4, \
  STM     u ($1 + 0.05):($6/1e6):($6/1e6-$12/1e6):($6/1e6+$12/1e6) title "STM"    w yerrorbars lc 6, \
  STM     u 1:($6/1e6)                                             notitle        w lines      lc 6, \
  PSTM    u ($1 - 0.05):($6/1e6):($6/1e6-$12/1e6):($6/1e6+$12/1e6) title 'PSTM'   w yerrorbars lc 7, \
  PSTM    u 1:($6/1e6)                                             notitle        w lines      lc 7, \
  #NVHTM_W u ($1 + 0.00):($8/1e6):($8/1e6-$16/1e6):($8/1e6+$16/1e6) title 'NV-HTM_W' w yerrorbars lc 5, \
  #NVHTM_W u 1:($8/1e6)                                             notitle        w lines      lc 5, \
