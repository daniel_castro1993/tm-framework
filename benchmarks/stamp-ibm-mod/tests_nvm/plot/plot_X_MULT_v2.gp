set term postscript color eps enhanced 22 size 20cm,5cm
# ARG1 - PATH_TO_DATA
# ARG2 - BENCHMARK NAME
# ARG3 - BENCHMARK
set output sprintf("|ps2pdf -dEPSCrop - STAMP_X_MULT.pdf", ARG2)

#set grid ytics
#set grid xtics

#set format y "%.1t{/*0.8 x10}^{%T}"
#set logscale x 2
set ytics offset 0.6,0 font ",20"
set mxtics
set xtics rotate by -45 left font ",24" offset -0.8,0.3

set lmargin 9.2
set rmargin 3.3
set tmargin 1
set bmargin 1

### TODO:

#set key font ",16" outside right Left reverse width 4
set nokey
set ylabel "Throughput ({/*0.6 x10}^5 TXs/s)" font ",24" offset 1.7,-0.4
#set title font ",18" sprintf("%s", ARG3)

GENOME_STM  =sprintf("%s/test_GENOME_STM.txt",     ARG1)
GENOME_PSTM =sprintf("%s/test_GENOME_PSTM.txt",    ARG1)
GENOME_HTM  =sprintf("%s/test_GENOME_HTM.txt",     ARG1)
GENOME_PHTM =sprintf("%s/test_GENOME_PHTM.txt",    ARG1)
GENOME_HTPM =sprintf("%s/test_GENOME_NVHTM_B.txt", ARG1)

INTRUD_STM  =sprintf("%s/test_INTRUDER_STM.txt",     ARG1)
INTRUD_PSTM =sprintf("%s/test_INTRUDER_PSTM.txt",    ARG1)
INTRUD_HTM  =sprintf("%s/test_INTRUDER_HTM.txt",     ARG1)
INTRUD_PHTM =sprintf("%s/test_INTRUDER_PHTM.txt",    ARG1)
INTRUD_HTPM =sprintf("%s/test_INTRUDER_NVHTM_B.txt", ARG1)

KMEANS_H_STM  =sprintf("%s/test_KMEANS_HIGH_STM.txt",     ARG1)
KMEANS_H_PSTM =sprintf("%s/test_KMEANS_HIGH_PSTM.txt",    ARG1)
KMEANS_H_HTM  =sprintf("%s/test_KMEANS_HIGH_HTM.txt",     ARG1)
KMEANS_H_PHTM =sprintf("%s/test_KMEANS_HIGH_PHTM.txt",    ARG1)
KMEANS_H_HTPM =sprintf("%s/test_KMEANS_HIGH_NVHTM_B.txt", ARG1)
KMEANS_H_HTPMS=sprintf("%s/test_KMEANS_HIGH_NVHTM_B_10.txt", ARG1)

KMEANS_L_STM  =sprintf("%s/test_KMEANS_LOW_STM.txt",     ARG1)
KMEANS_L_PSTM =sprintf("%s/test_KMEANS_LOW_PSTM.txt",    ARG1)
KMEANS_L_HTM  =sprintf("%s/test_KMEANS_LOW_HTM.txt",     ARG1)
KMEANS_L_PHTM =sprintf("%s/test_KMEANS_LOW_PHTM.txt",    ARG1)
KMEANS_L_HTPM =sprintf("%s/test_KMEANS_LOW_NVHTM_B.txt", ARG1)

VACAT_H_STM  =sprintf("%s/test_VACATION_HIGH_STM.txt",     ARG1)
VACAT_H_PSTM =sprintf("%s/test_VACATION_HIGH_PSTM.txt",    ARG1)
VACAT_H_HTM  =sprintf("%s/test_VACATION_HIGH_HTM.txt",     ARG1)
VACAT_H_PHTM =sprintf("%s/test_VACATION_HIGH_PHTM.txt",    ARG1)
VACAT_H_HTPM =sprintf("%s/test_VACATION_HIGH_NVHTM_B.txt", ARG1)
VACAT_H_HTPMS=sprintf("%s/test_VACATION_HIGH_NVHTM_B_10.txt", ARG1)

VACAT_L_STM  =sprintf("%s/test_VACATION_LOW_STM.txt",     ARG1)
VACAT_L_PSTM =sprintf("%s/test_VACATION_LOW_PSTM.txt",    ARG1)
VACAT_L_HTM  =sprintf("%s/test_VACATION_LOW_HTM.txt",     ARG1)
VACAT_L_PHTM =sprintf("%s/test_VACATION_LOW_PHTM.txt",    ARG1)
VACAT_L_HTPM =sprintf("%s/test_VACATION_LOW_NVHTM_B.txt", ARG1)

LABYR_STM  =sprintf("%s/test_LABYRINTH_STM.txt",     ARG1)
LABYR_PSTM =sprintf("%s/test_LABYRINTH_PSTM.txt",    ARG1)
LABYR_HTM  =sprintf("%s/test_LABYRINTH_HTM.txt",     ARG1)
LABYR_PHTM =sprintf("%s/test_LABYRINTH_PHTM.txt",    ARG1)
LABYR_HTPM =sprintf("%s/test_LABYRINTH_NVHTM_B.txt", ARG1)

YADA_STM  =sprintf("%s/test_YADA_STM.txt",     ARG1)
YADA_PSTM =sprintf("%s/test_YADA_PSTM.txt",    ARG1)
YADA_HTM  =sprintf("%s/test_YADA_HTM.txt",     ARG1)
YADA_PHTM =sprintf("%s/test_YADA_PHTM.txt",    ARG1)
YADA_HTPM =sprintf("%s/test_YADA_NVHTM_B.txt", ARG1)
YADA_HTPMS=sprintf("%s/test_YADA_NVHTM_B_10.txt", ARG1)

SSCA2_STM  =sprintf("%s/test_SSCA2_STM.txt",     ARG1)
SSCA2_PSTM =sprintf("%s/test_SSCA2_PSTM.txt",    ARG1)
SSCA2_HTM  =sprintf("%s/test_SSCA2_HTM.txt",     ARG1)
SSCA2_PHTM =sprintf("%s/test_SSCA2_PHTM.txt",    ARG1)
SSCA2_HTPM =sprintf("%s/test_SSCA2_NVHTM_B.txt", ARG1)

AVG_STM  =sprintf("%s/AVG_STM.txt",     ARG1)
AVG_PSTM =sprintf("%s/AVG_PSTM.txt",    ARG1)
AVG_HTM  =sprintf("%s/AVG_HTM.txt",     ARG1)
AVG_PHTM =sprintf("%s/AVG_PHTM.txt",    ARG1)
AVG_HTPM =sprintf("%s/AVG_NVHTM_B.txt", ARG1)
AVG_HTPMS=sprintf("%s/AVG_NVHTM_B_10.txt", ARG1)

set multiplot layout 1, 3 margins 0.05,0.983,0.14,0.96 spacing 0.03,0.13

#set xlabel "Kmeans (high)" font ",34" offset -0.0,0.1
#set xlabel "Number of threads" font ",34" offset 28.0,0.1

#unset xtics

nytics = 4

set yrange [0:9.5]

set key font ",22" at graph 0.85,0.98 right Left reverse maxcols 3 maxrows 3 \
  width -2 spacing 1

plot \
KMEANS_H_PSTM  u ($1 - 0.05):($6/1e5):($6/1e5-$12/1e5):($6/1e5+$12/1e5) notitle w yerrorbars lc 7 lw 4 ps 2, \
KMEANS_H_PSTM  u 1:($6/1e5)                                             notitle  w lines          lc 7 lw 4, \
KMEANS_H_PHTM  u ($1 + 0.05):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars lc 2 lw 4 ps 2, \
KMEANS_H_PHTM  u 1:($8/1e5)                                             notitle  w lines          lc 2 lw 4, \
KMEANS_H_HTPM  u ($1 - 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) title 'NV-HTM_{NLP}' w yerrorbars pt 4 lc 4 lw 4 ps 1.6, \
KMEANS_H_HTPM  u 1:($8/1e5)                                             notitle  w lines          lc 4 lw 4, \
KMEANS_H_HTPMS u ($1 + 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars pt 5 lc 4 lw 4 ps 2, \
KMEANS_H_HTPMS u 1:($8/1e5)                                             notitle  w lines          dt "." lc 4 lw 4, \
#

set key at graph 0.49,0.98

unset ylabel
#unset xlabel
#set xlabel "Vacation (high)"
set xlabel ""
set yrange [0:6.5]

plot \
VACAT_H_PSTM  u ($1 - 0.05):($6/1e5):($6/1e5-$12/1e5):($6/1e5+$12/1e5) notitle w yerrorbars lc 7 lw 4 ps 2, \
VACAT_H_PSTM  u 1:($6/1e5)                             notitle      w lines      lc 7 lw 4, \
VACAT_H_PHTM  u ($1 + 0.05):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars lc 2 lw 4 ps 2, \
VACAT_H_PHTM  u 1:($8/1e5)                             notitle      w lines      lc 2 lw 4, \
VACAT_H_HTPM  u ($1 - 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars pt 4 lc 4 lw 4 ps 1.6, \
VACAT_H_HTPM  u 1:($8/1e5)                             notitle      w lines      lc 4 lw 4, \
VACAT_H_HTPMS u ($1 + 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) title 'NV-HTM_{10x}' w yerrorbars pt 5 lc 4 lw 4 ps 2, \
VACAT_H_HTPMS u 1:($8/1e5)                             notitle      w lines  dt "." lc 4 lw 4, \
#

set key at graph 0.35,0.98

#set xlabel "Yada"
set xlabel ""

plot \
YADA_PSTM u ($1 - 0.05):($6/1e5):($6/1e5-$12/1e5):($6/1e5+$12/1e5) title 'PSTM' w yerrorbars lc 7 lw 4 ps 2, \
YADA_PSTM u 1:($6/1e5)                                             notitle      w lines      lc 7 lw 4, \
YADA_PHTM u ($1 + 0.05):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) title 'PHTM' w yerrorbars lc 2 lw 4 ps 2, \
YADA_PHTM u 1:($8/1e5)                                             notitle      w lines      lc 2 lw 4, \
YADA_HTPM u ($1 - 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars pt 4 lc 4 lw 4 ps 1.6, \
YADA_HTPM u 1:($8/1e5)                                             notitle      w lines      lc 4 lw 4, \
YADA_HTPMS u ($1 + 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars pt 5 lc 4 lw 4 ps 2, \
YADA_HTPMS u 1:($8/1e5)                                             notitle   w lines  dt "." lc 4 lw 4, \
#

#set xlabel "Average of All"
set xlabel ""
set key at graph 0.38,0.98

#set label  "Number of Threads\n(1, 2, 4, 8, 12, 13, 14,\n16, 20 24, 26, 27, 28)" center at graph 1.7,0.2 font ",26"

#plot \
#AVG_PSTM u ($1 - 0.05):($6/1e5):($6/1e5-$12/1e5):($6/1e5+$12/1e5) notitle w yerrorbars lc 7 lw 4 ps 2, \
#AVG_PSTM u 1:($6/1e5)                             notitle      w lines      lc 7 lw 4, \
#AVG_PHTM u ($1 + 0.05):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars lc 2 lw 4 ps 2, \
#AVG_PHTM u 1:($8/1e5)                             notitle      w lines      lc 2 lw 4, \
#AVG_HTPM u ($1 - 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars pt 4 lc 4 lw 4 ps 1.6, \
#AVG_HTPM u 1:($8/1e5)                             notitle      w lines      lc 4 lw 4, \
#AVG_HTPMS u ($1 + 0.20):($8/1e5):($8/1e5-$16/1e5):($8/1e5+$16/1e5) notitle w yerrorbars pt 5 lc 4 lw 4 ps 2, \
#AVG_HTPMS u 1:($8/1e5)                             notitle      w lines  dt "." lc 4 lw 4, \
#

unset multiplot
