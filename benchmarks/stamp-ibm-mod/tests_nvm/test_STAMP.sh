#!/bin/bash

#############
# Runs the test suit:
# HTM / PHTM / NV-HTM
#############

SAMPLES=10
NB_BENCH=2

test[1]="./kmeans/kmeans -m15 -n15 -t0.05 -i ./kmeans/inputs/random-n65536-d32-c16.txt -p" # high
test[2]="./vacation/vacation -n4 -q60 -u90 -r8192 -t262144 -c"  # high
test[3]="./yada/yada -a10 -i ./yada/inputs/ttimeu10000.2 -t"
test[4]="./genome/genome -g16384 -s64 -n16777216 -t"
test[5]="./ssca2/ssca2 -s14 -i1.0 -u1.0 -l9 -p9 -t"
test[6]="./kmeans/kmeans -m40 -n40 -t0.05 -i ./kmeans/inputs/random-n65536-d32-c16.txt -p" # low
test[7]="./vacation/vacation -n2 -q90 -u98 -r8192 -t262144 -c"  # low
test[8]="./intruder/intruder -a10 -l128 -n131072 -s1 -t"
test[9]="./labyrinth/labyrinth -i labyrinth/inputs/random-x48-y48-z3-n64.txt -t"
test[10]="./bayes/bayes -v32 -r4096 -n2 -p20 -i2 -e2 -t" # Does not work

# drop labyrith, it does not had much info

test_name[1]="KMEANS_HIGH"
test_name[2]="VACATION_HIGH"
test_name[3]="YADA"
test_name[4]="GENOME"
test_name[5]="SSCA2"
test_name[6]="KMEANS_LOW"
test_name[7]="VACATION_LOW"
test_name[8]="INTRUDER"
test_name[9]="LABYRINTH"
test_name[10]="BAYES"

test_exec[1]="kmeans"
test_exec[2]="vacation"
test_exec[3]="yada"
test_exec[4]="genome"
test_exec[5]="ssca2"
test_exec[6]="kmeans"
test_exec[7]="vacation"
test_exec[8]="intruder"
test_exec[9]="labyrith"
test_exec[10]="bayes"

THREADS="1 2 4 8 12 13 14 16 20 24 26 27 28"

# 1GB
LOG_SIZE=40000000

BUILD_SCRIPT=./build-stamp.sh
DATA_FOLDER=./dataSTAMP
SCRIPTS_FOLDER=../../scripts
PLOT_FOLDER=./tests_nvm/plot

# dir where build.sh is
cd ..

mkdir -p $DATA_FOLDER

### Call with 1-program 2-threads
function bench {
	echo -ne "$2\n" >> parameters.txt
	ipcrm -M 0x00054321 >/dev/null
	timeout --signal=2 5m ${test[$1]} $2 >/dev/null
    # retry
    if [ $? -ne 0 ]; then
			pkill ${test_exec[$1]}
      timeout --signal=2 10m ${test[$1]} $2  >/dev/null
    fi
    # retry
    if [ $? -ne 0 ]; then
			pkill ${test_exec[$1]}
      timeout --signal=2 20m ${test[$1]} $2  >/dev/null
    fi
		pkill ${test_exec[$1]}
}

function run_bench {
    rm -f parameters.txt stats_file stats_file.aux_thr
    for i in `seq $NB_BENCH`
    do
        for a in `seq $SAMPLES`
        do
            echo -ne "#THREADS\n" > parameters.txt
			for t in $THREADS
			do
				# rm -f /tmp/*.socket
				bench $i $t
                wait ;
                sleep 0.01
                echo "$a - ${test[$i]} $t COMPLETE!\n"
			done
            mv parameters.txt     $DATA_FOLDER/"${test_name[$i]}"_"$1"_par_s"$a"
            mv stats_file         $DATA_FOLDER/"${test_name[$i]}"_"$1"_s"$a"
            mv stats_file.aux_thr $DATA_FOLDER/"${test_name[$i]}"_"$1"_aux_s"$a"
		done
		echo "${test_name[$i]} COMPLETE!\n"
	done
}

# MAKEFILE_ARGS=" NDEBUG=1" \
#     $BUILD_SCRIPT stm-tinystm file >/dev/null
# run_bench STM
# # #
# MAKEFILE_ARGS="PERSISTENT_TM=1 NDEBUG=1" \
#     $BUILD_SCRIPT stm-tinystm file >/dev/null
# run_bench PSTM
# # #
# MAKEFILE_ARGS="SOLUTION=1 NDEBUG=1" \
#     $BUILD_SCRIPT htm-sgl-nvm file >/dev/null
# run_bench HTM
#
# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=4 LOG_SIZE=$LOG_SIZE \
#     NDEBUG=1" \
#     $BUILD_SCRIPT htm-sgl-nvm file >/dev/null
# run_bench NVHTM_W

# NVHTM physical clocks, 2 threads sort,
# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE \
#     SORT_ALG=4 THRESHOLD=0.0 NDEBUG=1" \
#     $BUILD_SCRIPT htm-sgl-nvm file >/dev/null
# run_bench NVHTM_F
#
# MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE \
#    SORT_ALG=4 THRESHOLD=0.0 NDEBUG=1" \
#    $BUILD_SCRIPT htm-sgl-nvm file >/dev/null
# run_bench NVHTM_F_LC

MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE \
    SORT_ALG=5 FILTER=0.50 NDEBUG=1" \
    $BUILD_SCRIPT htm-sgl-nvm file >/dev/null
run_bench NVHTM_B

LOG_SIZE=550000 # wrap around 10 times
MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE \
SORT_ALG=5 FILTER=0.50 NDEBUG=1" \
$BUILD_SCRIPT htm-sgl-nvm file >/dev/null
run_bench NVHTM_B_10

MAKEFILE_ARGS="SOLUTION=2 NDEBUG=1" \
    $BUILD_SCRIPT htm-sgl-nvm file >/dev/null
run_bench PHTM

cd ./tests_nvm
./test_proc_STAMP.sh

rm $DATA_FOLDER/*CAT $DATA_FOLDER/*CAT.cols
