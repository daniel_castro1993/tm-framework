#!/bin/bash

# test[1]="./genome/genome -g16384 -s64 -n16777216 -r1 -t" 
# test[2]="./intruder/intruder -a10 -l128 -n262144 -s1 -r1 -t" 
# test[3]="./kmeans/kmeans -m15 -n15 -t0.00001 -i kmeans/inputs/random-n65536-d32-c16.txt -r1 -p" 
# test[4]="./kmeans/kmeans -m40 -n40 -t0.00001 -i kmeans/inputs/random-n65536-d32-c16.txt -r1 -p" 
# test[5]="./labyrinth/labyrinth  -i labyrinth/inputs/random-x512-y512-z7-n512.txt -t" 
# test[6]="./ssca2/ssca2 -s20 -i1.0 -u1.0 -l3 -p3 -r1 -t" 
# test[7]="./vacation/vacation -n4 -q60 -u90 -r1048576 -t4194304 -a1 -c" 
# test[8]="./vacation/vacation  -n2 -q90 -u98 -r1048576 -t4194304 -a1 -c" 
# test[9]="./yada/yadome -g16384 -s64 -n16777216 -r1 -t" 
#before intruder -n32768
test[1]="./intruder/intruder -a10 -l128 -n262144 -s1 -t" 
test[2]="./genome/genome -g16384 -s64 -n16777216 -t" 
test[3]="./vacation/vacation -n2 -q20 -u10 -r1048576 -t4194304 -c"  # low
test[4]="./vacation/vacation -n4 -q50 -u30 -r1048576 -t4194304 -c"  # high
test[5]="./yada/yada -a15 -i ./yada/inputs/ttimeu1000000.2 -t"
test[6]="./kmeans/kmeans -m40 -n40 -t0.00001 -i ./kmeans/inputs/random-n65536-d32-c16.txt -p" # low
test[7]="./kmeans/kmeans -m15 -n15 -t0.00001 -i ./kmeans/inputs/random-n65536-d32-c16.txt -p" # high
test[8]="./ssca2/ssca2 -s20 -i1.0 -u1.0 -l3 -p3 -t" 
test[9]="./labyrinth/labyrinth -i labyrinth/inputs/random-x128-y128-z5-n128.txt -t" 
#labyrinth before random-x256-y256-z3-n256.txt

test_name[1]="INTRUDER"
test_name[2]="GENOME"
test_name[3]="VACATION_LOW"
test_name[4]="VACATION_HIGH"
test_name[5]="YADA"
test_name[6]="KMEANS_LOW"
test_name[7]="KMEANS_HIGH"
test_name[8]="SSCA2"
test_name[9]="LABYRINTH"

SAMPLES=3

function run_bench {

	rm -f $1

	rm -f stats_file stats_file.aux_thr
	for i in `seq 9`
	do
		for a in `seq $SAMPLES`
		do
			for t in 1 2 4 6 8 12 14 20 28 36 44 52 54 # 1 2 4 6 8 # 
			do
				# rm -f /tmp/*.socket
				ipcrm -M 0x00054321
				timeout 10m ${test[$i]} $t
                wait ;
                sleep 0.01
			done
			mv stats_file "${test_name[$i]}"_"$1".s"$a" 
		done
		# echo "${test[$i]}1:8 COMPLETE!\n" >> $1.txt
	done
}

LOG_SIZE=100000

##### MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=1 LOG_SIZE=10000000 PERIOD=1000" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-P.txt
##### run_bench test_REDO-TS-LLOG-P

##### MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=1 LOG_SIZE=10000 PERIOD=1000" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-SLOG-P.txt
##### run_bench test_REDO-TS-SLOG-P

### FORK
MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE THRESHOLD=0.0 \
SORT_ALG=4" ./build-stamp.sh htm-sgl-nvm REDO-COUNTER-LLOG-F.txt
run_bench REDO-COUNTER-LLOG-F

MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE THRESHOLD=0.0 \
SORT_ALG=4" ./build-stamp.sh htm-sgl-nvm REDO-TS-LLOG-F.txt
run_bench REDO-TS-LLOG-F

# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-R.txt
# run_bench test_REDO-TS-LLOG-R

# MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-COUNTER-LLOG-R.txt
# run_bench test_REDO-COUNTER-LLOG-R

##### MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=2 LOG_SIZE=10000 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-SLOG-R.txt
##### run_bench test_REDO-TS-SLOG-R

### With mallocs

# MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-COUNTER-LLOG-R-MALLOC.txt
# run_bench test_REDO-COUNTER-LLOG-R-MALLOC

# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-R-MALLOC.txt
# run_bench test_REDO-TS-LLOG-R-MALLOC

MAKEFILE_ARGS="SOLUTION=1" ./build-stamp.sh htm-sgl-nvm HTM.txt
run_bench HTM

MAKEFILE_ARGS="SOLUTION=2" ./build-stamp.sh htm-sgl-nvm AVNI.txt
run_bench AVNI

# MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-COUNTER-LLOG-F-MALLOC.txt
# run_bench test_REDO-COUNTER-LLOG-F-MALLOC

# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-F-MALLOC.txt
# run_bench test_REDO-TS-LLOG-F-MALLOC

