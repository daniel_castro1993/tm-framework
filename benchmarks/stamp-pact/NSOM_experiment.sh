#!/bin/bash

test[1]="./intruder/intruder -a10 -l128 -n262144 -s1 -t"

test_name[1]="INTRUDER"

SAMPLES=10

function run_bench {

	rm -f $1

	rm -f stats_file stats_file.aux_thr
	for i in `seq 1`
	do
		for a in `seq $SAMPLES`
		do
			for t in 1 2 4 6 8 10 12 14 # 1 2 4 6 8 # 20 28 40 54
			do
				# rm -f /tmp/*.socket
				ipcrm -M 0x00054321
				timeout 10m ${test[$i]} $t
                wait ;
                sleep 0.01
			done
			mv stats_file "${test_name[$i]}"_"$1".s"$a" 
		done
		# echo "${test[$i]}1:8 COMPLETE!\n" >> $1.txt
	done
}

LOG_SIZE=500000

##### MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=1 LOG_SIZE=10000000 PERIOD=1000" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-P.txt
##### run_bench test_REDO-TS-LLOG-P

##### MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=1 LOG_SIZE=10000 PERIOD=1000" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-SLOG-P.txt
##### run_bench test_REDO-TS-SLOG-P

### FORK
MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE THRESHOLD=0.0 \
SORT_ALG=4" ./build-stamp.sh htm-sgl-nvm REDO-COUNTER-LLOG-F.txt
run_bench REDO-COUNTER-LLOG-F

MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE THRESHOLD=0.0 \
SORT_ALG=4" ./build-stamp.sh htm-sgl-nvm REDO-TS-LLOG-F.txt
run_bench REDO-TS-LLOG-F

# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-R.txt
# run_bench test_REDO-TS-LLOG-R

# MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-COUNTER-LLOG-R.txt
# run_bench test_REDO-COUNTER-LLOG-R

##### MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=2 LOG_SIZE=10000 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-SLOG-R.txt
##### run_bench test_REDO-TS-SLOG-R

### With mallocs

# MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-COUNTER-LLOG-R-MALLOC.txt
# run_bench test_REDO-COUNTER-LLOG-R-MALLOC

# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=2 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-R-MALLOC.txt
# run_bench test_REDO-TS-LLOG-R-MALLOC

MAKEFILE_ARGS="SOLUTION=1" ./build-stamp.sh htm-sgl-nvm HTM.txt
run_bench HTM

MAKEFILE_ARGS="SOLUTION=2" ./build-stamp.sh htm-sgl-nvm AVNI.txt
run_bench AVNI

# MAKEFILE_ARGS="SOLUTION=3 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-COUNTER-LLOG-F-MALLOC.txt
# run_bench test_REDO-COUNTER-LLOG-F-MALLOC

# MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$LOG_SIZE USE_MALLOC=1 THRESHOLD=0.5" ./build-stamp.sh htm-sgl-nvm-pact test_REDO-TS-LLOG-F-MALLOC.txt
# run_bench test_REDO-TS-LLOG-F-MALLOC

