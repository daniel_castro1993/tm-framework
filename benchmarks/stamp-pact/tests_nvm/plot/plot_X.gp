set term postscript color eps enhanced 22
# ARG1 - PATH_TO_DATA
# ARG2 - BENCHMARK NAME
# ARG3 - BENCHMARK
set output sprintf("|ps2pdf -dEPSCrop - test_%s_X.pdf", ARG2)

set grid ytics
set grid xtics

#set logscale x 2
set mxtics

### TODO:

set key font ",16" outside right Left reverse width 4
set ylabel "Throughput"
set xlabel "Number of Threads"
set title font ",18" sprintf("%s", ARG3)

STM  =sprintf("%s/test_%s_STM.txt", ARG1, ARG2)
PSTM =sprintf("%s/test_%s_PSTM.txt", ARG1, ARG2)
HTM  =sprintf("%s/test_%s_HTM.txt", ARG1, ARG2)
PHTM =sprintf("%s/test_%s_PHTM.txt", ARG1, ARG2)
NVHTM_F_LC=sprintf("%s/test_%s_NVHTM_F_LC.txt", ARG1, ARG2)
NVHTM_B=sprintf("%s/test_%s_NVHTM_B.txt", ARG1, ARG2)
NVHTM_F=sprintf("%s/test_%s_NVHTM_F.txt", ARG1, ARG2)
NVHTM_W=sprintf("%s/test_%s_NVHTM_W.txt", ARG1, ARG2)

plot \
  STM         using ($1 - 0.05):6:($6-$12):($6+$12) title "STM" with yerrorbars lc 6, \
  STM         using 1:6                 notitle with lines lc 6, \
  PSTM        using ($1 - 0.025):6:($6-$12):($6+$12) title 'PSTM' with yerrorbars lc 7, \
  PSTM        using 1:6                 notitle with lines lc 7, \
  HTM         using ($1 - 0.05):6:($6-$12):($6+$12) title "HTM" with yerrorbars lc 1, \
  HTM         using 1:6                 notitle with lines lc 1, \
  PHTM        using ($1 - 0.025):6:($6-$12):($6+$12) title 'PHTM' with yerrorbars lc 2, \
  PHTM        using 1:6                 notitle with lines lc 2, \
  NVHTM_F     using 1:6:($6-$12):($6+$12) title 'NVHTM_F' with yerrorbars lc 3, \
  NVHTM_F     using 1:6                 notitle with lines lc 3, \
  NVHTM_B     using ($1 + 0.025):6:($6-$12):($6+$12) title 'NVHTM_B' with yerrorbars lc 4, \
  NVHTM_B     using 1:6                 notitle with lines lc 4, \
  NVHTM_W     using ($1 + 0.05):6:($6-$12):($6+$12) title 'NVHTM_W' with yerrorbars lc 5, \
  NVHTM_W     using 1:6                 notitle with lines lc 5, \
  