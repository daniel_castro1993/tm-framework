
hashmap:     file format elf64-powerpc


Disassembly of section .text:

0000000000000000 <._Z13hm_insert_htmP6List_tl>:

List** bucket;

TM_CALLABLE
long hm_insert_htm(TM_ARGDECL List* set, long val)
{
       0:	7c 08 02 a6 	mflr    r0
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
       4:	3d 22 00 00 	addis   r9,r2,0
       8:	fb a1 ff e8 	std     r29,-24(r1)
       c:	fb e1 ff f8 	std     r31,-8(r1)
      10:	e9 29 00 00 	ld      r9,0(r9)
      14:	fb c1 ff f0 	std     r30,-16(r1)
      18:	7d 29 6a 14 	add     r9,r9,r13
      1c:	f8 01 00 10 	std     r0,16(r1)
      20:	f8 21 ff 61 	stdu    r1,-160(r1)
      24:	81 29 00 00 	lwz     r9,0(r9)
        // traverse the list to find the insertion point
        Node_HM* prev = set->sentinel;
      28:	eb a3 00 00 	ld      r29,0(r3)
      2c:	2b 89 00 01 	cmplwi  cr7,r9,1
        Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);
      30:	eb fd 00 88 	ld      r31,136(r29)
      34:	41 9e 01 4c 	beq     cr7,180 <._Z13hm_insert_htmP6List_tl+0x180>

        while (curr != NULL) {
      38:	2f bf 00 00 	cmpdi   cr7,r31,0
      3c:	41 9e 00 d0 	beq     cr7,10c <._Z13hm_insert_htmP6List_tl+0x10c>
      40:	3c a2 00 00 	addis   r5,r2,0
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
      44:	3c c2 00 00 	addis   r6,r2,0
      48:	e8 a5 00 00 	ld      r5,0(r5)
      4c:	e8 c6 00 00 	ld      r6,0(r6)
      50:	7d 45 6a 14 	add     r10,r5,r13
      54:	7c c6 6a 14 	add     r6,r6,r13
      58:	38 61 00 78 	addi    r3,r1,120
      5c:	e9 0a 00 00 	ld      r8,0(r10)
      60:	48 00 00 2c 	b       8c <._Z13hm_insert_htmP6List_tl+0x8c>
      64:	60 00 00 00 	nop
      68:	60 00 00 00 	nop
      6c:	60 42 00 00 	ori     r2,r2,0
                if (FAST_PATH_SHARED_READ(curr->m_val) >= val)
      70:	7f 2a 20 00 	cmpd    cr6,r10,r4
      74:	40 98 00 3c 	bge     cr6,b0 <._Z13hm_insert_htmP6List_tl+0xb0>
      78:	7f fd fb 78 	mr      r29,r31
                        break;
                prev = curr;
                curr = FAST_PATH_SHARED_READ_P(prev->m_next);
      7c:	eb ff 00 88 	ld      r31,136(r31)
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
      80:	41 86 00 70 	beq     cr1,f0 <._Z13hm_insert_htmP6List_tl+0xf0>
{
        // traverse the list to find the insertion point
        Node_HM* prev = set->sentinel;
        Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);

        while (curr != NULL) {
      84:	2f bf 00 00 	cmpdi   cr7,r31,0
      88:	41 9e 00 7c 	beq     cr7,104 <._Z13hm_insert_htmP6List_tl+0x104>


inline intptr_t TxLoad(intptr_t var);
inline intptr_t TxLoad(intptr_t var){
	/*printf("read %d\n",*var)*/;
	if(local_exec_mode == 1) {
      8c:	2b 89 00 01 	cmplwi  cr7,r9,1
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
      90:	28 89 00 01 	cmplwi  cr1,r9,1
                if (FAST_PATH_SHARED_READ(curr->m_val) >= val)
      94:	e9 5f 00 00 	ld      r10,0(r31)


inline intptr_t TxLoad(intptr_t var);
inline intptr_t TxLoad(intptr_t var){
	/*printf("read %d\n",*var)*/;
	if(local_exec_mode == 1) {
      98:	40 9e ff d8 	bne     cr7,70 <._Z13hm_insert_htmP6List_tl+0x70>
      9c:	7f 2a 20 00 	cmpd    cr6,r10,r4
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
      a0:	79 07 1f 24 	rldicr  r7,r8,3,60
      a4:	39 08 00 01 	addi    r8,r8,1
      a8:	7c 66 39 2a 	stdx    r3,r6,r7
      ac:	41 98 ff cc 	blt     cr6,78 <._Z13hm_insert_htmP6List_tl+0x78>


inline intptr_t TxLoad(intptr_t var);
inline intptr_t TxLoad(intptr_t var){
	/*printf("read %d\n",*var)*/;
	if(local_exec_mode == 1) {
      b0:	2b 89 00 01 	cmplwi  cr7,r9,1
      b4:	7c a5 6a 14 	add     r5,r5,r13
      b8:	f9 41 00 78 	std     r10,120(r1)
      bc:	f9 41 00 70 	std     r10,112(r1)
      c0:	f9 05 00 00 	std     r8,0(r5)
      c4:	41 9e 00 8c 	beq     cr7,150 <._Z13hm_insert_htmP6List_tl+0x150>
                        break;
                prev = curr;
                curr = FAST_PATH_SHARED_READ_P(prev->m_next);
        }

        if (!curr || (FAST_PATH_SHARED_READ(curr->m_val) > val)) {
      c8:	41 99 00 48 	bgt     cr6,110 <._Z13hm_insert_htmP6List_tl+0x110>
                i->m_next = (Node_HM*)(curr);
                FAST_PATH_SHARED_WRITE_P(insert_point->m_next, i);
		return 1;
        }
	return 0;
}
      cc:	38 21 00 a0 	addi    r1,r1,160
                i->m_val = val;
                i->m_next = (Node_HM*)(curr);
                FAST_PATH_SHARED_WRITE_P(insert_point->m_next, i);
		return 1;
        }
	return 0;
      d0:	38 60 00 00 	li      r3,0
}
      d4:	e8 01 00 10 	ld      r0,16(r1)
      d8:	eb a1 ff e8 	ld      r29,-24(r1)
      dc:	eb c1 ff f0 	ld      r30,-16(r1)
      e0:	eb e1 ff f8 	ld      r31,-8(r1)
      e4:	7c 08 03 a6 	mtlr    r0
      e8:	4e 80 00 20 	blr
      ec:	60 42 00 00 	ori     r2,r2,0
{
        // traverse the list to find the insertion point
        Node_HM* prev = set->sentinel;
        Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);

        while (curr != NULL) {
      f0:	2f bf 00 00 	cmpdi   cr7,r31,0
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
      f4:	79 0a 1f 24 	rldicr  r10,r8,3,60
      f8:	39 08 00 01 	addi    r8,r8,1
      fc:	7f e6 51 2a 	stdx    r31,r6,r10
     100:	40 9e ff 8c 	bne     cr7,8c <._Z13hm_insert_htmP6List_tl+0x8c>
     104:	7c a5 6a 14 	add     r5,r5,r13
     108:	f9 05 00 00 	std     r8,0(r5)
     10c:	3b e0 00 00 	li      r31,0

        if (!curr || (FAST_PATH_SHARED_READ(curr->m_val) > val)) {
                Node_HM* insert_point = (Node_HM*)(prev);

                // create the new node
                Node_HM* i = (Node_HM*)TM_MALLOC(sizeof(Node_HM));
     110:	38 60 00 90 	li      r3,144
     114:	7c 9e 23 78 	mr      r30,r4
     118:	48 00 00 01 	bl      118 <._Z13hm_insert_htmP6List_tl+0x118>
     11c:	60 00 00 00 	nop
                i->m_next = (Node_HM*)(curr);
                FAST_PATH_SHARED_WRITE_P(insert_point->m_next, i);
		return 1;
        }
	return 0;
}
     120:	38 21 00 a0 	addi    r1,r1,160
     124:	e8 01 00 10 	ld      r0,16(r1)

        if (!curr || (FAST_PATH_SHARED_READ(curr->m_val) > val)) {
                Node_HM* insert_point = (Node_HM*)(prev);

                // create the new node
                Node_HM* i = (Node_HM*)TM_MALLOC(sizeof(Node_HM));
     128:	7c 69 1b 78 	mr      r9,r3
                i->m_val = val;
                i->m_next = (Node_HM*)(curr);
                FAST_PATH_SHARED_WRITE_P(insert_point->m_next, i);
		return 1;
     12c:	38 60 00 01 	li      r3,1
        if (!curr || (FAST_PATH_SHARED_READ(curr->m_val) > val)) {
                Node_HM* insert_point = (Node_HM*)(prev);

                // create the new node
                Node_HM* i = (Node_HM*)TM_MALLOC(sizeof(Node_HM));
                i->m_val = val;
     130:	fb c9 00 00 	std     r30,0(r9)
                i->m_next = (Node_HM*)(curr);
     134:	fb e9 00 88 	std     r31,136(r9)
                FAST_PATH_SHARED_WRITE_P(insert_point->m_next, i);
		return 1;
        }
	return 0;
}
     138:	7c 08 03 a6 	mtlr    r0

                // create the new node
                Node_HM* i = (Node_HM*)TM_MALLOC(sizeof(Node_HM));
                i->m_val = val;
                i->m_next = (Node_HM*)(curr);
                FAST_PATH_SHARED_WRITE_P(insert_point->m_next, i);
     13c:	f9 3d 00 88 	std     r9,136(r29)
		return 1;
        }
	return 0;
}
     140:	eb c1 ff f0 	ld      r30,-16(r1)
     144:	eb a1 ff e8 	ld      r29,-24(r1)
     148:	eb e1 ff f8 	ld      r31,-8(r1)
     14c:	4e 80 00 20 	blr
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     150:	3d 22 00 00 	addis   r9,r2,0
     154:	79 0a 1f 24 	rldicr  r10,r8,3,60
     158:	e9 29 00 00 	ld      r9,0(r9)
		rs_counter++;
     15c:	39 08 00 01 	addi    r8,r8,1
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     160:	7d 29 6a 14 	add     r9,r9,r13
     164:	38 e1 00 70 	addi    r7,r1,112
		rs_counter++;
     168:	f9 05 00 00 	std     r8,0(r5)
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     16c:	7c e9 51 2a 	stdx    r7,r9,r10
     170:	4b ff ff 58 	b       c8 <._Z13hm_insert_htmP6List_tl+0xc8>
     174:	60 00 00 00 	nop
     178:	60 00 00 00 	nop
     17c:	60 42 00 00 	ori     r2,r2,0
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     180:	3d 42 00 00 	addis   r10,r2,0
     184:	3d 02 00 00 	addis   r8,r2,0
     188:	e9 4a 00 00 	ld      r10,0(r10)
     18c:	e9 08 00 00 	ld      r8,0(r8)
     190:	7d 4a 6a 14 	add     r10,r10,r13
     194:	7d 08 6a 14 	add     r8,r8,r13
     198:	e8 ea 00 00 	ld      r7,0(r10)
     19c:	78 e6 1f 24 	rldicr  r6,r7,3,60
	                rs_counter++;
     1a0:	38 e7 00 01 	addi    r7,r7,1
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     1a4:	7f e8 31 2a 	stdx    r31,r8,r6
	                rs_counter++;
     1a8:	f8 ea 00 00 	std     r7,0(r10)
     1ac:	4b ff fe 8c 	b       38 <._Z13hm_insert_htmP6List_tl+0x38>
     1b0:	00 00 00 00 	.long 0x0
     1b4:	00 09 00 01 	.long 0x90001
     1b8:	80 03 00 00 	lwz     r0,0(r3)
     1bc:	60 42 00 00 	ori     r2,r2,0

00000000000001c0 <._Z13hm_insert_seqP6List_tl>:

void hm_insert_seq(List* set, long val)
{
     1c0:	7c 08 02 a6 	mflr    r0
     1c4:	fb c1 ff f0 	std     r30,-16(r1)
     1c8:	fb a1 ff e8 	std     r29,-24(r1)
     1cc:	fb e1 ff f8 	std     r31,-8(r1)
     1d0:	f8 01 00 10 	std     r0,16(r1)
     1d4:	f8 21 ff 71 	stdu    r1,-144(r1)
        Node_HM* prev = set->sentinel;
     1d8:	e9 43 00 00 	ld      r10,0(r3)
        Node_HM* curr = prev->m_next;
     1dc:	eb ca 00 88 	ld      r30,136(r10)

        while (curr != NULL) {
     1e0:	2f be 00 00 	cmpdi   cr7,r30,0
     1e4:	41 9e 00 a0 	beq     cr7,284 <._Z13hm_insert_seqP6List_tl+0xc4>
                if (curr->m_val >= val)
     1e8:	e9 3e 00 00 	ld      r9,0(r30)
     1ec:	7f a4 48 00 	cmpd    cr7,r4,r9
     1f0:	41 9d 00 20 	bgt     cr7,210 <._Z13hm_insert_seqP6List_tl+0x50>
     1f4:	48 00 00 98 	b       28c <._Z13hm_insert_seqP6List_tl+0xcc>
     1f8:	60 00 00 00 	nop
     1fc:	60 42 00 00 	ori     r2,r2,0
     200:	e9 3f 00 00 	ld      r9,0(r31)
     204:	7f a9 20 00 	cmpd    cr7,r9,r4
     208:	40 9c 00 58 	bge     cr7,260 <._Z13hm_insert_seqP6List_tl+0xa0>
     20c:	7f fe fb 78 	mr      r30,r31
                        break;
                prev = curr;
                curr = prev->m_next;
     210:	eb fe 00 88 	ld      r31,136(r30)
void hm_insert_seq(List* set, long val)
{
        Node_HM* prev = set->sentinel;
        Node_HM* curr = prev->m_next;

        while (curr != NULL) {
     214:	2f bf 00 00 	cmpdi   cr7,r31,0
     218:	40 9e ff e8 	bne     cr7,200 <._Z13hm_insert_seqP6List_tl+0x40>
     21c:	3b e0 00 00 	li      r31,0
        // now insert new_node between prev and curr
        if (!curr || (curr->m_val > val)) {
                Node_HM* insert_point = (Node_HM*)(prev);

                // create the new node
                Node_HM* i = (Node_HM*)malloc(sizeof(Node_HM));
     220:	38 60 00 90 	li      r3,144
     224:	7c 9d 23 78 	mr      r29,r4
     228:	48 00 00 01 	bl      228 <._Z13hm_insert_seqP6List_tl+0x68>
     22c:	60 00 00 00 	nop
                i->m_val = val;
                i->m_next = (Node_HM*)(curr);
                insert_point->m_next = i;
        }
}
     230:	38 21 00 90 	addi    r1,r1,144
     234:	e8 01 00 10 	ld      r0,16(r1)
        if (!curr || (curr->m_val > val)) {
                Node_HM* insert_point = (Node_HM*)(prev);

                // create the new node
                Node_HM* i = (Node_HM*)malloc(sizeof(Node_HM));
                i->m_val = val;
     238:	fb a3 00 00 	std     r29,0(r3)
                i->m_next = (Node_HM*)(curr);
                insert_point->m_next = i;
        }
}
     23c:	eb a1 ff e8 	ld      r29,-24(r1)
                Node_HM* insert_point = (Node_HM*)(prev);

                // create the new node
                Node_HM* i = (Node_HM*)malloc(sizeof(Node_HM));
                i->m_val = val;
                i->m_next = (Node_HM*)(curr);
     240:	fb e3 00 88 	std     r31,136(r3)
                insert_point->m_next = i;
     244:	f8 7e 00 88 	std     r3,136(r30)
        }
}
     248:	eb e1 ff f8 	ld      r31,-8(r1)
     24c:	eb c1 ff f0 	ld      r30,-16(r1)
     250:	7c 08 03 a6 	mtlr    r0
     254:	4e 80 00 20 	blr
     258:	60 00 00 00 	nop
     25c:	60 42 00 00 	ori     r2,r2,0
     260:	7f a4 48 00 	cmpd    cr7,r4,r9
                prev = curr;
                curr = prev->m_next;
        }

        // now insert new_node between prev and curr
        if (!curr || (curr->m_val > val)) {
     264:	41 9c ff bc 	blt     cr7,220 <._Z13hm_insert_seqP6List_tl+0x60>
                Node_HM* i = (Node_HM*)malloc(sizeof(Node_HM));
                i->m_val = val;
                i->m_next = (Node_HM*)(curr);
                insert_point->m_next = i;
        }
}
     268:	38 21 00 90 	addi    r1,r1,144
     26c:	e8 01 00 10 	ld      r0,16(r1)
     270:	eb a1 ff e8 	ld      r29,-24(r1)
     274:	eb c1 ff f0 	ld      r30,-16(r1)
     278:	eb e1 ff f8 	ld      r31,-8(r1)
     27c:	7c 08 03 a6 	mtlr    r0
     280:	4e 80 00 20 	blr
void hm_insert_seq(List* set, long val)
{
        Node_HM* prev = set->sentinel;
        Node_HM* curr = prev->m_next;

        while (curr != NULL) {
     284:	7d 5e 53 78 	mr      r30,r10
     288:	4b ff ff 94 	b       21c <._Z13hm_insert_seqP6List_tl+0x5c>
                if (curr->m_val >= val)
     28c:	7f df f3 78 	mr      r31,r30
     290:	7d 5e 53 78 	mr      r30,r10
                prev = curr;
                curr = prev->m_next;
        }

        // now insert new_node between prev and curr
        if (!curr || (curr->m_val > val)) {
     294:	41 9c ff 8c 	blt     cr7,220 <._Z13hm_insert_seqP6List_tl+0x60>
     298:	4b ff ff d0 	b       268 <._Z13hm_insert_seqP6List_tl+0xa8>
     29c:	00 00 00 00 	.long 0x0
     2a0:	00 09 00 01 	.long 0x90001
     2a4:	80 03 00 00 	lwz     r0,0(r3)
     2a8:	60 00 00 00 	nop
     2ac:	60 42 00 00 	ori     r2,r2,0

00000000000002b0 <._Z13hm_lookup_htmP6List_tl>:
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
     2b0:	3d 22 00 00 	addis   r9,r2,0

TM_CALLABLE
long hm_lookup_htm(TM_ARGDECL List* set, long val)
{
	int found = 0;
	const Node_HM* curr = set->sentinel;
     2b4:	e9 43 00 00 	ld      r10,0(r3)
     2b8:	e9 29 00 00 	ld      r9,0(r9)
     2bc:	7d 29 6a 14 	add     r9,r9,r13
     2c0:	81 09 00 00 	lwz     r8,0(r9)
	curr = FAST_PATH_SHARED_READ_P(curr->m_next);
     2c4:	e9 2a 00 88 	ld      r9,136(r10)
     2c8:	2b 88 00 01 	cmplwi  cr7,r8,1
     2cc:	41 9e 00 e4 	beq     cr7,3b0 <._Z13hm_lookup_htmP6List_tl+0x100>

	while (curr != NULL) {
     2d0:	2f a9 00 00 	cmpdi   cr7,r9,0
     2d4:	41 9e 00 c8 	beq     cr7,39c <._Z13hm_lookup_htmP6List_tl+0xec>
     2d8:	3c 62 00 00 	addis   r3,r2,0
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     2dc:	3c a2 00 00 	addis   r5,r2,0
     2e0:	e8 63 00 00 	ld      r3,0(r3)
     2e4:	e8 a5 00 00 	ld      r5,0(r5)
     2e8:	7d 43 6a 14 	add     r10,r3,r13
     2ec:	7c a5 6a 14 	add     r5,r5,r13
     2f0:	39 61 ff f8 	addi    r11,r1,-8
     2f4:	e8 ea 00 00 	ld      r7,0(r10)
     2f8:	48 00 00 20 	b       318 <._Z13hm_lookup_htmP6List_tl+0x68>
     2fc:	60 42 00 00 	ori     r2,r2,0
		if (FAST_PATH_SHARED_READ(curr->m_val) >= val)
     300:	7f aa 20 00 	cmpd    cr7,r10,r4
     304:	40 9c 00 38 	bge     cr7,33c <._Z13hm_lookup_htmP6List_tl+0x8c>
			break;
		curr = FAST_PATH_SHARED_READ_P(curr->m_next);
     308:	e9 29 00 88 	ld      r9,136(r9)
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
     30c:	41 9a 00 74 	beq     cr6,380 <._Z13hm_lookup_htmP6List_tl+0xd0>
{
	int found = 0;
	const Node_HM* curr = set->sentinel;
	curr = FAST_PATH_SHARED_READ_P(curr->m_next);

	while (curr != NULL) {
     310:	2f a9 00 00 	cmpdi   cr7,r9,0
     314:	41 9e 00 80 	beq     cr7,394 <._Z13hm_lookup_htmP6List_tl+0xe4>


inline intptr_t TxLoad(intptr_t var);
inline intptr_t TxLoad(intptr_t var){
	/*printf("read %d\n",*var)*/;
	if(local_exec_mode == 1) {
     318:	2b 88 00 01 	cmplwi  cr7,r8,1
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
     31c:	2b 08 00 01 	cmplwi  cr6,r8,1
		if (FAST_PATH_SHARED_READ(curr->m_val) >= val)
     320:	e9 49 00 00 	ld      r10,0(r9)


inline intptr_t TxLoad(intptr_t var);
inline intptr_t TxLoad(intptr_t var){
	/*printf("read %d\n",*var)*/;
	if(local_exec_mode == 1) {
     324:	40 9e ff dc 	bne     cr7,300 <._Z13hm_lookup_htmP6List_tl+0x50>
     328:	7f aa 20 00 	cmpd    cr7,r10,r4
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     32c:	78 e6 1f 24 	rldicr  r6,r7,3,60
     330:	38 e7 00 01 	addi    r7,r7,1
     334:	7d 65 31 2a 	stdx    r11,r5,r6
     338:	41 9c ff d0 	blt     cr7,308 <._Z13hm_lookup_htmP6List_tl+0x58>


inline intptr_t TxLoad(intptr_t var);
inline intptr_t TxLoad(intptr_t var){
	/*printf("read %d\n",*var)*/;
	if(local_exec_mode == 1) {
     33c:	2b 88 00 01 	cmplwi  cr7,r8,1
     340:	7c 63 6a 14 	add     r3,r3,r13
     344:	f8 e3 00 00 	std     r7,0(r3)
     348:	40 9e 00 24 	bne     cr7,36c <._Z13hm_lookup_htmP6List_tl+0xbc>
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     34c:	3d 22 00 00 	addis   r9,r2,0
     350:	78 e8 1f 24 	rldicr  r8,r7,3,60
     354:	e9 29 00 00 	ld      r9,0(r9)
		rs_counter++;
     358:	38 e7 00 01 	addi    r7,r7,1
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     35c:	7d 29 6a 14 	add     r9,r9,r13
     360:	38 c1 ff f0 	addi    r6,r1,-16
		rs_counter++;
     364:	f8 e3 00 00 	std     r7,0(r3)
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     368:	7c c9 41 2a 	stdx    r6,r9,r8
			break;
		curr = FAST_PATH_SHARED_READ_P(curr->m_next);
	}

	found = ((curr != NULL) && (FAST_PATH_SHARED_READ(curr->m_val) == val));
     36c:	7c 83 52 78 	xor     r3,r4,r10
     370:	7c 63 00 74 	cntlzd  r3,r3
     374:	78 63 d1 82 	rldicl  r3,r3,58,6
     378:	4e 80 00 20 	blr
     37c:	60 42 00 00 	ori     r2,r2,0
{
	int found = 0;
	const Node_HM* curr = set->sentinel;
	curr = FAST_PATH_SHARED_READ_P(curr->m_next);

	while (curr != NULL) {
     380:	2f a9 00 00 	cmpdi   cr7,r9,0
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     384:	78 ea 1f 24 	rldicr  r10,r7,3,60
     388:	38 e7 00 01 	addi    r7,r7,1
     38c:	7d 25 51 2a 	stdx    r9,r5,r10
     390:	40 9e ff 88 	bne     cr7,318 <._Z13hm_lookup_htmP6List_tl+0x68>
     394:	7c 63 6a 14 	add     r3,r3,r13
     398:	f8 e3 00 00 	std     r7,0(r3)
        }
}

TM_CALLABLE
long hm_lookup_htm(TM_ARGDECL List* set, long val)
{
     39c:	38 60 00 00 	li      r3,0
     3a0:	4e 80 00 20 	blr
     3a4:	60 00 00 00 	nop
     3a8:	60 00 00 00 	nop
     3ac:	60 42 00 00 	ori     r2,r2,0
     3b0:	3d 42 00 00 	addis   r10,r2,0
     3b4:	3c e2 00 00 	addis   r7,r2,0
     3b8:	e9 4a 00 00 	ld      r10,0(r10)
     3bc:	e8 e7 00 00 	ld      r7,0(r7)
     3c0:	7d 4a 6a 14 	add     r10,r10,r13
     3c4:	7c e7 6a 14 	add     r7,r7,r13
     3c8:	e8 ca 00 00 	ld      r6,0(r10)
     3cc:	78 c5 1f 24 	rldicr  r5,r6,3,60
	                rs_counter++;
     3d0:	38 c6 00 01 	addi    r6,r6,1
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     3d4:	7d 27 29 2a 	stdx    r9,r7,r5
	                rs_counter++;
     3d8:	f8 ca 00 00 	std     r6,0(r10)
     3dc:	4b ff fe f4 	b       2d0 <._Z13hm_lookup_htmP6List_tl+0x20>
     3e0:	00 00 00 00 	.long 0x0
     3e4:	00 09 00 00 	.long 0x90000
     3e8:	00 00 00 00 	.long 0x0
     3ec:	60 42 00 00 	ori     r2,r2,0

00000000000003f0 <._Z13hm_remove_htmP6List_tl>:
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
     3f0:	3d 22 00 00 	addis   r9,r2,0
}

TM_CALLABLE
int hm_remove_htm(TM_ARGDECL List* set, long val)
{
	Node_HM* prev = set->sentinel;
     3f4:	e8 a3 00 00 	ld      r5,0(r3)
     3f8:	e9 29 00 00 	ld      r9,0(r9)
     3fc:	7d 29 6a 14 	add     r9,r9,r13
     400:	81 09 00 00 	lwz     r8,0(r9)
	Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);
     404:	e9 25 00 88 	ld      r9,136(r5)
     408:	2b 88 00 01 	cmplwi  cr7,r8,1
     40c:	41 9e 01 24 	beq     cr7,530 <._Z13hm_remove_htmP6List_tl+0x140>
	while (curr != NULL) {
     410:	2f a9 00 00 	cmpdi   cr7,r9,0
     414:	41 9e 01 58 	beq     cr7,56c <._Z13hm_remove_htmP6List_tl+0x17c>
     418:	3d 82 00 00 	addis   r12,r2,0
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     41c:	3d 62 00 00 	addis   r11,r2,0
     420:	e8 61 ff e0 	ld      r3,-32(r1)
	return found;
}

TM_CALLABLE
int hm_remove_htm(TM_ARGDECL List* set, long val)
{
     424:	fb e1 ff f8 	std     r31,-8(r1)
     428:	e9 8c 00 00 	ld      r12,0(r12)
     42c:	e9 6b 00 00 	ld      r11,0(r11)
     430:	7d 4c 6a 14 	add     r10,r12,r13
     434:	7d 6b 6a 14 	add     r11,r11,r13
     438:	38 01 ff e8 	addi    r0,r1,-24
     43c:	3b e1 ff e0 	addi    r31,r1,-32
     440:	e8 ca 00 00 	ld      r6,0(r10)
     444:	60 00 00 00 	nop
     448:	60 00 00 00 	nop
     44c:	60 42 00 00 	ori     r2,r2,0


inline intptr_t TxLoad(intptr_t var);
inline intptr_t TxLoad(intptr_t var){
	/*printf("read %d\n",*var)*/;
	if(local_exec_mode == 1) {
     450:	2b 88 00 01 	cmplwi  cr7,r8,1
     454:	2b 08 00 01 	cmplwi  cr6,r8,1
	Node_HM* prev = set->sentinel;
	Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	while (curr != NULL) {

		if (FAST_PATH_SHARED_READ(curr->m_val) == val) {
     458:	e9 49 00 00 	ld      r10,0(r9)
     45c:	41 9e 00 44 	beq     cr7,4a0 <._Z13hm_remove_htmP6List_tl+0xb0>
     460:	7f aa 20 00 	cmpd    cr7,r10,r4
     464:	41 9e 00 50 	beq     cr7,4b4 <._Z13hm_remove_htmP6List_tl+0xc4>
     468:	41 9a 00 98 	beq     cr6,500 <._Z13hm_remove_htmP6List_tl+0x110>
			FAST_PATH_SHARED_WRITE_P(mod_point->m_next, FAST_PATH_SHARED_READ_P(curr->m_next));

			FAST_PATH_FREE((Node_HM*)(curr));
			return 1;
		}
		else if (FAST_PATH_SHARED_READ(curr->m_val) > val) {
     46c:	41 9d 00 1c 	bgt     cr7,488 <._Z13hm_remove_htmP6List_tl+0x98>
			return 0;
		}
		prev = curr;
		curr = FAST_PATH_SHARED_READ_P(prev->m_next);
     470:	e8 e9 00 88 	ld      r7,136(r9)
TM_CALLABLE
int hm_remove_htm(TM_ARGDECL List* set, long val)
{
	Node_HM* prev = set->sentinel;
	Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	while (curr != NULL) {
     474:	2f a7 00 00 	cmpdi   cr7,r7,0
     478:	7d 25 4b 78 	mr      r5,r9
     47c:	7d 43 53 78 	mr      r3,r10
     480:	7c e9 3b 78 	mr      r9,r7
     484:	40 9e ff cc 	bne     cr7,450 <._Z13hm_remove_htmP6List_tl+0x60>
     488:	7d 8c 6a 14 	add     r12,r12,r13
     48c:	f8 cc 00 00 	std     r6,0(r12)
		}
		prev = curr;
		curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	}
	return 0;
}
     490:	eb e1 ff f8 	ld      r31,-8(r1)
			return 0;
		}
		prev = curr;
		curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	}
	return 0;
     494:	38 60 00 00 	li      r3,0
}
     498:	4e 80 00 20 	blr
     49c:	60 42 00 00 	ori     r2,r2,0
{
	Node_HM* prev = set->sentinel;
	Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	while (curr != NULL) {

		if (FAST_PATH_SHARED_READ(curr->m_val) == val) {
     4a0:	7f aa 20 00 	cmpd    cr7,r10,r4
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     4a4:	78 c7 1f 24 	rldicr  r7,r6,3,60
     4a8:	38 c6 00 01 	addi    r6,r6,1
     4ac:	7c 0b 39 2a 	stdx    r0,r11,r7
     4b0:	40 9e ff b8 	bne     cr7,468 <._Z13hm_remove_htmP6List_tl+0x78>
}

inline intptr_t TxLoad_P(void* var);
inline intptr_t TxLoad_P(void* var){
	/*printf("p read %d\n",(long*)var)*/;
	if(local_exec_mode == 1) {
     4b4:	2b 88 00 01 	cmplwi  cr7,r8,1
     4b8:	7d 8c 6a 14 	add     r12,r12,r13
     4bc:	f9 41 ff e8 	std     r10,-24(r1)
     4c0:	f8 61 ff e0 	std     r3,-32(r1)
			Node_HM* mod_point = (Node_HM*)(prev);
			FAST_PATH_SHARED_WRITE_P(mod_point->m_next, FAST_PATH_SHARED_READ_P(curr->m_next));
     4c4:	e9 29 00 88 	ld      r9,136(r9)
     4c8:	f8 cc 00 00 	std     r6,0(r12)
     4cc:	40 9e 00 20 	bne     cr7,4ec <._Z13hm_remove_htmP6List_tl+0xfc>
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     4d0:	3d 42 00 00 	addis   r10,r2,0
     4d4:	78 c8 1f 24 	rldicr  r8,r6,3,60
     4d8:	e9 4a 00 00 	ld      r10,0(r10)
	                rs_counter++;
     4dc:	38 c6 00 01 	addi    r6,r6,1
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     4e0:	7d 4a 6a 14 	add     r10,r10,r13
	                rs_counter++;
     4e4:	f8 cc 00 00 	std     r6,0(r12)
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     4e8:	7d 2a 41 2a 	stdx    r9,r10,r8
		}
		prev = curr;
		curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	}
	return 0;
}
     4ec:	eb e1 ff f8 	ld      r31,-8(r1)
		if (FAST_PATH_SHARED_READ(curr->m_val) == val) {
			Node_HM* mod_point = (Node_HM*)(prev);
			FAST_PATH_SHARED_WRITE_P(mod_point->m_next, FAST_PATH_SHARED_READ_P(curr->m_next));

			FAST_PATH_FREE((Node_HM*)(curr));
			return 1;
     4f0:	38 60 00 01 	li      r3,1
	Node_HM* curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	while (curr != NULL) {

		if (FAST_PATH_SHARED_READ(curr->m_val) == val) {
			Node_HM* mod_point = (Node_HM*)(prev);
			FAST_PATH_SHARED_WRITE_P(mod_point->m_next, FAST_PATH_SHARED_READ_P(curr->m_next));
     4f4:	f9 25 00 88 	std     r9,136(r5)
		}
		prev = curr;
		curr = FAST_PATH_SHARED_READ_P(prev->m_next);
	}
	return 0;
}
     4f8:	4e 80 00 20 	blr
     4fc:	60 42 00 00 	ori     r2,r2,0
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     500:	78 c7 1f 24 	rldicr  r7,r6,3,60
		rs_counter++;
     504:	38 a6 00 01 	addi    r5,r6,1
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     508:	78 a3 1f 24 	rldicr  r3,r5,3,60
		/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		item->addr = var;
		item->type = 0;
	  	item->next = rot_readset->head;
		rot_readset->head = item;*/
		rot_readset[rs_counter] = &var;
     50c:	7f eb 39 2a 	stdx    r31,r11,r7
			FAST_PATH_SHARED_WRITE_P(mod_point->m_next, FAST_PATH_SHARED_READ_P(curr->m_next));

			FAST_PATH_FREE((Node_HM*)(curr));
			return 1;
		}
		else if (FAST_PATH_SHARED_READ(curr->m_val) > val) {
     510:	41 9d 00 50 	bgt     cr7,560 <._Z13hm_remove_htmP6List_tl+0x170>
			return 0;
		}
		prev = curr;
		curr = FAST_PATH_SHARED_READ_P(prev->m_next);
     514:	e8 e9 00 88 	ld      r7,136(r9)
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
	                rs_counter++;
     518:	38 c6 00 02 	addi    r6,r6,2
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     51c:	7c eb 19 2a 	stdx    r7,r11,r3
     520:	4b ff ff 54 	b       474 <._Z13hm_remove_htmP6List_tl+0x84>
     524:	60 00 00 00 	nop
     528:	60 00 00 00 	nop
     52c:	60 42 00 00 	ori     r2,r2,0
     530:	3d 42 00 00 	addis   r10,r2,0
     534:	3c e2 00 00 	addis   r7,r2,0
     538:	e9 4a 00 00 	ld      r10,0(r10)
     53c:	e8 e7 00 00 	ld      r7,0(r7)
     540:	7d 4a 6a 14 	add     r10,r10,r13
     544:	7c e7 6a 14 	add     r7,r7,r13
     548:	e8 ca 00 00 	ld      r6,0(r10)
     54c:	78 c3 1f 24 	rldicr  r3,r6,3,60
	                rs_counter++;
     550:	38 c6 00 01 	addi    r6,r6,1
			/*readset_item_t *item = (readset_item_t *)malloc(sizeof(readset_item_t));
		        item->addr_p = (long*)temp;;
			item->type = 1;
        		item->next = rot_readset->head;
	        	rot_readset->head = item;*/
			rot_readset[rs_counter] = var;
     554:	7d 27 19 2a 	stdx    r9,r7,r3
	                rs_counter++;
     558:	f8 ca 00 00 	std     r6,0(r10)
     55c:	4b ff fe b4 	b       410 <._Z13hm_remove_htmP6List_tl+0x20>
     560:	7d 8c 6a 14 	add     r12,r12,r13
     564:	f8 ac 00 00 	std     r5,0(r12)
     568:	4b ff ff 28 	b       490 <._Z13hm_remove_htmP6List_tl+0xa0>
	}
	return 0;
     56c:	38 60 00 00 	li      r3,0
}
     570:	4e 80 00 20 	blr
     574:	00 00 00 00 	.long 0x0
     578:	00 09 00 00 	.long 0x90000
     57c:	00 01 00 00 	.long 0x10000

0000000000000580 <._Z15priv_insert_html>:


TM_CALLABLE
long priv_insert_htm(TM_ARGDECL long val)
{
    return hm_insert_htm(TM_ARG (bucket[val % N_BUCKETS]), val);
     580:	3d 02 00 00 	addis   r8,r2,0
     584:	3d 42 00 00 	addis   r10,r2,0
     588:	e9 4a 00 00 	ld      r10,0(r10)
     58c:	7c 64 1b 78 	mr      r4,r3
     590:	e9 28 00 02 	lwa     r9,0(r8)
     594:	7d 03 4b d2 	divd    r8,r3,r9
     598:	7d 28 49 d2 	mulld   r9,r8,r9
     59c:	7c 69 18 50 	subf    r3,r9,r3
     5a0:	78 63 1f 24 	rldicr  r3,r3,3,60
     5a4:	7c 6a 18 2a 	ldx     r3,r10,r3
     5a8:	48 00 00 00 	b       5a8 <._Z15priv_insert_html+0x28>
     5ac:	00 00 00 00 	.long 0x0
     5b0:	00 09 00 00 	.long 0x90000
     5b4:	00 00 00 00 	.long 0x0
     5b8:	60 00 00 00 	nop
     5bc:	60 42 00 00 	ori     r2,r2,0

00000000000005c0 <._Z15priv_insert_seql>:
}

void priv_insert_seq(long val)
{
	hm_insert_seq( (bucket[val % N_BUCKETS]), val);
     5c0:	3d 02 00 00 	addis   r8,r2,0
     5c4:	3d 42 00 00 	addis   r10,r2,0
     5c8:	e9 4a 00 00 	ld      r10,0(r10)
     5cc:	7c 64 1b 78 	mr      r4,r3
     5d0:	e9 28 00 02 	lwa     r9,0(r8)
     5d4:	7d 03 4b d2 	divd    r8,r3,r9
     5d8:	7d 28 49 d2 	mulld   r9,r8,r9
     5dc:	7c 69 18 50 	subf    r3,r9,r3
     5e0:	78 63 1f 24 	rldicr  r3,r3,3,60
     5e4:	7c 6a 18 2a 	ldx     r3,r10,r3
     5e8:	48 00 00 00 	b       5e8 <._Z15priv_insert_seql+0x28>
     5ec:	00 00 00 00 	.long 0x0
     5f0:	00 09 00 00 	.long 0x90000
     5f4:	00 00 00 00 	.long 0x0
     5f8:	60 00 00 00 	nop
     5fc:	60 42 00 00 	ori     r2,r2,0

0000000000000600 <._Z15priv_lookup_html>:
}

TM_CALLABLE
long priv_lookup_htm(TM_ARGDECL long val)
{
    return hm_lookup_htm(TM_ARG (bucket[val % N_BUCKETS]), val);
     600:	3d 02 00 00 	addis   r8,r2,0
     604:	3d 42 00 00 	addis   r10,r2,0
     608:	e9 4a 00 00 	ld      r10,0(r10)
     60c:	7c 64 1b 78 	mr      r4,r3
     610:	e9 28 00 02 	lwa     r9,0(r8)
     614:	7d 03 4b d2 	divd    r8,r3,r9
     618:	7d 28 49 d2 	mulld   r9,r8,r9
     61c:	7c 69 18 50 	subf    r3,r9,r3
     620:	78 63 1f 24 	rldicr  r3,r3,3,60
     624:	7c 6a 18 2a 	ldx     r3,r10,r3
     628:	48 00 00 00 	b       628 <._Z15priv_lookup_html+0x28>
     62c:	00 00 00 00 	.long 0x0
     630:	00 09 00 00 	.long 0x90000
     634:	00 00 00 00 	.long 0x0
     638:	60 00 00 00 	nop
     63c:	60 42 00 00 	ori     r2,r2,0

0000000000000640 <._Z20priv_remove_item_html>:
}

TM_CALLABLE
int priv_remove_item_htm(TM_ARGDECL long val)
{
    return hm_remove_htm(TM_ARG (bucket[val % N_BUCKETS]), val);
     640:	3d 02 00 00 	addis   r8,r2,0
     644:	3d 42 00 00 	addis   r10,r2,0
     648:	e9 4a 00 00 	ld      r10,0(r10)
     64c:	7c 64 1b 78 	mr      r4,r3
     650:	e9 28 00 02 	lwa     r9,0(r8)
     654:	7d 03 4b d2 	divd    r8,r3,r9
     658:	7d 28 49 d2 	mulld   r9,r8,r9
     65c:	7c 69 18 50 	subf    r3,r9,r3
     660:	78 63 1f 24 	rldicr  r3,r3,3,60
     664:	7c 6a 18 2a 	ldx     r3,r10,r3
     668:	48 00 00 00 	b       668 <._Z20priv_remove_item_html+0x28>
     66c:	00 00 00 00 	.long 0x0
     670:	00 09 00 00 	.long 0x90000
     674:	00 00 00 00 	.long 0x0
     678:	60 00 00 00 	nop
     67c:	60 42 00 00 	ori     r2,r2,0

0000000000000680 <._Z11set_add_seql>:
}


long set_add_seq(long val) {
     680:	7c 08 02 a6 	mflr    r0
     684:	f8 01 00 10 	std     r0,16(r1)
     688:	f8 21 ff 91 	stdu    r1,-112(r1)
	priv_insert_seq(val);
     68c:	48 00 00 01 	bl      68c <._Z11set_add_seql+0xc>
 return 1;
}
     690:	38 21 00 70 	addi    r1,r1,112
     694:	38 60 00 01 	li      r3,1
     698:	e8 01 00 10 	ld      r0,16(r1)
     69c:	7c 08 03 a6 	mtlr    r0
     6a0:	4e 80 00 20 	blr
     6a4:	00 00 00 00 	.long 0x0
     6a8:	00 09 00 01 	.long 0x90001
     6ac:	80 00 00 00 	lwz     r0,0(0)

00000000000006b0 <._Z7set_addl>:

long set_add(TM_ARGDECL long val)
{
     6b0:	7c 08 02 a6 	mflr    r0
     6b4:	fb 21 ff c8 	std     r25,-56(r1)
     6b8:	fb 41 ff d0 	std     r26,-48(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     6bc:	39 40 00 00 	li      r10,0
	priv_insert_seq(val);
 return 1;
}

long set_add(TM_ARGDECL long val)
{
     6c0:	fb 61 ff d8 	std     r27,-40(r1)
     6c4:	fb 81 ff e0 	std     r28,-32(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     6c8:	3f 62 00 00 	addis   r27,r2,0
	priv_insert_seq(val);
 return 1;
}

long set_add(TM_ARGDECL long val)
{
     6cc:	7c 79 1b 78 	mr      r25,r3
     6d0:	fb c1 ff f0 	std     r30,-16(r1)
     6d4:	fb e1 ff f8 	std     r31,-8(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     6d8:	eb 7b 00 00 	ld      r27,0(r27)
     6dc:	3f 82 00 00 	addis   r28,r2,0
	priv_insert_seq(val);
 return 1;
}

long set_add(TM_ARGDECL long val)
{
     6e0:	fa e1 ff b8 	std     r23,-72(r1)
     6e4:	fb 01 ff c0 	std     r24,-64(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     6e8:	eb 9c 00 00 	ld      r28,0(r28)
	priv_insert_seq(val);
 return 1;
}

long set_add(TM_ARGDECL long val)
{
     6ec:	fb a1 ff e8 	std     r29,-24(r1)
     6f0:	f8 01 00 10 	std     r0,16(r1)
     6f4:	f8 21 fc b1 	stdu    r1,-848(r1)
     6f8:	e9 21 00 00 	ld      r9,0(r1)
     6fc:	7c 3f 0b 78 	mr      r31,r1
     700:	f9 21 fe 81 	stdu    r9,-384(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     704:	7d 3b 6a 14 	add     r9,r27,r13
     708:	91 49 00 00 	stw     r10,0(r9)
     70c:	48 00 00 01 	bl      70c <._Z7set_addl+0x5c>
     710:	60 00 00 00 	nop
     714:	3f c2 00 00 	addis   r30,r2,0
     718:	eb de 00 00 	ld      r30,0(r30)
     71c:	7d 5c 6a 14 	add     r10,r28,r13
	priv_insert_seq(val);
 return 1;
}

long set_add(TM_ARGDECL long val)
{
     720:	3b 41 00 ef 	addi    r26,r1,239
     724:	7b 5a 06 24 	rldicr  r26,r26,0,56
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     728:	90 6a 00 00 	stw     r3,0(r10)
     72c:	81 3e 00 00 	lwz     r9,0(r30)
     730:	2f 89 00 00 	cmpwi   cr7,r9,0
     734:	41 9e 00 18 	beq     cr7,74c <._Z7set_addl+0x9c>
     738:	60 00 00 00 	nop
     73c:	60 42 00 00 	ori     r2,r2,0
     740:	81 3e 00 00 	lwz     r9,0(r30)
     744:	2f 89 00 00 	cmpwi   cr7,r9,0
     748:	40 9e ff f8 	bne     cr7,740 <._Z7set_addl+0x90>
	priv_insert_seq(val);
 return 1;
}

long set_add(TM_ARGDECL long val)
{
     74c:	39 00 00 04 	li      r8,4
     750:	3f 02 00 00 	addis   r24,r2,0
     754:	eb 18 00 00 	ld      r24,0(r24)
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     758:	7c fc 6a 14 	add     r7,r28,r13
     75c:	48 00 00 04 	b       760 <._Z7set_addl+0xb0>
     760:	81 3e 00 00 	lwz     r9,0(r30)
     764:	2f 89 00 00 	cmpwi   cr7,r9,0
     768:	40 9e ff f8 	bne     cr7,760 <._Z7set_addl+0xb0>
extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_begin (void* const TM_buff)
{
  *_TEXASRL_PTR (TM_buff) = 0;
  if (__builtin_expect (__builtin_tbegin (0), 1))
     76c:	7c 00 05 1d 	tbegin.
     770:	41 82 00 9c 	beq     80c <._Z7set_addl+0x15c>
     774:	81 3e 00 00 	lwz     r9,0(r30)
     778:	2f 89 00 00 	cmpwi   cr7,r9,0
     77c:	40 9e 08 34 	bne     cr7,fb0 <._Z7set_addl+0x900>
    res = priv_insert_htm(TM_ARG val);
    TM_END();
     780:	7f 7b 6a 14 	add     r27,r27,r13
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
    res = priv_insert_htm(TM_ARG val);
     784:	7f 23 cb 78 	mr      r3,r25
     788:	48 00 00 01 	bl      788 <._Z7set_addl+0xd8>
    TM_END();
     78c:	81 3b 00 00 	lwz     r9,0(r27)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
    res = priv_insert_htm(TM_ARG val);
     790:	7c 7b 07 b4 	extsw   r27,r3
    TM_END();
     794:	2f a9 00 00 	cmpdi   cr7,r9,0
     798:	41 9e 02 80 	beq     cr7,a18 <._Z7set_addl+0x368>
     79c:	2b 89 00 01 	cmplwi  cr7,r9,1
     7a0:	41 9e 05 64 	beq     cr7,d04 <._Z7set_addl+0x654>
     7a4:	7f 9c 6a 14 	add     r28,r28,r13
     7a8:	7f c3 f3 78 	mr      r3,r30
     7ac:	81 3c 00 00 	lwz     r9,0(r28)
     7b0:	79 2a 3e 24 	rldicr  r10,r9,7,56
     7b4:	79 29 4d a4 	rldicr  r9,r9,9,54
     7b8:	7d 2a 48 50 	subf    r9,r10,r9
     7bc:	7d 38 4a 14 	add     r9,r24,r9
     7c0:	e9 49 00 98 	ld      r10,152(r9)
     7c4:	39 4a 00 01 	addi    r10,r10,1
     7c8:	f9 49 00 98 	std     r10,152(r9)
     7cc:	48 00 00 01 	bl      7cc <._Z7set_addl+0x11c>
     7d0:	60 00 00 00 	nop

    return res;
}
     7d4:	38 3f 03 50 	addi    r1,r31,848
     7d8:	7f 63 db 78 	mr      r3,r27
     7dc:	e8 01 00 10 	ld      r0,16(r1)
     7e0:	ea e1 ff b8 	ld      r23,-72(r1)
     7e4:	eb 01 ff c0 	ld      r24,-64(r1)
     7e8:	eb 21 ff c8 	ld      r25,-56(r1)
     7ec:	eb 41 ff d0 	ld      r26,-48(r1)
     7f0:	eb 61 ff d8 	ld      r27,-40(r1)
     7f4:	eb 81 ff e0 	ld      r28,-32(r1)
     7f8:	eb a1 ff e8 	ld      r29,-24(r1)
     7fc:	eb c1 ff f0 	ld      r30,-16(r1)
     800:	eb e1 ff f8 	ld      r31,-8(r1)
     804:	7c 08 03 a6 	mtlr    r0
     808:	4e 80 00 20 	blr
    return _HTM_TBEGIN_STARTED;
#ifdef __powerpc64__
  *_TEXASR_PTR (TM_buff) = __builtin_get_texasr ();
     80c:	7d 22 22 a6 	mfspr   r9,130
#else
  *_TEXASRU_PTR (TM_buff) = __builtin_get_texasru ();
  *_TEXASRL_PTR (TM_buff) = __builtin_get_texasr ();
#endif
  *_TFIAR_PTR (TM_buff) = __builtin_get_tfiar ();
     810:	7d 41 22 a6 	mfspr   r10,129

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_conflict(void* const TM_buff)
{
  texasru_t texasru = *_TEXASRU_PTR (TM_buff);
     814:	79 29 00 22 	rldicl  r9,r9,32,32
  /* Return TEXASR bits 11 (Self-Induced Conflict) through
     14 (Translation Invalidation Conflict).  */
  return (_TEXASRU_EXTRACT_BITS (texasru, 14, 4)) ? 1 : 0;
     818:	79 25 7f 23 	rldicl. r5,r9,47,60
     81c:	41 82 03 5c 	beq     b78 <._Z7set_addl+0x4c8>
long set_add(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     820:	81 47 00 00 	lwz     r10,0(r7)
     824:	79 49 3e 24 	rldicr  r9,r10,7,56
     828:	79 4a 4d a4 	rldicr  r10,r10,9,54
     82c:	7d 29 50 50 	subf    r9,r9,r10
     830:	7d 38 4a 14 	add     r9,r24,r9
     834:	e9 49 00 10 	ld      r10,16(r9)
     838:	39 4a 00 01 	addi    r10,r10,1
     83c:	f9 49 00 10 	std     r10,16(r9)

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_self_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
     840:	7d 42 22 a6 	mfspr   r10,130
     844:	79 45 67 e1 	rldicl. r5,r10,12,63
     848:	41 82 03 cc 	beq     c14 <._Z7set_addl+0x564>
     84c:	e9 49 00 18 	ld      r10,24(r9)
     850:	39 4a 00 01 	addi    r10,r10,1
     854:	f9 49 00 18 	std     r10,24(r9)
     858:	39 28 ff ff 	addi    r9,r8,-1
     85c:	2f 89 ff ff 	cmpwi   cr7,r9,-1
     860:	79 28 00 20 	clrldi  r8,r9,32
     864:	40 9e fe fc 	bne     cr7,760 <._Z7set_addl+0xb0>
     868:	81 3e 00 00 	lwz     r9,0(r30)
     86c:	7d 5b 6a 14 	add     r10,r27,r13
     870:	39 00 00 01 	li      r8,1
     874:	91 0a 00 00 	stw     r8,0(r10)
     878:	2f 89 00 00 	cmpwi   cr7,r9,0
     87c:	41 9e 00 10 	beq     cr7,88c <._Z7set_addl+0x1dc>
     880:	81 3e 00 00 	lwz     r9,0(r30)
     884:	2f 89 00 00 	cmpwi   cr7,r9,0
     888:	40 9e ff f8 	bne     cr7,880 <._Z7set_addl+0x1d0>
     88c:	3c c2 00 00 	addis   r6,r2,0
     890:	3f a2 00 00 	addis   r29,r2,0
     894:	eb bd 00 00 	ld      r29,0(r29)
	priv_insert_seq(val);
 return 1;
}

long set_add(TM_ARGDECL long val)
{
     898:	39 00 00 05 	li      r8,5
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     89c:	e8 c6 00 00 	ld      r6,0(r6)
     8a0:	7c fc 6a 14 	add     r7,r28,r13
     8a4:	7c c6 6a 14 	add     r6,r6,r13
     8a8:	60 00 00 00 	nop
     8ac:	60 42 00 00 	ori     r2,r2,0
     8b0:	81 27 00 00 	lwz     r9,0(r7)
     8b4:	79 29 45 e4 	rldicr  r9,r9,8,55
     8b8:	7c bd 4a 14 	add     r5,r29,r9
     8bc:	7d 5d 48 2a 	ldx     r10,r29,r9
     8c0:	79 44 07 e1 	clrldi. r4,r10,63
     8c4:	40 82 01 cc 	bne     a90 <._Z7set_addl+0x3e0>
     8c8:	7d 5d 48 2a 	ldx     r10,r29,r9
     8cc:	39 4a 00 03 	addi    r10,r10,3
     8d0:	7d 5d 49 2a 	stdx    r10,r29,r9
     8d4:	7c 00 04 ac 	sync
     8d8:	81 3e 00 00 	lwz     r9,0(r30)
     8dc:	7d 2a 07 b5 	extsw.  r10,r9
     8e0:	41 82 01 d8 	beq     ab8 <._Z7set_addl+0x408>
     8e4:	81 27 00 00 	lwz     r9,0(r7)
     8e8:	79 29 45 e4 	rldicr  r9,r9,8,55
     8ec:	7d 5d 48 2a 	ldx     r10,r29,r9
     8f0:	39 4a 00 05 	addi    r10,r10,5
     8f4:	7d 5d 49 2a 	stdx    r10,r29,r9
     8f8:	7c 00 04 ac 	sync
     8fc:	81 3e 00 00 	lwz     r9,0(r30)
     900:	2f 89 00 00 	cmpwi   cr7,r9,0
     904:	41 9e 00 18 	beq     cr7,91c <._Z7set_addl+0x26c>
     908:	60 00 00 00 	nop
     90c:	60 42 00 00 	ori     r2,r2,0
     910:	81 3e 00 00 	lwz     r9,0(r30)
     914:	2f 89 00 00 	cmpwi   cr7,r9,0
     918:	40 9e ff f8 	bne     cr7,910 <._Z7set_addl+0x260>
     91c:	39 20 00 01 	li      r9,1
     920:	2f 88 00 00 	cmpwi   cr7,r8,0
     924:	41 9d ff 8c 	bgt     cr7,8b0 <._Z7set_addl+0x200>
     928:	2f a9 00 00 	cmpdi   cr7,r9,0
     92c:	40 9e fe 54 	bne     cr7,780 <._Z7set_addl+0xd0>
     930:	7d 3c 6a 14 	add     r9,r28,r13
     934:	7d 5b 6a 14 	add     r10,r27,r13
     938:	39 00 00 02 	li      r8,2
     93c:	81 29 00 00 	lwz     r9,0(r9)
     940:	91 0a 00 00 	stw     r8,0(r10)
     944:	79 29 45 e4 	rldicr  r9,r9,8,55
     948:	7d 5d 48 2a 	ldx     r10,r29,r9
     94c:	79 45 07 61 	clrldi. r5,r10,61
     950:	7d 5d 48 2a 	ldx     r10,r29,r9
     954:	40 82 05 74 	bne     ec8 <._Z7set_addl+0x818>
     958:	79 46 07 a1 	clrldi. r6,r10,62
     95c:	41 82 05 c0 	beq     f1c <._Z7set_addl+0x86c>
     960:	7d 3d 48 2a 	ldx     r9,r29,r9
     964:	7c 00 04 ac 	sync
     968:	7f d7 f3 78 	mr      r23,r30
     96c:	60 42 00 00 	ori     r2,r2,0
     970:	81 3e 00 00 	lwz     r9,0(r30)
     974:	7e e3 bb 78 	mr      r3,r23
     978:	2f 89 00 00 	cmpwi   cr7,r9,0
     97c:	40 9e 05 64 	bne     cr7,ee0 <._Z7set_addl+0x830>
     980:	48 00 00 01 	bl      980 <._Z7set_addl+0x2d0>
     984:	60 00 00 00 	nop
     988:	2f a3 00 00 	cmpdi   cr7,r3,0
     98c:	40 9e ff e4 	bne     cr7,970 <._Z7set_addl+0x2c0>
     990:	3d 22 00 00 	addis   r9,r2,0
     994:	e9 29 00 00 	ld      r9,0(r9)
     998:	39 00 00 01 	li      r8,1
     99c:	e9 29 00 00 	ld      r9,0(r9)
     9a0:	f9 3a 00 00 	std     r9,0(r26)
     9a4:	60 42 00 00 	ori     r2,r2,0
     9a8:	e9 3a 00 00 	ld      r9,0(r26)
     9ac:	2f a9 00 00 	cmpdi   cr7,r9,0
     9b0:	40 9d fd d0 	ble     cr7,780 <._Z7set_addl+0xd0>
     9b4:	60 00 00 00 	nop
     9b8:	60 00 00 00 	nop
     9bc:	60 42 00 00 	ori     r2,r2,0
     9c0:	39 48 ff ff 	addi    r10,r8,-1
     9c4:	7d 4a 07 b4 	extsw   r10,r10
     9c8:	79 4a 45 e4 	rldicr  r10,r10,8,55
     9cc:	7d 3d 50 2a 	ldx     r9,r29,r10
     9d0:	79 25 07 e1 	clrldi. r5,r9,63
     9d4:	41 82 00 18 	beq     9ec <._Z7set_addl+0x33c>
     9d8:	60 00 00 00 	nop
     9dc:	60 42 00 00 	ori     r2,r2,0
     9e0:	7d 3d 50 2a 	ldx     r9,r29,r10
     9e4:	79 24 07 e1 	clrldi. r4,r9,63
     9e8:	40 82 ff f8 	bne     9e0 <._Z7set_addl+0x330>
     9ec:	e9 5a 00 00 	ld      r10,0(r26)
     9f0:	7f a8 50 00 	cmpd    cr7,r8,r10
     9f4:	39 08 00 01 	addi    r8,r8,1
     9f8:	41 9c ff c8 	blt     cr7,9c0 <._Z7set_addl+0x310>
    res = priv_insert_htm(TM_ARG val);
    TM_END();
     9fc:	7f 7b 6a 14 	add     r27,r27,r13
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
    res = priv_insert_htm(TM_ARG val);
     a00:	7f 23 cb 78 	mr      r3,r25
     a04:	48 00 00 01 	bl      a04 <._Z7set_addl+0x354>
    TM_END();
     a08:	81 3b 00 00 	lwz     r9,0(r27)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
    res = priv_insert_htm(TM_ARG val);
     a0c:	7c 7b 07 b4 	extsw   r27,r3
    TM_END();
     a10:	2f a9 00 00 	cmpdi   cr7,r9,0
     a14:	40 9e fd 88 	bne     cr7,79c <._Z7set_addl+0xec>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_suspend (void)
{
  __builtin_tsuspend ();
     a18:	7c 00 05 dd 	tsuspend.
     a1c:	3d 22 00 00 	addis   r9,r2,0
     a20:	e9 29 00 00 	ld      r9,0(r9)
     a24:	e8 a9 00 00 	ld      r5,0(r9)
     a28:	2f a5 00 00 	cmpdi   cr7,r5,0
     a2c:	41 9e 02 58 	beq     cr7,c84 <._Z7set_addl+0x5d4>
     a30:	39 20 00 50 	li      r9,80
     a34:	3f a2 00 00 	addis   r29,r2,0
     a38:	eb bd 00 00 	ld      r29,0(r29)
     a3c:	38 e0 00 00 	li      r7,0
     a40:	7d 29 03 a6 	mtctr   r9
     a44:	39 00 00 00 	li      r8,0
     a48:	38 df 00 70 	addi    r6,r31,112
     a4c:	48 00 00 20 	b       a6c <._Z7set_addl+0x3bc>
     a50:	e9 3f 02 f8 	ld      r9,760(r31)
     a54:	7d 26 39 2a 	stdx    r9,r6,r7
     a58:	39 08 00 01 	addi    r8,r8,1
     a5c:	7f a5 40 00 	cmpd    cr7,r5,r8
     a60:	42 40 01 d0 	bdz     c30 <._Z7set_addl+0x580>
     a64:	38 e7 00 08 	addi    r7,r7,8
     a68:	41 9e 01 c8 	beq     cr7,c30 <._Z7set_addl+0x580>
     a6c:	79 09 45 e4 	rldicr  r9,r8,8,55
     a70:	7d 3d 48 2a 	ldx     r9,r29,r9
     a74:	f9 3f 02 f8 	std     r9,760(r31)
     a78:	60 42 00 00 	ori     r2,r2,0
     a7c:	e9 3f 02 f8 	ld      r9,760(r31)
     a80:	79 2a 07 e1 	clrldi. r10,r9,63
     a84:	40 82 ff cc 	bne     a50 <._Z7set_addl+0x3a0>
     a88:	7d 46 39 2a 	stdx    r10,r6,r7
     a8c:	4b ff ff cc 	b       a58 <._Z7set_addl+0x3a8>
long set_add(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     a90:	7d 3d 48 2a 	ldx     r9,r29,r9
     a94:	79 2a 07 61 	clrldi. r10,r9,61
     a98:	e9 25 00 00 	ld      r9,0(r5)
     a9c:	41 82 00 c4 	beq     b60 <._Z7set_addl+0x4b0>
     aa0:	39 29 00 04 	addi    r9,r9,4
     aa4:	f9 25 00 00 	std     r9,0(r5)
     aa8:	7c 00 04 ac 	sync
     aac:	81 3e 00 00 	lwz     r9,0(r30)
     ab0:	7d 2a 07 b5 	extsw.  r10,r9
     ab4:	40 82 fe 30 	bne     8e4 <._Z7set_addl+0x234>
     ab8:	f9 46 00 00 	std     r10,0(r6)
extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_begin_rot (void* const TM_buff)
{
  *_TEXASRL_PTR (TM_buff) = 0;
  if (__builtin_expect (__builtin_tbegin (1), 1)){
     abc:	7c 20 05 1d 	tbegin. 1
     ac0:	40 82 fc c0 	bne     780 <._Z7set_addl+0xd0>
    return _HTM_TBEGIN_STARTED;
  }
#ifdef __powerpc64__
  *_TEXASR_PTR (TM_buff) = __builtin_get_texasr ();
     ac4:	7d 22 22 a6 	mfspr   r9,130
#else
  *_TEXASRU_PTR (TM_buff) = __builtin_get_texasru ();
  *_TEXASRL_PTR (TM_buff) = __builtin_get_texasr ();
#endif
  *_TFIAR_PTR (TM_buff) = __builtin_get_tfiar ();
     ac8:	7d 41 22 a6 	mfspr   r10,129

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_conflict(void* const TM_buff)
{
  texasru_t texasru = *_TEXASRU_PTR (TM_buff);
     acc:	79 29 00 22 	rldicl  r9,r9,32,32
  /* Return TEXASR bits 11 (Self-Induced Conflict) through
     14 (Translation Invalidation Conflict).  */
  return (_TEXASRU_EXTRACT_BITS (texasru, 14, 4)) ? 1 : 0;
     ad0:	79 25 7f 23 	rldicl. r5,r9,47,60
     ad4:	41 82 00 4c 	beq     b20 <._Z7set_addl+0x470>
     ad8:	81 47 00 00 	lwz     r10,0(r7)
     adc:	79 49 3e 24 	rldicr  r9,r10,7,56
     ae0:	79 4a 4d a4 	rldicr  r10,r10,9,54
     ae4:	7d 29 50 50 	subf    r9,r9,r10
     ae8:	7d 38 4a 14 	add     r9,r24,r9
     aec:	e9 49 00 58 	ld      r10,88(r9)
     af0:	39 4a 00 01 	addi    r10,r10,1
     af4:	f9 49 00 58 	std     r10,88(r9)

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_self_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
     af8:	7d 42 22 a6 	mfspr   r10,130
     afc:	79 44 67 e1 	rldicl. r4,r10,12,63
     b00:	41 82 01 e8 	beq     ce8 <._Z7set_addl+0x638>
     b04:	e9 49 00 60 	ld      r10,96(r9)
     b08:	39 4a 00 01 	addi    r10,r10,1
     b0c:	f9 49 00 60 	std     r10,96(r9)
     b10:	39 08 ff ff 	addi    r8,r8,-1
     b14:	39 20 00 00 	li      r9,0
     b18:	7d 08 07 b4 	extsw   r8,r8
     b1c:	4b ff fe 04 	b       920 <._Z7set_addl+0x270>
     b20:	79 2a 07 e1 	clrldi. r10,r9,63
     b24:	41 82 00 b8 	beq     bdc <._Z7set_addl+0x52c>
     b28:	81 27 00 00 	lwz     r9,0(r7)
     b2c:	39 08 ff ff 	addi    r8,r8,-1
     b30:	7d 08 07 b4 	extsw   r8,r8
     b34:	79 2a 3e 24 	rldicr  r10,r9,7,56
     b38:	79 29 4d a4 	rldicr  r9,r9,9,54
     b3c:	7d 2a 48 50 	subf    r9,r10,r9
     b40:	7d 38 4a 14 	add     r9,r24,r9
     b44:	e9 49 00 78 	ld      r10,120(r9)
     b48:	39 4a 00 01 	addi    r10,r10,1
     b4c:	f9 49 00 78 	std     r10,120(r9)
     b50:	39 20 00 00 	li      r9,0
     b54:	4b ff fd cc 	b       920 <._Z7set_addl+0x270>
     b58:	60 00 00 00 	nop
     b5c:	60 42 00 00 	ori     r2,r2,0
     b60:	79 24 07 a1 	clrldi. r4,r9,62
     b64:	41 82 fd 70 	beq     8d4 <._Z7set_addl+0x224>
     b68:	e9 25 00 00 	ld      r9,0(r5)
     b6c:	39 29 00 08 	addi    r9,r9,8
     b70:	f9 25 00 00 	std     r9,0(r5)
     b74:	4b ff fd 60 	b       8d4 <._Z7set_addl+0x224>
     b78:	79 26 07 e1 	clrldi. r6,r9,63
     b7c:	41 82 00 34 	beq     bb0 <._Z7set_addl+0x500>
     b80:	81 27 00 00 	lwz     r9,0(r7)
     b84:	79 2a 3e 24 	rldicr  r10,r9,7,56
     b88:	79 29 4d a4 	rldicr  r9,r9,9,54
     b8c:	7d 2a 48 50 	subf    r9,r10,r9
     b90:	7d 38 4a 14 	add     r9,r24,r9
     b94:	e9 49 00 30 	ld      r10,48(r9)
     b98:	39 4a 00 01 	addi    r10,r10,1
     b9c:	f9 49 00 30 	std     r10,48(r9)
     ba0:	4b ff fc b8 	b       858 <._Z7set_addl+0x1a8>
     ba4:	60 00 00 00 	nop
     ba8:	60 00 00 00 	nop
     bac:	60 42 00 00 	ori     r2,r2,0
     bb0:	79 25 5f e3 	rldicl. r5,r9,43,63
     bb4:	40 82 03 84 	bne     f38 <._Z7set_addl+0x888>
     bb8:	81 27 00 00 	lwz     r9,0(r7)
     bbc:	79 2a 3e 24 	rldicr  r10,r9,7,56
     bc0:	79 29 4d a4 	rldicr  r9,r9,9,54
     bc4:	7d 2a 48 50 	subf    r9,r10,r9
     bc8:	7d 38 4a 14 	add     r9,r24,r9
     bcc:	e9 49 00 48 	ld      r10,72(r9)
     bd0:	39 4a 00 01 	addi    r10,r10,1
     bd4:	f9 49 00 48 	std     r10,72(r9)
     bd8:	4b ff fc 80 	b       858 <._Z7set_addl+0x1a8>
     bdc:	79 2a 5f e3 	rldicl. r10,r9,43,63
     be0:	40 82 03 94 	bne     f74 <._Z7set_addl+0x8c4>
     be4:	81 27 00 00 	lwz     r9,0(r7)
     be8:	39 08 ff ff 	addi    r8,r8,-1
     bec:	7d 08 07 b4 	extsw   r8,r8
     bf0:	79 2a 3e 24 	rldicr  r10,r9,7,56
     bf4:	79 29 4d a4 	rldicr  r9,r9,9,54
     bf8:	7d 2a 48 50 	subf    r9,r10,r9
     bfc:	7d 38 4a 14 	add     r9,r24,r9
     c00:	e9 49 00 90 	ld      r10,144(r9)
     c04:	39 4a 00 01 	addi    r10,r10,1
     c08:	f9 49 00 90 	std     r10,144(r9)
     c0c:	39 20 00 00 	li      r9,0
     c10:	4b ff fd 10 	b       920 <._Z7set_addl+0x270>

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_trans_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
     c14:	7d 42 22 a6 	mfspr   r10,130
     c18:	79 46 77 e1 	rldicl. r6,r10,14,63
     c1c:	41 82 02 c8 	beq     ee4 <._Z7set_addl+0x834>
     c20:	e9 49 00 20 	ld      r10,32(r9)
     c24:	39 4a 00 01 	addi    r10,r10,1
     c28:	f9 49 00 20 	std     r10,32(r9)
     c2c:	4b ff fc 2c 	b       858 <._Z7set_addl+0x1a8>
     c30:	39 1f 00 68 	addi    r8,r31,104
    res = priv_insert_htm(TM_ARG val);
    TM_END();
     c34:	38 c0 00 00 	li      r6,0
     c38:	60 00 00 00 	nop
     c3c:	60 42 00 00 	ori     r2,r2,0
     c40:	e9 28 00 09 	ldu     r9,8(r8)
     c44:	2f a9 00 00 	cmpdi   cr7,r9,0
     c48:	41 9e 00 28 	beq     cr7,c70 <._Z7set_addl+0x5c0>
     c4c:	78 c7 45 e4 	rldicr  r7,r6,8,55
     c50:	7d 5d 38 2a 	ldx     r10,r29,r7
     c54:	7f a9 50 00 	cmpd    cr7,r9,r10
     c58:	40 9e 00 18 	bne     cr7,c70 <._Z7set_addl+0x5c0>
     c5c:	60 42 00 00 	ori     r2,r2,0
     c60:	7d 3d 38 2a 	ldx     r9,r29,r7
     c64:	e9 48 00 00 	ld      r10,0(r8)
     c68:	7f a9 50 00 	cmpd    cr7,r9,r10
     c6c:	41 9e ff f4 	beq     cr7,c60 <._Z7set_addl+0x5b0>
     c70:	38 c6 00 01 	addi    r6,r6,1
     c74:	2f a6 00 50 	cmpdi   cr7,r6,80
     c78:	41 9e 00 0c 	beq     cr7,c84 <._Z7set_addl+0x5d4>
     c7c:	7f a5 30 00 	cmpd    cr7,r5,r6
     c80:	40 9e ff c0 	bne     cr7,c40 <._Z7set_addl+0x590>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_resume (void)
{
  __builtin_tresume ();
     c84:	7c 20 05 dd 	tresume.

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_end (void)
{
  if (__builtin_expect (__builtin_tend (0), 1))
     c88:	7c 00 05 5d 	tend.
     c8c:	7f 9c 6a 14 	add     r28,r28,r13

    return res;
}
     c90:	7f 63 db 78 	mr      r3,r27
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
    res = priv_insert_htm(TM_ARG val);
    TM_END();
     c94:	81 3c 00 00 	lwz     r9,0(r28)
     c98:	79 2a 3e 24 	rldicr  r10,r9,7,56
     c9c:	79 29 4d a4 	rldicr  r9,r9,9,54
     ca0:	7d 2a 48 50 	subf    r9,r10,r9
     ca4:	7f 18 4a 14 	add     r24,r24,r9
     ca8:	e9 38 00 08 	ld      r9,8(r24)
     cac:	39 29 00 01 	addi    r9,r9,1
     cb0:	f9 38 00 08 	std     r9,8(r24)

    return res;
}
     cb4:	38 3f 03 50 	addi    r1,r31,848
     cb8:	e8 01 00 10 	ld      r0,16(r1)
     cbc:	ea e1 ff b8 	ld      r23,-72(r1)
     cc0:	eb 01 ff c0 	ld      r24,-64(r1)
     cc4:	eb 21 ff c8 	ld      r25,-56(r1)
     cc8:	eb 41 ff d0 	ld      r26,-48(r1)
     ccc:	eb 61 ff d8 	ld      r27,-40(r1)
     cd0:	eb 81 ff e0 	ld      r28,-32(r1)
     cd4:	eb a1 ff e8 	ld      r29,-24(r1)
     cd8:	eb c1 ff f0 	ld      r30,-16(r1)
     cdc:	eb e1 ff f8 	ld      r31,-8(r1)
     ce0:	7c 08 03 a6 	mtlr    r0
     ce4:	4e 80 00 20 	blr
     ce8:	7d 42 22 a6 	mfspr   r10,130
long set_add(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     cec:	79 44 77 e1 	rldicl. r4,r10,14,63
     cf0:	41 82 02 10 	beq     f00 <._Z7set_addl+0x850>
     cf4:	e9 49 00 68 	ld      r10,104(r9)
     cf8:	39 4a 00 01 	addi    r10,r10,1
     cfc:	f9 49 00 68 	std     r10,104(r9)
     d00:	4b ff fe 10 	b       b10 <._Z7set_addl+0x460>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_suspend (void)
{
  __builtin_tsuspend ();
     d04:	7c 00 05 dd 	tsuspend.
    res = priv_insert_htm(TM_ARG val);
    TM_END();
     d08:	7d 3c 6a 14 	add     r9,r28,r13
     d0c:	3f a2 00 00 	addis   r29,r2,0
     d10:	eb bd 00 00 	ld      r29,0(r29)
     d14:	81 29 00 00 	lwz     r9,0(r9)
     d18:	79 29 45 e4 	rldicr  r9,r9,8,55
     d1c:	7d 5d 48 2a 	ldx     r10,r29,r9
     d20:	39 4a 00 04 	addi    r10,r10,4
     d24:	7d 5d 49 2a 	stdx    r10,r29,r9
     d28:	7c 00 04 ac 	sync

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_resume (void)
{
  __builtin_tresume ();
     d2c:	7c 20 05 dd 	tresume.
     d30:	3d 42 00 00 	addis   r10,r2,0
     d34:	e9 4a 00 00 	ld      r10,0(r10)
     d38:	39 00 00 00 	li      r8,0
     d3c:	39 20 00 00 	li      r9,0
     d40:	38 df 00 70 	addi    r6,r31,112
     d44:	38 e0 00 00 	li      r7,0
     d48:	e8 aa 00 00 	ld      r5,0(r10)
     d4c:	39 40 00 50 	li      r10,80
     d50:	7d 49 03 a6 	mtctr   r10
     d54:	2f a5 00 00 	cmpdi   cr7,r5,0
     d58:	40 9e 00 b0 	bne     cr7,e08 <._Z7set_addl+0x758>

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_end (void)
{
  if (__builtin_expect (__builtin_tend (0), 1))
     d5c:	7c 00 05 5d 	tend.
     d60:	7f 9c 6a 14 	add     r28,r28,r13
     d64:	81 3c 00 00 	lwz     r9,0(r28)
     d68:	79 29 45 e4 	rldicr  r9,r9,8,55
     d6c:	7d 5d 48 2a 	ldx     r10,r29,r9
     d70:	39 4a 00 01 	addi    r10,r10,1
     d74:	7d 5d 49 2a 	stdx    r10,r29,r9
     d78:	7c 00 04 ac 	sync
     d7c:	81 3c 00 00 	lwz     r9,0(r28)
     d80:	79 2a 3e 24 	rldicr  r10,r9,7,56
     d84:	79 29 4d a4 	rldicr  r9,r9,9,54
     d88:	7d 2a 48 50 	subf    r9,r10,r9
     d8c:	7f 18 4a 14 	add     r24,r24,r9
     d90:	e9 38 00 50 	ld      r9,80(r24)
     d94:	39 29 00 01 	addi    r9,r9,1
     d98:	f9 38 00 50 	std     r9,80(r24)

    return res;
}
     d9c:	38 3f 03 50 	addi    r1,r31,848
     da0:	7f 63 db 78 	mr      r3,r27
     da4:	e8 01 00 10 	ld      r0,16(r1)
     da8:	ea e1 ff b8 	ld      r23,-72(r1)
     dac:	eb 01 ff c0 	ld      r24,-64(r1)
     db0:	eb 21 ff c8 	ld      r25,-56(r1)
     db4:	eb 41 ff d0 	ld      r26,-48(r1)
     db8:	eb 61 ff d8 	ld      r27,-40(r1)
     dbc:	eb 81 ff e0 	ld      r28,-32(r1)
     dc0:	eb a1 ff e8 	ld      r29,-24(r1)
     dc4:	eb c1 ff f0 	ld      r30,-16(r1)
     dc8:	eb e1 ff f8 	ld      r31,-8(r1)
     dcc:	7c 08 03 a6 	mtlr    r0
     dd0:	4e 80 00 20 	blr
     dd4:	60 00 00 00 	nop
     dd8:	60 00 00 00 	nop
     ddc:	60 42 00 00 	ori     r2,r2,0
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
    res = priv_insert_htm(TM_ARG val);
    TM_END();
     de0:	e9 5f 02 f0 	ld      r10,752(r31)
     de4:	79 44 07 e1 	clrldi. r4,r10,63
     de8:	41 82 00 3c 	beq     e24 <._Z7set_addl+0x774>
     dec:	e9 5f 02 f0 	ld      r10,752(r31)
     df0:	7d 46 41 2a 	stdx    r10,r6,r8
     df4:	39 29 00 01 	addi    r9,r9,1
     df8:	7f a5 48 00 	cmpd    cr7,r5,r9
     dfc:	42 40 00 30 	bdz     e2c <._Z7set_addl+0x77c>
     e00:	39 08 00 08 	addi    r8,r8,8
     e04:	41 9e 00 28 	beq     cr7,e2c <._Z7set_addl+0x77c>
     e08:	79 2a 45 e4 	rldicr  r10,r9,8,55
     e0c:	7d 5d 50 2a 	ldx     r10,r29,r10
     e10:	f9 5f 02 f0 	std     r10,752(r31)
     e14:	60 42 00 00 	ori     r2,r2,0
     e18:	e9 5f 02 f0 	ld      r10,752(r31)
     e1c:	79 44 07 61 	clrldi. r4,r10,61
     e20:	41 82 ff c0 	beq     de0 <._Z7set_addl+0x730>
     e24:	7c e6 41 2a 	stdx    r7,r6,r8
     e28:	4b ff ff cc 	b       df4 <._Z7set_addl+0x744>
     e2c:	39 1f 00 68 	addi    r8,r31,104
     e30:	38 c0 00 00 	li      r6,0
     e34:	60 00 00 00 	nop
     e38:	60 00 00 00 	nop
     e3c:	60 42 00 00 	ori     r2,r2,0
     e40:	e9 28 00 09 	ldu     r9,8(r8)
     e44:	2f a9 00 00 	cmpdi   cr7,r9,0
     e48:	41 9e 00 28 	beq     cr7,e70 <._Z7set_addl+0x7c0>
     e4c:	78 c7 45 e4 	rldicr  r7,r6,8,55
     e50:	7d 5d 38 2a 	ldx     r10,r29,r7
     e54:	7f a9 50 00 	cmpd    cr7,r9,r10
     e58:	40 9e 00 18 	bne     cr7,e70 <._Z7set_addl+0x7c0>
     e5c:	60 42 00 00 	ori     r2,r2,0
     e60:	7d 3d 38 2a 	ldx     r9,r29,r7
     e64:	e9 48 00 00 	ld      r10,0(r8)
     e68:	7f a9 50 00 	cmpd    cr7,r9,r10
     e6c:	41 9e ff f4 	beq     cr7,e60 <._Z7set_addl+0x7b0>
     e70:	38 c6 00 01 	addi    r6,r6,1
     e74:	2f a6 00 50 	cmpdi   cr7,r6,80
     e78:	41 9e fe e4 	beq     cr7,d5c <._Z7set_addl+0x6ac>
     e7c:	7f a5 30 00 	cmpd    cr7,r5,r6
     e80:	40 9e ff c0 	bne     cr7,e40 <._Z7set_addl+0x790>
     e84:	7c 00 05 5d 	tend.
     e88:	7f 9c 6a 14 	add     r28,r28,r13
     e8c:	81 3c 00 00 	lwz     r9,0(r28)
     e90:	79 29 45 e4 	rldicr  r9,r9,8,55
     e94:	7d 5d 48 2a 	ldx     r10,r29,r9
     e98:	39 4a 00 01 	addi    r10,r10,1
     e9c:	7d 5d 49 2a 	stdx    r10,r29,r9
     ea0:	7c 00 04 ac 	sync
     ea4:	81 3c 00 00 	lwz     r9,0(r28)
     ea8:	79 2a 3e 24 	rldicr  r10,r9,7,56
     eac:	79 29 4d a4 	rldicr  r9,r9,9,54
     eb0:	7d 2a 48 50 	subf    r9,r10,r9
     eb4:	7f 18 4a 14 	add     r24,r24,r9
     eb8:	e9 38 00 50 	ld      r9,80(r24)
     ebc:	39 29 00 01 	addi    r9,r9,1
     ec0:	f9 38 00 50 	std     r9,80(r24)
     ec4:	4b ff fe d8 	b       d9c <._Z7set_addl+0x6ec>
long set_add(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(0);
     ec8:	39 4a 00 01 	addi    r10,r10,1
     ecc:	7d 5d 49 2a 	stdx    r10,r29,r9
     ed0:	4b ff fa 94 	b       964 <._Z7set_addl+0x2b4>
     ed4:	60 00 00 00 	nop
     ed8:	60 00 00 00 	nop
     edc:	60 42 00 00 	ori     r2,r2,0
     ee0:	4b ff fa 90 	b       970 <._Z7set_addl+0x2c0>

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_nontrans_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
     ee4:	7d 42 22 a6 	mfspr   r10,130
     ee8:	79 44 6f e1 	rldicl. r4,r10,13,63
     eec:	41 82 f9 6c 	beq     858 <._Z7set_addl+0x1a8>
     ef0:	e9 49 00 28 	ld      r10,40(r9)
     ef4:	39 4a 00 01 	addi    r10,r10,1
     ef8:	f9 49 00 28 	std     r10,40(r9)
     efc:	4b ff f9 5c 	b       858 <._Z7set_addl+0x1a8>
     f00:	7d 42 22 a6 	mfspr   r10,130
     f04:	79 45 6f e1 	rldicl. r5,r10,13,63
     f08:	41 82 fc 08 	beq     b10 <._Z7set_addl+0x460>
     f0c:	e9 49 00 70 	ld      r10,112(r9)
     f10:	39 4a 00 01 	addi    r10,r10,1
     f14:	f9 49 00 70 	std     r10,112(r9)
     f18:	4b ff fb f8 	b       b10 <._Z7set_addl+0x460>
     f1c:	7d 5d 48 2a 	ldx     r10,r29,r9
     f20:	79 47 07 e1 	clrldi. r7,r10,63
     f24:	41 82 fa 40 	beq     964 <._Z7set_addl+0x2b4>
     f28:	7d 5d 48 2a 	ldx     r10,r29,r9
     f2c:	39 4a 00 07 	addi    r10,r10,7
     f30:	7d 5d 49 2a 	stdx    r10,r29,r9
     f34:	4b ff fa 30 	b       964 <._Z7set_addl+0x2b4>
     f38:	79 26 47 e3 	rldicl. r6,r9,40,63
     f3c:	7d 5c 6a 14 	add     r10,r28,r13
     f40:	81 4a 00 00 	lwz     r10,0(r10)
     f44:	79 48 3e 24 	rldicr  r8,r10,7,56
     f48:	79 4a 4d a4 	rldicr  r10,r10,9,54
     f4c:	7d 48 50 50 	subf    r10,r8,r10
     f50:	7d 58 52 14 	add     r10,r24,r10
     f54:	e9 0a 00 38 	ld      r8,56(r10)
     f58:	39 08 00 01 	addi    r8,r8,1
     f5c:	f9 0a 00 38 	std     r8,56(r10)
     f60:	41 82 f9 08 	beq     868 <._Z7set_addl+0x1b8>
     f64:	e9 2a 00 40 	ld      r9,64(r10)
     f68:	39 29 00 01 	addi    r9,r9,1
     f6c:	f9 2a 00 40 	std     r9,64(r10)
     f70:	4b ff f8 f8 	b       868 <._Z7set_addl+0x1b8>
     f74:	79 24 47 e3 	rldicl. r4,r9,40,63
     f78:	7d 5c 6a 14 	add     r10,r28,r13
     f7c:	81 4a 00 00 	lwz     r10,0(r10)
     f80:	79 48 3e 24 	rldicr  r8,r10,7,56
     f84:	79 4a 4d a4 	rldicr  r10,r10,9,54
     f88:	7d 48 50 50 	subf    r10,r8,r10
     f8c:	7d 58 52 14 	add     r10,r24,r10
     f90:	e9 0a 00 88 	ld      r8,136(r10)
     f94:	39 08 00 01 	addi    r8,r8,1
     f98:	f9 0a 00 88 	std     r8,136(r10)
     f9c:	41 82 f9 94 	beq     930 <._Z7set_addl+0x280>
     fa0:	e9 2a 00 80 	ld      r9,128(r10)
     fa4:	39 29 00 01 	addi    r9,r9,1
     fa8:	f9 2a 00 80 	std     r9,128(r10)
     fac:	4b ff f9 84 	b       930 <._Z7set_addl+0x280>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_abort (void)
{
  __builtin_tabort (0);
     fb0:	39 20 00 00 	li      r9,0
     fb4:	7c 09 07 1d 	tabort. r9
     fb8:	4b ff f7 c8 	b       780 <._Z7set_addl+0xd0>
     fbc:	00 00 00 00 	.long 0x0
     fc0:	00 09 00 01 	.long 0x90001
     fc4:	80 09 00 00 	lwz     r0,0(r9)
     fc8:	60 00 00 00 	nop
     fcc:	60 42 00 00 	ori     r2,r2,0

0000000000000fd0 <._Z10set_removel>:

    return res;
}

int set_remove(TM_ARGDECL long val)
{
     fd0:	7c 08 02 a6 	mflr    r0
     fd4:	fb 21 ff c8 	std     r25,-56(r1)
     fd8:	fb 41 ff d0 	std     r26,-48(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
     fdc:	39 40 00 00 	li      r10,0

    return res;
}

int set_remove(TM_ARGDECL long val)
{
     fe0:	fb 61 ff d8 	std     r27,-40(r1)
     fe4:	fb 81 ff e0 	std     r28,-32(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
     fe8:	3f 62 00 00 	addis   r27,r2,0

    return res;
}

int set_remove(TM_ARGDECL long val)
{
     fec:	7c 79 1b 78 	mr      r25,r3
     ff0:	fb c1 ff f0 	std     r30,-16(r1)
     ff4:	fb e1 ff f8 	std     r31,-8(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
     ff8:	eb 7b 00 00 	ld      r27,0(r27)
     ffc:	3f 82 00 00 	addis   r28,r2,0

    return res;
}

int set_remove(TM_ARGDECL long val)
{
    1000:	fa e1 ff b8 	std     r23,-72(r1)
    1004:	fb 01 ff c0 	std     r24,-64(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    1008:	eb 9c 00 00 	ld      r28,0(r28)

    return res;
}

int set_remove(TM_ARGDECL long val)
{
    100c:	fb a1 ff e8 	std     r29,-24(r1)
    1010:	f8 01 00 10 	std     r0,16(r1)
    1014:	f8 21 fc b1 	stdu    r1,-848(r1)
    1018:	e9 21 00 00 	ld      r9,0(r1)
    101c:	7c 3f 0b 78 	mr      r31,r1
    1020:	f9 21 fe 81 	stdu    r9,-384(r1)
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    1024:	7d 3b 6a 14 	add     r9,r27,r13
    1028:	91 49 00 00 	stw     r10,0(r9)
    102c:	48 00 00 01 	bl      102c <._Z10set_removel+0x5c>
    1030:	60 00 00 00 	nop
    1034:	3f c2 00 00 	addis   r30,r2,0
    1038:	eb de 00 00 	ld      r30,0(r30)
    103c:	7d 5c 6a 14 	add     r10,r28,r13

    return res;
}

int set_remove(TM_ARGDECL long val)
{
    1040:	3b 41 00 ef 	addi    r26,r1,239
    1044:	7b 5a 06 24 	rldicr  r26,r26,0,56
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    1048:	90 6a 00 00 	stw     r3,0(r10)
    104c:	81 3e 00 00 	lwz     r9,0(r30)
    1050:	2f 89 00 00 	cmpwi   cr7,r9,0
    1054:	41 9e 00 18 	beq     cr7,106c <._Z10set_removel+0x9c>
    1058:	60 00 00 00 	nop
    105c:	60 42 00 00 	ori     r2,r2,0
    1060:	81 3e 00 00 	lwz     r9,0(r30)
    1064:	2f 89 00 00 	cmpwi   cr7,r9,0
    1068:	40 9e ff f8 	bne     cr7,1060 <._Z10set_removel+0x90>

    return res;
}

int set_remove(TM_ARGDECL long val)
{
    106c:	39 00 00 04 	li      r8,4
    1070:	3f 02 00 00 	addis   r24,r2,0
    1074:	eb 18 00 00 	ld      r24,0(r24)
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    1078:	7c fc 6a 14 	add     r7,r28,r13
    107c:	48 00 00 04 	b       1080 <._Z10set_removel+0xb0>
    1080:	81 3e 00 00 	lwz     r9,0(r30)
    1084:	2f 89 00 00 	cmpwi   cr7,r9,0
    1088:	40 9e ff f8 	bne     cr7,1080 <._Z10set_removel+0xb0>
extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_begin (void* const TM_buff)
{
  *_TEXASRL_PTR (TM_buff) = 0;
  if (__builtin_expect (__builtin_tbegin (0), 1))
    108c:	7c 00 05 1d 	tbegin.
    1090:	41 82 00 9c 	beq     112c <._Z10set_removel+0x15c>
    1094:	81 3e 00 00 	lwz     r9,0(r30)
    1098:	2f 89 00 00 	cmpwi   cr7,r9,0
    109c:	40 9e 08 34 	bne     cr7,18d0 <._Z10set_removel+0x900>
    res = priv_remove_item_htm(TM_ARG val);
    TM_END();
    10a0:	7f 7b 6a 14 	add     r27,r27,r13
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    res = priv_remove_item_htm(TM_ARG val);
    10a4:	7f 23 cb 78 	mr      r3,r25
    10a8:	48 00 00 01 	bl      10a8 <._Z10set_removel+0xd8>
    TM_END();
    10ac:	81 3b 00 00 	lwz     r9,0(r27)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    res = priv_remove_item_htm(TM_ARG val);
    10b0:	7c 7a 1b 78 	mr      r26,r3
    TM_END();
    10b4:	2f a9 00 00 	cmpdi   cr7,r9,0
    10b8:	41 9e 02 80 	beq     cr7,1338 <._Z10set_removel+0x368>
    10bc:	2b 89 00 01 	cmplwi  cr7,r9,1
    10c0:	41 9e 05 64 	beq     cr7,1624 <._Z10set_removel+0x654>
    10c4:	7f 9c 6a 14 	add     r28,r28,r13
    10c8:	7f c3 f3 78 	mr      r3,r30
    10cc:	81 3c 00 00 	lwz     r9,0(r28)
    10d0:	79 2a 3e 24 	rldicr  r10,r9,7,56
    10d4:	79 29 4d a4 	rldicr  r9,r9,9,54
    10d8:	7d 2a 48 50 	subf    r9,r10,r9
    10dc:	7d 38 4a 14 	add     r9,r24,r9
    10e0:	e9 49 00 98 	ld      r10,152(r9)
    10e4:	39 4a 00 01 	addi    r10,r10,1
    10e8:	f9 49 00 98 	std     r10,152(r9)
    10ec:	48 00 00 01 	bl      10ec <._Z10set_removel+0x11c>
    10f0:	60 00 00 00 	nop

    return res;
}
    10f4:	38 3f 03 50 	addi    r1,r31,848
    10f8:	7f 43 d3 78 	mr      r3,r26
    10fc:	e8 01 00 10 	ld      r0,16(r1)
    1100:	ea e1 ff b8 	ld      r23,-72(r1)
    1104:	eb 01 ff c0 	ld      r24,-64(r1)
    1108:	eb 21 ff c8 	ld      r25,-56(r1)
    110c:	eb 41 ff d0 	ld      r26,-48(r1)
    1110:	eb 61 ff d8 	ld      r27,-40(r1)
    1114:	eb 81 ff e0 	ld      r28,-32(r1)
    1118:	eb a1 ff e8 	ld      r29,-24(r1)
    111c:	eb c1 ff f0 	ld      r30,-16(r1)
    1120:	eb e1 ff f8 	ld      r31,-8(r1)
    1124:	7c 08 03 a6 	mtlr    r0
    1128:	4e 80 00 20 	blr
    return _HTM_TBEGIN_STARTED;
#ifdef __powerpc64__
  *_TEXASR_PTR (TM_buff) = __builtin_get_texasr ();
    112c:	7d 22 22 a6 	mfspr   r9,130
#else
  *_TEXASRU_PTR (TM_buff) = __builtin_get_texasru ();
  *_TEXASRL_PTR (TM_buff) = __builtin_get_texasr ();
#endif
  *_TFIAR_PTR (TM_buff) = __builtin_get_tfiar ();
    1130:	7d 41 22 a6 	mfspr   r10,129

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_conflict(void* const TM_buff)
{
  texasru_t texasru = *_TEXASRU_PTR (TM_buff);
    1134:	79 29 00 22 	rldicl  r9,r9,32,32
  /* Return TEXASR bits 11 (Self-Induced Conflict) through
     14 (Translation Invalidation Conflict).  */
  return (_TEXASRU_EXTRACT_BITS (texasru, 14, 4)) ? 1 : 0;
    1138:	79 25 7f 23 	rldicl. r5,r9,47,60
    113c:	41 82 03 5c 	beq     1498 <._Z10set_removel+0x4c8>
int set_remove(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    1140:	81 47 00 00 	lwz     r10,0(r7)
    1144:	79 49 3e 24 	rldicr  r9,r10,7,56
    1148:	79 4a 4d a4 	rldicr  r10,r10,9,54
    114c:	7d 29 50 50 	subf    r9,r9,r10
    1150:	7d 38 4a 14 	add     r9,r24,r9
    1154:	e9 49 00 10 	ld      r10,16(r9)
    1158:	39 4a 00 01 	addi    r10,r10,1
    115c:	f9 49 00 10 	std     r10,16(r9)

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_self_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
    1160:	7d 42 22 a6 	mfspr   r10,130
    1164:	79 45 67 e1 	rldicl. r5,r10,12,63
    1168:	41 82 03 cc 	beq     1534 <._Z10set_removel+0x564>
    116c:	e9 49 00 18 	ld      r10,24(r9)
    1170:	39 4a 00 01 	addi    r10,r10,1
    1174:	f9 49 00 18 	std     r10,24(r9)
    1178:	39 28 ff ff 	addi    r9,r8,-1
    117c:	2f 89 ff ff 	cmpwi   cr7,r9,-1
    1180:	79 28 00 20 	clrldi  r8,r9,32
    1184:	40 9e fe fc 	bne     cr7,1080 <._Z10set_removel+0xb0>
    1188:	81 3e 00 00 	lwz     r9,0(r30)
    118c:	7d 5b 6a 14 	add     r10,r27,r13
    1190:	39 00 00 01 	li      r8,1
    1194:	91 0a 00 00 	stw     r8,0(r10)
    1198:	2f 89 00 00 	cmpwi   cr7,r9,0
    119c:	41 9e 00 10 	beq     cr7,11ac <._Z10set_removel+0x1dc>
    11a0:	81 3e 00 00 	lwz     r9,0(r30)
    11a4:	2f 89 00 00 	cmpwi   cr7,r9,0
    11a8:	40 9e ff f8 	bne     cr7,11a0 <._Z10set_removel+0x1d0>
    11ac:	3c c2 00 00 	addis   r6,r2,0
    11b0:	3f a2 00 00 	addis   r29,r2,0
    11b4:	eb bd 00 00 	ld      r29,0(r29)

    return res;
}

int set_remove(TM_ARGDECL long val)
{
    11b8:	39 00 00 05 	li      r8,5
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    11bc:	e8 c6 00 00 	ld      r6,0(r6)
    11c0:	7c fc 6a 14 	add     r7,r28,r13
    11c4:	7c c6 6a 14 	add     r6,r6,r13
    11c8:	60 00 00 00 	nop
    11cc:	60 42 00 00 	ori     r2,r2,0
    11d0:	81 27 00 00 	lwz     r9,0(r7)
    11d4:	79 29 45 e4 	rldicr  r9,r9,8,55
    11d8:	7c bd 4a 14 	add     r5,r29,r9
    11dc:	7d 5d 48 2a 	ldx     r10,r29,r9
    11e0:	79 44 07 e1 	clrldi. r4,r10,63
    11e4:	40 82 01 cc 	bne     13b0 <._Z10set_removel+0x3e0>
    11e8:	7d 5d 48 2a 	ldx     r10,r29,r9
    11ec:	39 4a 00 03 	addi    r10,r10,3
    11f0:	7d 5d 49 2a 	stdx    r10,r29,r9
    11f4:	7c 00 04 ac 	sync
    11f8:	81 3e 00 00 	lwz     r9,0(r30)
    11fc:	7d 2a 07 b5 	extsw.  r10,r9
    1200:	41 82 01 d8 	beq     13d8 <._Z10set_removel+0x408>
    1204:	81 27 00 00 	lwz     r9,0(r7)
    1208:	79 29 45 e4 	rldicr  r9,r9,8,55
    120c:	7d 5d 48 2a 	ldx     r10,r29,r9
    1210:	39 4a 00 05 	addi    r10,r10,5
    1214:	7d 5d 49 2a 	stdx    r10,r29,r9
    1218:	7c 00 04 ac 	sync
    121c:	81 3e 00 00 	lwz     r9,0(r30)
    1220:	2f 89 00 00 	cmpwi   cr7,r9,0
    1224:	41 9e 00 18 	beq     cr7,123c <._Z10set_removel+0x26c>
    1228:	60 00 00 00 	nop
    122c:	60 42 00 00 	ori     r2,r2,0
    1230:	81 3e 00 00 	lwz     r9,0(r30)
    1234:	2f 89 00 00 	cmpwi   cr7,r9,0
    1238:	40 9e ff f8 	bne     cr7,1230 <._Z10set_removel+0x260>
    123c:	39 20 00 01 	li      r9,1
    1240:	2f 88 00 00 	cmpwi   cr7,r8,0
    1244:	41 9d ff 8c 	bgt     cr7,11d0 <._Z10set_removel+0x200>
    1248:	2f a9 00 00 	cmpdi   cr7,r9,0
    124c:	40 9e fe 54 	bne     cr7,10a0 <._Z10set_removel+0xd0>
    1250:	7d 3c 6a 14 	add     r9,r28,r13
    1254:	7d 5b 6a 14 	add     r10,r27,r13
    1258:	39 00 00 02 	li      r8,2
    125c:	81 29 00 00 	lwz     r9,0(r9)
    1260:	91 0a 00 00 	stw     r8,0(r10)
    1264:	79 29 45 e4 	rldicr  r9,r9,8,55
    1268:	7d 5d 48 2a 	ldx     r10,r29,r9
    126c:	79 45 07 61 	clrldi. r5,r10,61
    1270:	7d 5d 48 2a 	ldx     r10,r29,r9
    1274:	40 82 05 74 	bne     17e8 <._Z10set_removel+0x818>
    1278:	79 46 07 a1 	clrldi. r6,r10,62
    127c:	41 82 05 c0 	beq     183c <._Z10set_removel+0x86c>
    1280:	7d 3d 48 2a 	ldx     r9,r29,r9
    1284:	7c 00 04 ac 	sync
    1288:	7f d7 f3 78 	mr      r23,r30
    128c:	60 42 00 00 	ori     r2,r2,0
    1290:	81 3e 00 00 	lwz     r9,0(r30)
    1294:	7e e3 bb 78 	mr      r3,r23
    1298:	2f 89 00 00 	cmpwi   cr7,r9,0
    129c:	40 9e 05 64 	bne     cr7,1800 <._Z10set_removel+0x830>
    12a0:	48 00 00 01 	bl      12a0 <._Z10set_removel+0x2d0>
    12a4:	60 00 00 00 	nop
    12a8:	2f a3 00 00 	cmpdi   cr7,r3,0
    12ac:	40 9e ff e4 	bne     cr7,1290 <._Z10set_removel+0x2c0>
    12b0:	3d 22 00 00 	addis   r9,r2,0
    12b4:	e9 29 00 00 	ld      r9,0(r9)
    12b8:	39 00 00 01 	li      r8,1
    12bc:	e9 29 00 00 	ld      r9,0(r9)
    12c0:	f9 3a 00 00 	std     r9,0(r26)
    12c4:	60 42 00 00 	ori     r2,r2,0
    12c8:	e9 3a 00 00 	ld      r9,0(r26)
    12cc:	2f a9 00 00 	cmpdi   cr7,r9,0
    12d0:	40 9d fd d0 	ble     cr7,10a0 <._Z10set_removel+0xd0>
    12d4:	60 00 00 00 	nop
    12d8:	60 00 00 00 	nop
    12dc:	60 42 00 00 	ori     r2,r2,0
    12e0:	39 48 ff ff 	addi    r10,r8,-1
    12e4:	7d 4a 07 b4 	extsw   r10,r10
    12e8:	79 4a 45 e4 	rldicr  r10,r10,8,55
    12ec:	7d 3d 50 2a 	ldx     r9,r29,r10
    12f0:	79 25 07 e1 	clrldi. r5,r9,63
    12f4:	41 82 00 18 	beq     130c <._Z10set_removel+0x33c>
    12f8:	60 00 00 00 	nop
    12fc:	60 42 00 00 	ori     r2,r2,0
    1300:	7d 3d 50 2a 	ldx     r9,r29,r10
    1304:	79 24 07 e1 	clrldi. r4,r9,63
    1308:	40 82 ff f8 	bne     1300 <._Z10set_removel+0x330>
    130c:	e9 5a 00 00 	ld      r10,0(r26)
    1310:	7f a8 50 00 	cmpd    cr7,r8,r10
    1314:	39 08 00 01 	addi    r8,r8,1
    1318:	41 9c ff c8 	blt     cr7,12e0 <._Z10set_removel+0x310>
    res = priv_remove_item_htm(TM_ARG val);
    TM_END();
    131c:	7f 7b 6a 14 	add     r27,r27,r13
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    res = priv_remove_item_htm(TM_ARG val);
    1320:	7f 23 cb 78 	mr      r3,r25
    1324:	48 00 00 01 	bl      1324 <._Z10set_removel+0x354>
    TM_END();
    1328:	81 3b 00 00 	lwz     r9,0(r27)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    res = priv_remove_item_htm(TM_ARG val);
    132c:	7c 7a 1b 78 	mr      r26,r3
    TM_END();
    1330:	2f a9 00 00 	cmpdi   cr7,r9,0
    1334:	40 9e fd 88 	bne     cr7,10bc <._Z10set_removel+0xec>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_suspend (void)
{
  __builtin_tsuspend ();
    1338:	7c 00 05 dd 	tsuspend.
    133c:	3d 22 00 00 	addis   r9,r2,0
    1340:	e9 29 00 00 	ld      r9,0(r9)
    1344:	e8 a9 00 00 	ld      r5,0(r9)
    1348:	2f a5 00 00 	cmpdi   cr7,r5,0
    134c:	41 9e 02 58 	beq     cr7,15a4 <._Z10set_removel+0x5d4>
    1350:	39 20 00 50 	li      r9,80
    1354:	3f a2 00 00 	addis   r29,r2,0
    1358:	eb bd 00 00 	ld      r29,0(r29)
    135c:	38 e0 00 00 	li      r7,0
    1360:	7d 29 03 a6 	mtctr   r9
    1364:	39 00 00 00 	li      r8,0
    1368:	38 df 00 70 	addi    r6,r31,112
    136c:	48 00 00 20 	b       138c <._Z10set_removel+0x3bc>
    1370:	e9 3f 02 f8 	ld      r9,760(r31)
    1374:	7d 26 39 2a 	stdx    r9,r6,r7
    1378:	39 08 00 01 	addi    r8,r8,1
    137c:	7f a5 40 00 	cmpd    cr7,r5,r8
    1380:	42 40 01 d0 	bdz     1550 <._Z10set_removel+0x580>
    1384:	38 e7 00 08 	addi    r7,r7,8
    1388:	41 9e 01 c8 	beq     cr7,1550 <._Z10set_removel+0x580>
    138c:	79 09 45 e4 	rldicr  r9,r8,8,55
    1390:	7d 3d 48 2a 	ldx     r9,r29,r9
    1394:	f9 3f 02 f8 	std     r9,760(r31)
    1398:	60 42 00 00 	ori     r2,r2,0
    139c:	e9 3f 02 f8 	ld      r9,760(r31)
    13a0:	79 2a 07 e1 	clrldi. r10,r9,63
    13a4:	40 82 ff cc 	bne     1370 <._Z10set_removel+0x3a0>
    13a8:	7d 46 39 2a 	stdx    r10,r6,r7
    13ac:	4b ff ff cc 	b       1378 <._Z10set_removel+0x3a8>
int set_remove(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    13b0:	7d 3d 48 2a 	ldx     r9,r29,r9
    13b4:	79 2a 07 61 	clrldi. r10,r9,61
    13b8:	e9 25 00 00 	ld      r9,0(r5)
    13bc:	41 82 00 c4 	beq     1480 <._Z10set_removel+0x4b0>
    13c0:	39 29 00 04 	addi    r9,r9,4
    13c4:	f9 25 00 00 	std     r9,0(r5)
    13c8:	7c 00 04 ac 	sync
    13cc:	81 3e 00 00 	lwz     r9,0(r30)
    13d0:	7d 2a 07 b5 	extsw.  r10,r9
    13d4:	40 82 fe 30 	bne     1204 <._Z10set_removel+0x234>
    13d8:	f9 46 00 00 	std     r10,0(r6)
extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_begin_rot (void* const TM_buff)
{
  *_TEXASRL_PTR (TM_buff) = 0;
  if (__builtin_expect (__builtin_tbegin (1), 1)){
    13dc:	7c 20 05 1d 	tbegin. 1
    13e0:	40 82 fc c0 	bne     10a0 <._Z10set_removel+0xd0>
    return _HTM_TBEGIN_STARTED;
  }
#ifdef __powerpc64__
  *_TEXASR_PTR (TM_buff) = __builtin_get_texasr ();
    13e4:	7d 22 22 a6 	mfspr   r9,130
#else
  *_TEXASRU_PTR (TM_buff) = __builtin_get_texasru ();
  *_TEXASRL_PTR (TM_buff) = __builtin_get_texasr ();
#endif
  *_TFIAR_PTR (TM_buff) = __builtin_get_tfiar ();
    13e8:	7d 41 22 a6 	mfspr   r10,129

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_conflict(void* const TM_buff)
{
  texasru_t texasru = *_TEXASRU_PTR (TM_buff);
    13ec:	79 29 00 22 	rldicl  r9,r9,32,32
  /* Return TEXASR bits 11 (Self-Induced Conflict) through
     14 (Translation Invalidation Conflict).  */
  return (_TEXASRU_EXTRACT_BITS (texasru, 14, 4)) ? 1 : 0;
    13f0:	79 25 7f 23 	rldicl. r5,r9,47,60
    13f4:	41 82 00 4c 	beq     1440 <._Z10set_removel+0x470>
    13f8:	81 47 00 00 	lwz     r10,0(r7)
    13fc:	79 49 3e 24 	rldicr  r9,r10,7,56
    1400:	79 4a 4d a4 	rldicr  r10,r10,9,54
    1404:	7d 29 50 50 	subf    r9,r9,r10
    1408:	7d 38 4a 14 	add     r9,r24,r9
    140c:	e9 49 00 58 	ld      r10,88(r9)
    1410:	39 4a 00 01 	addi    r10,r10,1
    1414:	f9 49 00 58 	std     r10,88(r9)

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_self_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
    1418:	7d 42 22 a6 	mfspr   r10,130
    141c:	79 44 67 e1 	rldicl. r4,r10,12,63
    1420:	41 82 01 e8 	beq     1608 <._Z10set_removel+0x638>
    1424:	e9 49 00 60 	ld      r10,96(r9)
    1428:	39 4a 00 01 	addi    r10,r10,1
    142c:	f9 49 00 60 	std     r10,96(r9)
    1430:	39 08 ff ff 	addi    r8,r8,-1
    1434:	39 20 00 00 	li      r9,0
    1438:	7d 08 07 b4 	extsw   r8,r8
    143c:	4b ff fe 04 	b       1240 <._Z10set_removel+0x270>
    1440:	79 2a 07 e1 	clrldi. r10,r9,63
    1444:	41 82 00 b8 	beq     14fc <._Z10set_removel+0x52c>
    1448:	81 27 00 00 	lwz     r9,0(r7)
    144c:	39 08 ff ff 	addi    r8,r8,-1
    1450:	7d 08 07 b4 	extsw   r8,r8
    1454:	79 2a 3e 24 	rldicr  r10,r9,7,56
    1458:	79 29 4d a4 	rldicr  r9,r9,9,54
    145c:	7d 2a 48 50 	subf    r9,r10,r9
    1460:	7d 38 4a 14 	add     r9,r24,r9
    1464:	e9 49 00 78 	ld      r10,120(r9)
    1468:	39 4a 00 01 	addi    r10,r10,1
    146c:	f9 49 00 78 	std     r10,120(r9)
    1470:	39 20 00 00 	li      r9,0
    1474:	4b ff fd cc 	b       1240 <._Z10set_removel+0x270>
    1478:	60 00 00 00 	nop
    147c:	60 42 00 00 	ori     r2,r2,0
    1480:	79 24 07 a1 	clrldi. r4,r9,62
    1484:	41 82 fd 70 	beq     11f4 <._Z10set_removel+0x224>
    1488:	e9 25 00 00 	ld      r9,0(r5)
    148c:	39 29 00 08 	addi    r9,r9,8
    1490:	f9 25 00 00 	std     r9,0(r5)
    1494:	4b ff fd 60 	b       11f4 <._Z10set_removel+0x224>
    1498:	79 26 07 e1 	clrldi. r6,r9,63
    149c:	41 82 00 34 	beq     14d0 <._Z10set_removel+0x500>
    14a0:	81 27 00 00 	lwz     r9,0(r7)
    14a4:	79 2a 3e 24 	rldicr  r10,r9,7,56
    14a8:	79 29 4d a4 	rldicr  r9,r9,9,54
    14ac:	7d 2a 48 50 	subf    r9,r10,r9
    14b0:	7d 38 4a 14 	add     r9,r24,r9
    14b4:	e9 49 00 30 	ld      r10,48(r9)
    14b8:	39 4a 00 01 	addi    r10,r10,1
    14bc:	f9 49 00 30 	std     r10,48(r9)
    14c0:	4b ff fc b8 	b       1178 <._Z10set_removel+0x1a8>
    14c4:	60 00 00 00 	nop
    14c8:	60 00 00 00 	nop
    14cc:	60 42 00 00 	ori     r2,r2,0
    14d0:	79 25 5f e3 	rldicl. r5,r9,43,63
    14d4:	40 82 03 84 	bne     1858 <._Z10set_removel+0x888>
    14d8:	81 27 00 00 	lwz     r9,0(r7)
    14dc:	79 2a 3e 24 	rldicr  r10,r9,7,56
    14e0:	79 29 4d a4 	rldicr  r9,r9,9,54
    14e4:	7d 2a 48 50 	subf    r9,r10,r9
    14e8:	7d 38 4a 14 	add     r9,r24,r9
    14ec:	e9 49 00 48 	ld      r10,72(r9)
    14f0:	39 4a 00 01 	addi    r10,r10,1
    14f4:	f9 49 00 48 	std     r10,72(r9)
    14f8:	4b ff fc 80 	b       1178 <._Z10set_removel+0x1a8>
    14fc:	79 2a 5f e3 	rldicl. r10,r9,43,63
    1500:	40 82 03 94 	bne     1894 <._Z10set_removel+0x8c4>
    1504:	81 27 00 00 	lwz     r9,0(r7)
    1508:	39 08 ff ff 	addi    r8,r8,-1
    150c:	7d 08 07 b4 	extsw   r8,r8
    1510:	79 2a 3e 24 	rldicr  r10,r9,7,56
    1514:	79 29 4d a4 	rldicr  r9,r9,9,54
    1518:	7d 2a 48 50 	subf    r9,r10,r9
    151c:	7d 38 4a 14 	add     r9,r24,r9
    1520:	e9 49 00 90 	ld      r10,144(r9)
    1524:	39 4a 00 01 	addi    r10,r10,1
    1528:	f9 49 00 90 	std     r10,144(r9)
    152c:	39 20 00 00 	li      r9,0
    1530:	4b ff fd 10 	b       1240 <._Z10set_removel+0x270>

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_trans_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
    1534:	7d 42 22 a6 	mfspr   r10,130
    1538:	79 46 77 e1 	rldicl. r6,r10,14,63
    153c:	41 82 02 c8 	beq     1804 <._Z10set_removel+0x834>
    1540:	e9 49 00 20 	ld      r10,32(r9)
    1544:	39 4a 00 01 	addi    r10,r10,1
    1548:	f9 49 00 20 	std     r10,32(r9)
    154c:	4b ff fc 2c 	b       1178 <._Z10set_removel+0x1a8>
    1550:	39 1f 00 68 	addi    r8,r31,104
    res = priv_remove_item_htm(TM_ARG val);
    TM_END();
    1554:	38 c0 00 00 	li      r6,0
    1558:	60 00 00 00 	nop
    155c:	60 42 00 00 	ori     r2,r2,0
    1560:	e9 28 00 09 	ldu     r9,8(r8)
    1564:	2f a9 00 00 	cmpdi   cr7,r9,0
    1568:	41 9e 00 28 	beq     cr7,1590 <._Z10set_removel+0x5c0>
    156c:	78 c7 45 e4 	rldicr  r7,r6,8,55
    1570:	7d 5d 38 2a 	ldx     r10,r29,r7
    1574:	7f a9 50 00 	cmpd    cr7,r9,r10
    1578:	40 9e 00 18 	bne     cr7,1590 <._Z10set_removel+0x5c0>
    157c:	60 42 00 00 	ori     r2,r2,0
    1580:	7d 3d 38 2a 	ldx     r9,r29,r7
    1584:	e9 48 00 00 	ld      r10,0(r8)
    1588:	7f a9 50 00 	cmpd    cr7,r9,r10
    158c:	41 9e ff f4 	beq     cr7,1580 <._Z10set_removel+0x5b0>
    1590:	38 c6 00 01 	addi    r6,r6,1
    1594:	2f a6 00 50 	cmpdi   cr7,r6,80
    1598:	41 9e 00 0c 	beq     cr7,15a4 <._Z10set_removel+0x5d4>
    159c:	7f a5 30 00 	cmpd    cr7,r5,r6
    15a0:	40 9e ff c0 	bne     cr7,1560 <._Z10set_removel+0x590>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_resume (void)
{
  __builtin_tresume ();
    15a4:	7c 20 05 dd 	tresume.

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_end (void)
{
  if (__builtin_expect (__builtin_tend (0), 1))
    15a8:	7c 00 05 5d 	tend.
    15ac:	7f 9c 6a 14 	add     r28,r28,r13

    return res;
}
    15b0:	7f 43 d3 78 	mr      r3,r26
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    res = priv_remove_item_htm(TM_ARG val);
    TM_END();
    15b4:	81 3c 00 00 	lwz     r9,0(r28)
    15b8:	79 2a 3e 24 	rldicr  r10,r9,7,56
    15bc:	79 29 4d a4 	rldicr  r9,r9,9,54
    15c0:	7d 2a 48 50 	subf    r9,r10,r9
    15c4:	7f 18 4a 14 	add     r24,r24,r9
    15c8:	e9 38 00 08 	ld      r9,8(r24)
    15cc:	39 29 00 01 	addi    r9,r9,1
    15d0:	f9 38 00 08 	std     r9,8(r24)

    return res;
}
    15d4:	38 3f 03 50 	addi    r1,r31,848
    15d8:	e8 01 00 10 	ld      r0,16(r1)
    15dc:	ea e1 ff b8 	ld      r23,-72(r1)
    15e0:	eb 01 ff c0 	ld      r24,-64(r1)
    15e4:	eb 21 ff c8 	ld      r25,-56(r1)
    15e8:	eb 41 ff d0 	ld      r26,-48(r1)
    15ec:	eb 61 ff d8 	ld      r27,-40(r1)
    15f0:	eb 81 ff e0 	ld      r28,-32(r1)
    15f4:	eb a1 ff e8 	ld      r29,-24(r1)
    15f8:	eb c1 ff f0 	ld      r30,-16(r1)
    15fc:	eb e1 ff f8 	ld      r31,-8(r1)
    1600:	7c 08 03 a6 	mtlr    r0
    1604:	4e 80 00 20 	blr
    1608:	7d 42 22 a6 	mfspr   r10,130
int set_remove(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    160c:	79 44 77 e1 	rldicl. r4,r10,14,63
    1610:	41 82 02 10 	beq     1820 <._Z10set_removel+0x850>
    1614:	e9 49 00 68 	ld      r10,104(r9)
    1618:	39 4a 00 01 	addi    r10,r10,1
    161c:	f9 49 00 68 	std     r10,104(r9)
    1620:	4b ff fe 10 	b       1430 <._Z10set_removel+0x460>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_suspend (void)
{
  __builtin_tsuspend ();
    1624:	7c 00 05 dd 	tsuspend.
    res = priv_remove_item_htm(TM_ARG val);
    TM_END();
    1628:	7d 3c 6a 14 	add     r9,r28,r13
    162c:	3f a2 00 00 	addis   r29,r2,0
    1630:	eb bd 00 00 	ld      r29,0(r29)
    1634:	81 29 00 00 	lwz     r9,0(r9)
    1638:	79 29 45 e4 	rldicr  r9,r9,8,55
    163c:	7d 5d 48 2a 	ldx     r10,r29,r9
    1640:	39 4a 00 04 	addi    r10,r10,4
    1644:	7d 5d 49 2a 	stdx    r10,r29,r9
    1648:	7c 00 04 ac 	sync

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_resume (void)
{
  __builtin_tresume ();
    164c:	7c 20 05 dd 	tresume.
    1650:	3d 42 00 00 	addis   r10,r2,0
    1654:	e9 4a 00 00 	ld      r10,0(r10)
    1658:	39 00 00 00 	li      r8,0
    165c:	39 20 00 00 	li      r9,0
    1660:	38 df 00 70 	addi    r6,r31,112
    1664:	38 e0 00 00 	li      r7,0
    1668:	e8 aa 00 00 	ld      r5,0(r10)
    166c:	39 40 00 50 	li      r10,80
    1670:	7d 49 03 a6 	mtctr   r10
    1674:	2f a5 00 00 	cmpdi   cr7,r5,0
    1678:	40 9e 00 b0 	bne     cr7,1728 <._Z10set_removel+0x758>

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_end (void)
{
  if (__builtin_expect (__builtin_tend (0), 1))
    167c:	7c 00 05 5d 	tend.
    1680:	7f 9c 6a 14 	add     r28,r28,r13
    1684:	81 3c 00 00 	lwz     r9,0(r28)
    1688:	79 29 45 e4 	rldicr  r9,r9,8,55
    168c:	7d 5d 48 2a 	ldx     r10,r29,r9
    1690:	39 4a 00 01 	addi    r10,r10,1
    1694:	7d 5d 49 2a 	stdx    r10,r29,r9
    1698:	7c 00 04 ac 	sync
    169c:	81 3c 00 00 	lwz     r9,0(r28)
    16a0:	79 2a 3e 24 	rldicr  r10,r9,7,56
    16a4:	79 29 4d a4 	rldicr  r9,r9,9,54
    16a8:	7d 2a 48 50 	subf    r9,r10,r9
    16ac:	7f 18 4a 14 	add     r24,r24,r9
    16b0:	e9 38 00 50 	ld      r9,80(r24)
    16b4:	39 29 00 01 	addi    r9,r9,1
    16b8:	f9 38 00 50 	std     r9,80(r24)

    return res;
}
    16bc:	38 3f 03 50 	addi    r1,r31,848
    16c0:	7f 43 d3 78 	mr      r3,r26
    16c4:	e8 01 00 10 	ld      r0,16(r1)
    16c8:	ea e1 ff b8 	ld      r23,-72(r1)
    16cc:	eb 01 ff c0 	ld      r24,-64(r1)
    16d0:	eb 21 ff c8 	ld      r25,-56(r1)
    16d4:	eb 41 ff d0 	ld      r26,-48(r1)
    16d8:	eb 61 ff d8 	ld      r27,-40(r1)
    16dc:	eb 81 ff e0 	ld      r28,-32(r1)
    16e0:	eb a1 ff e8 	ld      r29,-24(r1)
    16e4:	eb c1 ff f0 	ld      r30,-16(r1)
    16e8:	eb e1 ff f8 	ld      r31,-8(r1)
    16ec:	7c 08 03 a6 	mtlr    r0
    16f0:	4e 80 00 20 	blr
    16f4:	60 00 00 00 	nop
    16f8:	60 00 00 00 	nop
    16fc:	60 42 00 00 	ori     r2,r2,0
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    res = priv_remove_item_htm(TM_ARG val);
    TM_END();
    1700:	e9 5f 02 f0 	ld      r10,752(r31)
    1704:	79 44 07 e1 	clrldi. r4,r10,63
    1708:	41 82 00 3c 	beq     1744 <._Z10set_removel+0x774>
    170c:	e9 5f 02 f0 	ld      r10,752(r31)
    1710:	7d 46 41 2a 	stdx    r10,r6,r8
    1714:	39 29 00 01 	addi    r9,r9,1
    1718:	7f a5 48 00 	cmpd    cr7,r5,r9
    171c:	42 40 00 30 	bdz     174c <._Z10set_removel+0x77c>
    1720:	39 08 00 08 	addi    r8,r8,8
    1724:	41 9e 00 28 	beq     cr7,174c <._Z10set_removel+0x77c>
    1728:	79 2a 45 e4 	rldicr  r10,r9,8,55
    172c:	7d 5d 50 2a 	ldx     r10,r29,r10
    1730:	f9 5f 02 f0 	std     r10,752(r31)
    1734:	60 42 00 00 	ori     r2,r2,0
    1738:	e9 5f 02 f0 	ld      r10,752(r31)
    173c:	79 44 07 61 	clrldi. r4,r10,61
    1740:	41 82 ff c0 	beq     1700 <._Z10set_removel+0x730>
    1744:	7c e6 41 2a 	stdx    r7,r6,r8
    1748:	4b ff ff cc 	b       1714 <._Z10set_removel+0x744>
    174c:	39 1f 00 68 	addi    r8,r31,104
    1750:	38 c0 00 00 	li      r6,0
    1754:	60 00 00 00 	nop
    1758:	60 00 00 00 	nop
    175c:	60 42 00 00 	ori     r2,r2,0
    1760:	e9 28 00 09 	ldu     r9,8(r8)
    1764:	2f a9 00 00 	cmpdi   cr7,r9,0
    1768:	41 9e 00 28 	beq     cr7,1790 <._Z10set_removel+0x7c0>
    176c:	78 c7 45 e4 	rldicr  r7,r6,8,55
    1770:	7d 5d 38 2a 	ldx     r10,r29,r7
    1774:	7f a9 50 00 	cmpd    cr7,r9,r10
    1778:	40 9e 00 18 	bne     cr7,1790 <._Z10set_removel+0x7c0>
    177c:	60 42 00 00 	ori     r2,r2,0
    1780:	7d 3d 38 2a 	ldx     r9,r29,r7
    1784:	e9 48 00 00 	ld      r10,0(r8)
    1788:	7f a9 50 00 	cmpd    cr7,r9,r10
    178c:	41 9e ff f4 	beq     cr7,1780 <._Z10set_removel+0x7b0>
    1790:	38 c6 00 01 	addi    r6,r6,1
    1794:	2f a6 00 50 	cmpdi   cr7,r6,80
    1798:	41 9e fe e4 	beq     cr7,167c <._Z10set_removel+0x6ac>
    179c:	7f a5 30 00 	cmpd    cr7,r5,r6
    17a0:	40 9e ff c0 	bne     cr7,1760 <._Z10set_removel+0x790>
    17a4:	7c 00 05 5d 	tend.
    17a8:	7f 9c 6a 14 	add     r28,r28,r13
    17ac:	81 3c 00 00 	lwz     r9,0(r28)
    17b0:	79 29 45 e4 	rldicr  r9,r9,8,55
    17b4:	7d 5d 48 2a 	ldx     r10,r29,r9
    17b8:	39 4a 00 01 	addi    r10,r10,1
    17bc:	7d 5d 49 2a 	stdx    r10,r29,r9
    17c0:	7c 00 04 ac 	sync
    17c4:	81 3c 00 00 	lwz     r9,0(r28)
    17c8:	79 2a 3e 24 	rldicr  r10,r9,7,56
    17cc:	79 29 4d a4 	rldicr  r9,r9,9,54
    17d0:	7d 2a 48 50 	subf    r9,r10,r9
    17d4:	7f 18 4a 14 	add     r24,r24,r9
    17d8:	e9 38 00 50 	ld      r9,80(r24)
    17dc:	39 29 00 01 	addi    r9,r9,1
    17e0:	f9 38 00 50 	std     r9,80(r24)
    17e4:	4b ff fe d8 	b       16bc <._Z10set_removel+0x6ec>
int set_remove(TM_ARGDECL long val)
{
    int res = 0;

    int ro = 0;
    TM_BEGIN(1);
    17e8:	39 4a 00 01 	addi    r10,r10,1
    17ec:	7d 5d 49 2a 	stdx    r10,r29,r9
    17f0:	4b ff fa 94 	b       1284 <._Z10set_removel+0x2b4>
    17f4:	60 00 00 00 	nop
    17f8:	60 00 00 00 	nop
    17fc:	60 42 00 00 	ori     r2,r2,0
    1800:	4b ff fa 90 	b       1290 <._Z10set_removel+0x2c0>

extern __inline long
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_nontrans_conflict(void* const TM_buff)
{
  texasr_t texasr = __builtin_get_texasr ();
    1804:	7d 42 22 a6 	mfspr   r10,130
    1808:	79 44 6f e1 	rldicl. r4,r10,13,63
    180c:	41 82 f9 6c 	beq     1178 <._Z10set_removel+0x1a8>
    1810:	e9 49 00 28 	ld      r10,40(r9)
    1814:	39 4a 00 01 	addi    r10,r10,1
    1818:	f9 49 00 28 	std     r10,40(r9)
    181c:	4b ff f9 5c 	b       1178 <._Z10set_removel+0x1a8>
    1820:	7d 42 22 a6 	mfspr   r10,130
    1824:	79 45 6f e1 	rldicl. r5,r10,13,63
    1828:	41 82 fc 08 	beq     1430 <._Z10set_removel+0x460>
    182c:	e9 49 00 70 	ld      r10,112(r9)
    1830:	39 4a 00 01 	addi    r10,r10,1
    1834:	f9 49 00 70 	std     r10,112(r9)
    1838:	4b ff fb f8 	b       1430 <._Z10set_removel+0x460>
    183c:	7d 5d 48 2a 	ldx     r10,r29,r9
    1840:	79 47 07 e1 	clrldi. r7,r10,63
    1844:	41 82 fa 40 	beq     1284 <._Z10set_removel+0x2b4>
    1848:	7d 5d 48 2a 	ldx     r10,r29,r9
    184c:	39 4a 00 07 	addi    r10,r10,7
    1850:	7d 5d 49 2a 	stdx    r10,r29,r9
    1854:	4b ff fa 30 	b       1284 <._Z10set_removel+0x2b4>
    1858:	79 26 47 e3 	rldicl. r6,r9,40,63
    185c:	7d 5c 6a 14 	add     r10,r28,r13
    1860:	81 4a 00 00 	lwz     r10,0(r10)
    1864:	79 48 3e 24 	rldicr  r8,r10,7,56
    1868:	79 4a 4d a4 	rldicr  r10,r10,9,54
    186c:	7d 48 50 50 	subf    r10,r8,r10
    1870:	7d 58 52 14 	add     r10,r24,r10
    1874:	e9 0a 00 38 	ld      r8,56(r10)
    1878:	39 08 00 01 	addi    r8,r8,1
    187c:	f9 0a 00 38 	std     r8,56(r10)
    1880:	41 82 f9 08 	beq     1188 <._Z10set_removel+0x1b8>
    1884:	e9 2a 00 40 	ld      r9,64(r10)
    1888:	39 29 00 01 	addi    r9,r9,1
    188c:	f9 2a 00 40 	std     r9,64(r10)
    1890:	4b ff f8 f8 	b       1188 <._Z10set_removel+0x1b8>
    1894:	79 24 47 e3 	rldicl. r4,r9,40,63
    1898:	7d 5c 6a 14 	add     r10,r28,r13
    189c:	81 4a 00 00 	lwz     r10,0(r10)
    18a0:	79 48 3e 24 	rldicr  r8,r10,7,56
    18a4:	79 4a 4d a4 	rldicr  r10,r10,9,54
    18a8:	7d 48 50 50 	subf    r10,r8,r10
    18ac:	7d 58 52 14 	add     r10,r24,r10
    18b0:	e9 0a 00 88 	ld      r8,136(r10)
    18b4:	39 08 00 01 	addi    r8,r8,1
    18b8:	f9 0a 00 88 	std     r8,136(r10)
    18bc:	41 82 f9 94 	beq     1250 <._Z10set_removel+0x280>
    18c0:	e9 2a 00 80 	ld      r9,128(r10)
    18c4:	39 29 00 01 	addi    r9,r9,1
    18c8:	f9 2a 00 80 	std     r9,128(r10)
    18cc:	4b ff f9 84 	b       1250 <._Z10set_removel+0x280>

extern __inline void
__attribute__ ((__gnu_inline__, __always_inline__, __artificial__))
__TM_abort (void)
{
  __builtin_tabort (0);
    18d0:	39 20 00 00 	li      r9,0
    18d4:	7c 09 07 1d 	tabort. r9
    18d8:	4b ff f7 c8 	b       10a0 <._Z10set_removel+0xd0>
    18dc:	00 00 00 00 	.long 0x0
    18e0:	00 09 00 01 	.long 0x90001
    18e4:	80 09 00 00 	lwz     r0,0(r9)
    18e8:	60 00 00 00 	nop
    18ec:	60 42 00 00 	ori     r2,r2,0

00000000000018f0 <._Z12set_containsl>:

    return res;
}

long set_contains(TM_ARGDECL long  val)
{
    18f0:	7c 08 02 a6 	mflr    r0
    18f4:	fb a1 ff e8 	std     r29,-24(r1)
    18f8:	fb c1 ff f0 	std     r30,-16(r1)
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    18fc:	3d 22 00 00 	addis   r9,r2,0

    return res;
}

long set_contains(TM_ARGDECL long  val)
{
    1900:	fb e1 ff f8 	std     r31,-8(r1)
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    1904:	e9 29 00 00 	ld      r9,0(r9)
    1908:	39 40 00 00 	li      r10,0
    190c:	7d 29 6a 14 	add     r9,r9,r13

    return res;
}

long set_contains(TM_ARGDECL long  val)
{
    1910:	7c 7d 1b 78 	mr      r29,r3
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    1914:	3f c2 00 00 	addis   r30,r2,0
    1918:	eb de 00 00 	ld      r30,0(r30)

    return res;
}

long set_contains(TM_ARGDECL long  val)
{
    191c:	f8 01 00 10 	std     r0,16(r1)
    1920:	f8 21 ff 71 	stdu    r1,-144(r1)
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    1924:	91 49 00 00 	stw     r10,0(r9)
    1928:	48 00 00 01 	bl      1928 <._Z12set_containsl+0x38>
    192c:	60 00 00 00 	nop
    1930:	3c c2 00 00 	addis   r6,r2,0
    1934:	e8 c6 00 00 	ld      r6,0(r6)
    1938:	7c fe 6a 14 	add     r7,r30,r13
    193c:	3d 42 00 00 	addis   r10,r2,0
    1940:	e9 4a 00 00 	ld      r10,0(r10)
    1944:	78 63 00 20 	clrldi  r3,r3,32
    1948:	78 69 45 e4 	rldicr  r9,r3,8,55
    194c:	7c df 33 78 	mr      r31,r6
    1950:	90 67 00 00 	stw     r3,0(r7)
    1954:	7d 06 48 2a 	ldx     r8,r6,r9
    1958:	39 08 00 01 	addi    r8,r8,1
    195c:	7d 06 49 2a 	stdx    r8,r6,r9
    1960:	7c 00 04 ac 	sync
    1964:	81 2a 00 00 	lwz     r9,0(r10)
    1968:	2f 89 00 00 	cmpwi   cr7,r9,0
    196c:	41 9e 00 64 	beq     cr7,19d0 <._Z12set_containsl+0xe0>
    1970:	81 27 00 00 	lwz     r9,0(r7)
    1974:	79 29 45 e4 	rldicr  r9,r9,8,55
    1978:	7d 1f 48 2a 	ldx     r8,r31,r9
    197c:	39 08 00 07 	addi    r8,r8,7
    1980:	7d 1f 49 2a 	stdx    r8,r31,r9
    1984:	7c 00 04 ac 	sync
    1988:	81 2a 00 00 	lwz     r9,0(r10)
    198c:	2f 89 00 00 	cmpwi   cr7,r9,0
    1990:	41 9e 00 1c 	beq     cr7,19ac <._Z12set_containsl+0xbc>
    1994:	60 00 00 00 	nop
    1998:	60 00 00 00 	nop
    199c:	60 42 00 00 	ori     r2,r2,0
    19a0:	81 2a 00 00 	lwz     r9,0(r10)
    19a4:	2f 89 00 00 	cmpwi   cr7,r9,0
    19a8:	40 9e ff f8 	bne     cr7,19a0 <._Z12set_containsl+0xb0>
    19ac:	80 67 00 00 	lwz     r3,0(r7)
    19b0:	78 69 45 e4 	rldicr  r9,r3,8,55
    19b4:	7d 06 48 2a 	ldx     r8,r6,r9
    19b8:	39 08 00 01 	addi    r8,r8,1
    19bc:	7d 06 49 2a 	stdx    r8,r6,r9
    19c0:	7c 00 04 ac 	sync
    19c4:	81 2a 00 00 	lwz     r9,0(r10)
    19c8:	2f 89 00 00 	cmpwi   cr7,r9,0
    19cc:	40 9e ff a4 	bne     cr7,1970 <._Z12set_containsl+0x80>
    res = priv_lookup_htm(TM_ARG val);
    TM_END();
    19d0:	7f de 6a 14 	add     r30,r30,r13
{
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    res = priv_lookup_htm(TM_ARG val);
    19d4:	7f a3 eb 78 	mr      r3,r29
    19d8:	48 00 00 01 	bl      19d8 <._Z12set_containsl+0xe8>
    TM_END();
    19dc:	81 3e 00 00 	lwz     r9,0(r30)

    return res;
}
    19e0:	38 21 00 90 	addi    r1,r1,144
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    res = priv_lookup_htm(TM_ARG val);
    TM_END();
    19e4:	3c c2 00 00 	addis   r6,r2,0
    19e8:	e8 c6 00 00 	ld      r6,0(r6)

    return res;
}
    19ec:	e8 01 00 10 	ld      r0,16(r1)
    19f0:	eb a1 ff e8 	ld      r29,-24(r1)
    19f4:	eb c1 ff f0 	ld      r30,-16(r1)
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    res = priv_lookup_htm(TM_ARG val);
    TM_END();
    19f8:	79 28 3e 24 	rldicr  r8,r9,7,56
    19fc:	79 2a 4d a4 	rldicr  r10,r9,9,54
    1a00:	79 29 45 e4 	rldicr  r9,r9,8,55
    1a04:	7d 48 50 50 	subf    r10,r8,r10

    return res;
}
    1a08:	7c 08 03 a6 	mtlr    r0
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    res = priv_lookup_htm(TM_ARG val);
    TM_END();
    1a0c:	7c ff 48 2a 	ldx     r7,r31,r9
    1a10:	7d 06 50 2a 	ldx     r8,r6,r10
    1a14:	38 e7 00 07 	addi    r7,r7,7
    1a18:	39 08 00 01 	addi    r8,r8,1
    1a1c:	7c ff 49 2a 	stdx    r7,r31,r9

    return res;
}
    1a20:	eb e1 ff f8 	ld      r31,-8(r1)
    long res = 0;

    int ro = 1;
    TM_BEGIN_EXT(2,1);
    res = priv_lookup_htm(TM_ARG val);
    TM_END();
    1a24:	7d 06 51 2a 	stdx    r8,r6,r10

    return res;
}
    1a28:	4e 80 00 20 	blr
    1a2c:	00 00 00 00 	.long 0x0
    1a30:	00 09 00 01 	.long 0x90001
    1a34:	80 03 00 00 	lwz     r0,0(r3)
    1a38:	60 00 00 00 	nop
    1a3c:	60 42 00 00 	ori     r2,r2,0

0000000000001a40 <._Z4testPv>:
  unsigned int nb_threads;
  unsigned int seed;
  long operations;

void *test(void *data)
{
    1a40:	7c 08 02 a6 	mflr    r0
    1a44:	fb e1 ff f8 	std     r31,-8(r1)
    1a48:	fb 61 ff d8 	std     r27,-40(r1)
    1a4c:	fb 81 ff e0 	std     r28,-32(r1)
    1a50:	fb a1 ff e8 	std     r29,-24(r1)
    1a54:	fb c1 ff f0 	std     r30,-16(r1)
    1a58:	f8 01 00 10 	std     r0,16(r1)
    1a5c:	f8 21 ff 51 	stdu    r1,-176(r1)

  TM_THREAD_ENTER();

  unsigned int mySeed = seed + sched_getcpu();
    1a60:	48 00 00 01 	bl      1a60 <._Z4testPv+0x20>
    1a64:	60 00 00 00 	nop

  long myOps = operations / nb_threads;
    1a68:	3d 42 00 00 	addis   r10,r2,0
    1a6c:	81 4a 00 00 	lwz     r10,0(r10)
    1a70:	3f e2 00 00 	addis   r31,r2,0
    1a74:	eb ff 00 00 	ld      r31,0(r31)
void *test(void *data)
{

  TM_THREAD_ENTER();

  unsigned int mySeed = seed + sched_getcpu();
    1a78:	3d 22 00 00 	addis   r9,r2,0
    1a7c:	81 29 00 00 	lwz     r9,0(r9)

  long myOps = operations / nb_threads;
    1a80:	7f ff 53 d2 	divd    r31,r31,r10
void *test(void *data)
{

  TM_THREAD_ENTER();

  unsigned int mySeed = seed + sched_getcpu();
    1a84:	7c 69 1a 14 	add     r3,r9,r3
    1a88:	90 61 00 70 	stw     r3,112(r1)
  long myOps = operations / nb_threads;
  long val = -1;
  int op;


  while (myOps > 0) {
    1a8c:	2f bf 00 00 	cmpdi   cr7,r31,0
    1a90:	40 9d 00 9c 	ble     cr7,1b2c <._Z4testPv+0xec>
    1a94:	3f a2 00 00 	addis   r29,r2,0
    op = rand_r(&mySeed) % 100;
    1a98:	3f 60 51 eb 	lis     r27,20971
  long myOps = operations / nb_threads;
  long val = -1;
  int op;


  while (myOps > 0) {
    1a9c:	3b c0 ff ff 	li      r30,-1
    1aa0:	3b bd 00 00 	addi    r29,r29,0
    op = rand_r(&mySeed) % 100;
    1aa4:	63 7b 85 1f 	ori     r27,r27,34079
    1aa8:	48 00 00 1c 	b       1ac4 <._Z4testPv+0x84>
    1aac:	60 42 00 00 	ori     r2,r2,0
    if (op < update) {
      if (val == -1) {
    1ab0:	41 9a 00 b0 	beq     cr6,1b60 <._Z4testPv+0x120>
        if(set_add(TM_ARG val) == 0) {
          val = -1;
        }
      } else {
        /* Remove random value */
        set_remove(TM_ARG  val);
    1ab4:	48 00 00 01 	bl      1ab4 <._Z4testPv+0x74>
        val = -1;
    1ab8:	3b c0 ff ff 	li      r30,-1
  long myOps = operations / nb_threads;
  long val = -1;
  int op;


  while (myOps > 0) {
    1abc:	37 ff ff ff 	addic.  r31,r31,-1
    1ac0:	41 82 00 6c 	beq     1b2c <._Z4testPv+0xec>
    op = rand_r(&mySeed) % 100;
    1ac4:	38 61 00 70 	addi    r3,r1,112
    1ac8:	48 00 00 01 	bl      1ac8 <._Z4testPv+0x88>
    1acc:	60 00 00 00 	nop
    if (op < update) {
    1ad0:	80 fd 00 1c 	lwz     r7,28(r29)
      if (val == -1) {
    1ad4:	2f 3e ff ff 	cmpdi   cr6,r30,-1
  long val = -1;
  int op;


  while (myOps > 0) {
    op = rand_r(&mySeed) % 100;
    1ad8:	7d 43 d8 96 	mulhw   r10,r3,r27
    1adc:	7c 69 fe 70 	srawi   r9,r3,31
    1ae0:	7c 68 1b 78 	mr      r8,r3
        if(set_add(TM_ARG val) == 0) {
          val = -1;
        }
      } else {
        /* Remove random value */
        set_remove(TM_ARG  val);
    1ae4:	7f c3 f3 78 	mr      r3,r30
  long val = -1;
  int op;


  while (myOps > 0) {
    op = rand_r(&mySeed) % 100;
    1ae8:	7d 4a 2e 70 	srawi   r10,r10,5
    1aec:	7d 29 50 50 	subf    r9,r9,r10
    1af0:	1d 29 00 64 	mulli   r9,r9,100
    1af4:	7d 29 40 50 	subf    r9,r9,r8
    if (op < update) {
    1af8:	7f 89 38 00 	cmpw    cr7,r9,r7
    1afc:	41 9c ff b4 	blt     cr7,1ab0 <._Z4testPv+0x70>
        set_remove(TM_ARG  val);
        val = -1;
      }
    } else {
      /* Look for random value */
      long tmp = (rand_r(&mySeed) % range) + 1;
    1b00:	38 61 00 70 	addi    r3,r1,112
    1b04:	48 00 00 01 	bl      1b04 <._Z4testPv+0xc4>
    1b08:	60 00 00 00 	nop
    1b0c:	e9 3d 00 20 	ld      r9,32(r29)
    1b10:	7d 43 4b 92 	divdu   r10,r3,r9
    1b14:	7d 2a 49 d2 	mulld   r9,r10,r9
    1b18:	7c 69 18 50 	subf    r3,r9,r3
      set_contains(TM_ARG tmp);
    1b1c:	38 63 00 01 	addi    r3,r3,1
    1b20:	48 00 00 01 	bl      1b20 <._Z4testPv+0xe0>
  long myOps = operations / nb_threads;
  long val = -1;
  int op;


  while (myOps > 0) {
    1b24:	37 ff ff ff 	addic.  r31,r31,-1
    1b28:	40 82 ff 9c 	bne     1ac4 <._Z4testPv+0x84>
    myOps--;
  }

  TM_THREAD_EXIT();
  return NULL;
}
    1b2c:	38 21 00 b0 	addi    r1,r1,176
    1b30:	38 60 00 00 	li      r3,0
    1b34:	e8 01 00 10 	ld      r0,16(r1)
    1b38:	eb 61 ff d8 	ld      r27,-40(r1)
    1b3c:	eb 81 ff e0 	ld      r28,-32(r1)
    1b40:	eb a1 ff e8 	ld      r29,-24(r1)
    1b44:	eb c1 ff f0 	ld      r30,-16(r1)
    1b48:	eb e1 ff f8 	ld      r31,-8(r1)
    1b4c:	7c 08 03 a6 	mtlr    r0
    1b50:	4e 80 00 20 	blr
    1b54:	60 00 00 00 	nop
    1b58:	60 00 00 00 	nop
    1b5c:	60 42 00 00 	ori     r2,r2,0
  while (myOps > 0) {
    op = rand_r(&mySeed) % 100;
    if (op < update) {
      if (val == -1) {
        /* Add random value */
        val = (rand_r(&mySeed) % range) + 1;
    1b60:	38 61 00 70 	addi    r3,r1,112
    1b64:	48 00 00 01 	bl      1b64 <._Z4testPv+0x124>
    1b68:	60 00 00 00 	nop
    1b6c:	e9 3d 00 20 	ld      r9,32(r29)
    1b70:	7f 83 4b 92 	divdu   r28,r3,r9
    1b74:	7f 9c 49 d2 	mulld   r28,r28,r9
    1b78:	7c 7c 18 50 	subf    r3,r28,r3
    1b7c:	3b 83 00 01 	addi    r28,r3,1
        if(set_add(TM_ARG val) == 0) {
    1b80:	7f 83 e3 78 	mr      r3,r28
    1b84:	48 00 00 01 	bl      1b84 <._Z4testPv+0x144>
    1b88:	2f a3 00 00 	cmpdi   cr7,r3,0
    1b8c:	41 9e ff 30 	beq     cr7,1abc <._Z4testPv+0x7c>
  while (myOps > 0) {
    op = rand_r(&mySeed) % 100;
    if (op < update) {
      if (val == -1) {
        /* Add random value */
        val = (rand_r(&mySeed) % range) + 1;
    1b90:	7f 9e e3 78 	mr      r30,r28
    1b94:	4b ff ff 28 	b       1abc <._Z4testPv+0x7c>
    1b98:	00 00 00 00 	.long 0x0
    1b9c:	00 09 00 01 	.long 0x90001
    1ba0:	80 05 00 00 	lwz     r0,0(r5)

Disassembly of section .text.startup:

0000000000000000 <.main>:

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
   0:	7c 08 02 a6 	mflr    r0
   4:	fa 61 ff 98 	std     r19,-104(r1)
   8:	fb 41 ff d0 	std     r26,-48(r1)
    {"range",                     required_argument, NULL, 'r'},
    {"seed",                      required_argument, NULL, 's'},
    {"buckets",                   required_argument, NULL, 'b'},
    {"update-rate",               required_argument, NULL, 'u'},
    {NULL, 0, NULL, 0}
  };
   c:	38 a0 01 20 	li      r5,288

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
  10:	fb 61 ff d8 	std     r27,-40(r1)
  14:	fb 81 ff e0 	std     r28,-32(r1)
  18:	7c 7c 1b 78 	mr      r28,r3

  int i, c;
  long val;
  operations = DEFAULT_DURATION;
  unsigned int initial = DEFAULT_INITIAL;
  nb_threads = DEFAULT_NB_THREADS;
  1c:	3e 62 00 00 	addis   r19,r2,0

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
  20:	fb a1 ff e8 	std     r29,-24(r1)
  24:	fb c1 ff f0 	std     r30,-16(r1)
  28:	7c 9d 23 78 	mr      r29,r4
    {"range",                     required_argument, NULL, 'r'},
    {"seed",                      required_argument, NULL, 's'},
    {"buckets",                   required_argument, NULL, 'b'},
    {"update-rate",               required_argument, NULL, 'u'},
    {NULL, 0, NULL, 0}
  };
  2c:	3c 82 00 00 	addis   r4,r2,0

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
  30:	fb e1 ff f8 	std     r31,-8(r1)
  34:	fa 81 ff a0 	std     r20,-96(r1)
    {"range",                     required_argument, NULL, 'r'},
    {"seed",                      required_argument, NULL, 's'},
    {"buckets",                   required_argument, NULL, 'b'},
    {"update-rate",               required_argument, NULL, 'u'},
    {NULL, 0, NULL, 0}
  };
  38:	38 84 00 00 	addi    r4,r4,0
  3c:	3f c2 00 00 	addis   r30,r2,0

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
  40:	fa a1 ff a8 	std     r21,-88(r1)
  44:	fa c1 ff b0 	std     r22,-80(r1)
       exit(0);
     case 'd':
       operations = atoi(optarg);
       break;
     case 'b':
       N_BUCKETS = atoi(optarg);
  48:	3f 42 00 00 	addis   r26,r2,0
  };

  int i, c;
  long val;
  operations = DEFAULT_DURATION;
  unsigned int initial = DEFAULT_INITIAL;
  4c:	3b 60 01 00 	li      r27,256

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
  50:	f8 01 00 10 	std     r0,16(r1)
  54:	fa e1 ff b8 	std     r23,-72(r1)
  58:	3b de 00 00 	addi    r30,r30,0
  range = DEFAULT_RANGE;
  update = DEFAULT_UPDATE;
  N_BUCKETS = 512;

  while(1) {
    i = 0;
  5c:	3b e0 00 00 	li      r31,0

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
  60:	fb 01 ff c0 	std     r24,-64(r1)
  64:	fb 21 ff c8 	std     r25,-56(r1)
       exit(0);
     case 'd':
       operations = atoi(optarg);
       break;
     case 'b':
       N_BUCKETS = atoi(optarg);
  68:	3b 5a 00 00 	addi    r26,r26,0

# define no_argument        0
# define required_argument  1
# define optional_argument  2

MAIN(argc, argv) {
  6c:	f8 21 fd 51 	stdu    r1,-688(r1)
    {"range",                     required_argument, NULL, 'r'},
    {"seed",                      required_argument, NULL, 's'},
    {"buckets",                   required_argument, NULL, 'b'},
    {"update-rate",               required_argument, NULL, 'u'},
    {NULL, 0, NULL, 0}
  };
  70:	38 61 00 f0 	addi    r3,r1,240
  74:	48 00 00 01 	bl      74 <.main+0x74>
  78:	60 00 00 00 	nop

  int i, c;
  long val;
  operations = DEFAULT_DURATION;
  7c:	39 40 27 10 	li      r10,10000
  80:	3d 02 00 00 	addis   r8,r2,0
  unsigned int initial = DEFAULT_INITIAL;
  nb_threads = DEFAULT_NB_THREADS;
  range = DEFAULT_RANGE;
  84:	39 20 ff ff 	li      r9,-1
  88:	79 29 04 20 	clrldi  r9,r9,48
    {NULL, 0, NULL, 0}
  };

  int i, c;
  long val;
  operations = DEFAULT_DURATION;
  8c:	f9 48 00 00 	std     r10,0(r8)
  unsigned int initial = DEFAULT_INITIAL;
  nb_threads = DEFAULT_NB_THREADS;
  90:	39 40 00 01 	li      r10,1
  range = DEFAULT_RANGE;
  94:	3d 02 00 00 	addis   r8,r2,0

  int i, c;
  long val;
  operations = DEFAULT_DURATION;
  unsigned int initial = DEFAULT_INITIAL;
  nb_threads = DEFAULT_NB_THREADS;
  98:	91 53 00 00 	stw     r10,0(r19)
  range = DEFAULT_RANGE;
  update = DEFAULT_UPDATE;
  9c:	3d 42 00 00 	addis   r10,r2,0
  N_BUCKETS = 512;
  a0:	3e 62 00 00 	addis   r19,r2,0
  int i, c;
  long val;
  operations = DEFAULT_DURATION;
  unsigned int initial = DEFAULT_INITIAL;
  nb_threads = DEFAULT_NB_THREADS;
  range = DEFAULT_RANGE;
  a4:	f9 28 00 00 	std     r9,0(r8)
  update = DEFAULT_UPDATE;
  a8:	39 20 00 14 	li      r9,20
  ac:	91 2a 00 00 	stw     r9,0(r10)
  N_BUCKETS = 512;
  b0:	39 20 02 00 	li      r9,512
  b4:	91 33 00 00 	stw     r9,0(r19)

  while(1) {
    i = 0;
    c = getopt_long(argc, argv, "hd:i:n:b:r:s:u:", long_options, &i);
  b8:	7f 83 e3 78 	mr      r3,r28
  bc:	7f a4 eb 78 	mr      r4,r29
  range = DEFAULT_RANGE;
  update = DEFAULT_UPDATE;
  N_BUCKETS = 512;

  while(1) {
    i = 0;
  c0:	93 e1 02 30 	stw     r31,560(r1)
    c = getopt_long(argc, argv, "hd:i:n:b:r:s:u:", long_options, &i);
  c4:	7f c5 f3 78 	mr      r5,r30
  c8:	38 c1 00 f0 	addi    r6,r1,240
  cc:	38 e1 02 30 	addi    r7,r1,560
  d0:	48 00 00 01 	bl      d0 <.main+0xd0>
  d4:	60 00 00 00 	nop

    if(c == -1)
  d8:	2f 83 ff ff 	cmpwi   cr7,r3,-1
  dc:	41 9e 01 ec 	beq     cr7,2c8 <.main+0x2c8>
      break;

    if(c == 0 && long_options[i].flag == 0)
  e0:	2f a3 00 00 	cmpdi   cr7,r3,0
  e4:	40 9e 00 2c 	bne     cr7,110 <.main+0x110>
  e8:	e9 21 02 32 	lwa     r9,560(r1)
  ec:	38 01 00 f0 	addi    r0,r1,240
  f0:	79 29 2e a4 	rldicr  r9,r9,5,58
  f4:	7d 20 4a 14 	add     r9,r0,r9
  f8:	39 49 00 10 	addi    r10,r9,16
  fc:	e9 29 00 10 	ld      r9,16(r9)
 100:	2f a9 00 00 	cmpdi   cr7,r9,0
 104:	40 9e ff b4 	bne     cr7,b8 <.main+0xb8>
      c = long_options[i].val;
 108:	e8 6a 00 0a 	lwa     r3,8(r10)
 10c:	60 42 00 00 	ori     r2,r2,0

    switch(c) {
 110:	2f 83 00 68 	cmpwi   cr7,r3,104
 114:	41 9e 01 28 	beq     cr7,23c <.main+0x23c>
 118:	40 9d 00 48 	ble     cr7,160 <.main+0x160>
 11c:	2f 83 00 72 	cmpwi   cr7,r3,114
 120:	41 9e 00 f0 	beq     cr7,210 <.main+0x210>
 124:	40 9d 00 84 	ble     cr7,1a8 <.main+0x1a8>
 128:	2f 83 00 73 	cmpwi   cr7,r3,115
 12c:	41 9e 01 2c 	beq     cr7,258 <.main+0x258>
 130:	2f 83 00 75 	cmpwi   cr7,r3,117
 134:	40 9e 00 d0 	bne     cr7,204 <.main+0x204>
       break;
     case 's':
       seed = atoi(optarg);
       break;
     case 'u':
       update = atoi(optarg);
 138:	3d 22 00 00 	addis   r9,r2,0
 13c:	e9 29 00 00 	ld      r9,0(r9)
#ifdef __USE_EXTERN_INLINES
__BEGIN_NAMESPACE_STD
__extern_inline int
__NTH (atoi (const char *__nptr))
{
  return (int) strtol (__nptr, (char **) NULL, 10);
 140:	38 80 00 00 	li      r4,0
 144:	38 a0 00 0a 	li      r5,10
 148:	3e 62 00 00 	addis   r19,r2,0
 14c:	e8 69 00 00 	ld      r3,0(r9)
 150:	48 00 00 01 	bl      150 <.main+0x150>
 154:	60 00 00 00 	nop
 158:	90 73 00 00 	stw     r3,0(r19)
       break;
 15c:	4b ff ff 5c 	b       b8 <.main+0xb8>
      break;

    if(c == 0 && long_options[i].flag == 0)
      c = long_options[i].val;

    switch(c) {
 160:	2f 83 00 3f 	cmpwi   cr7,r3,63
 164:	41 9e 00 7c 	beq     cr7,1e0 <.main+0x1e0>
 168:	40 9d 00 94 	ble     cr7,1fc <.main+0x1fc>
 16c:	2f 83 00 62 	cmpwi   cr7,r3,98
 170:	41 9e 01 10 	beq     cr7,280 <.main+0x280>
 174:	2f 83 00 64 	cmpwi   cr7,r3,100
 178:	40 9e 00 8c 	bne     cr7,204 <.main+0x204>
              "  -u, --update-rate <int>\n"
              "        Percentage of update transactions (default=" XSTR(DEFAULT_UPDATE) ")\n"
         );
       exit(0);
     case 'd':
       operations = atoi(optarg);
 17c:	3d 22 00 00 	addis   r9,r2,0
 180:	e9 29 00 00 	ld      r9,0(r9)
 184:	38 80 00 00 	li      r4,0
 188:	38 a0 00 0a 	li      r5,10
 18c:	e8 69 00 00 	ld      r3,0(r9)
 190:	48 00 00 01 	bl      190 <.main+0x190>
 194:	60 00 00 00 	nop
 198:	3d 22 00 00 	addis   r9,r2,0
 19c:	7c 63 07 b4 	extsw   r3,r3
 1a0:	f8 69 00 00 	std     r3,0(r9)
       break;
 1a4:	4b ff ff 14 	b       b8 <.main+0xb8>
      break;

    if(c == 0 && long_options[i].flag == 0)
      c = long_options[i].val;

    switch(c) {
 1a8:	2f 83 00 69 	cmpwi   cr7,r3,105
 1ac:	41 9e 00 f8 	beq     cr7,2a4 <.main+0x2a4>
 1b0:	2f 83 00 6e 	cmpwi   cr7,r3,110
 1b4:	40 9e 00 50 	bne     cr7,204 <.main+0x204>
       break;
     case 'i':
       initial = atoi(optarg);
       break;
     case 'n':
       nb_threads = atoi(optarg);
 1b8:	3d 22 00 00 	addis   r9,r2,0
 1bc:	e9 29 00 00 	ld      r9,0(r9)
 1c0:	38 80 00 00 	li      r4,0
 1c4:	38 a0 00 0a 	li      r5,10
 1c8:	e8 69 00 00 	ld      r3,0(r9)
 1cc:	48 00 00 01 	bl      1cc <.main+0x1cc>
 1d0:	60 00 00 00 	nop
 1d4:	3d 22 00 00 	addis   r9,r2,0
 1d8:	90 69 00 00 	stw     r3,0(r9)
       break;
 1dc:	4b ff fe dc 	b       b8 <.main+0xb8>
       break;
     case 'u':
       update = atoi(optarg);
       break;
     case '?':
       printf("Use -h or --help for help\n");
 1e0:	3c 62 00 00 	addis   r3,r2,0
 1e4:	38 63 00 00 	addi    r3,r3,0
 1e8:	48 00 00 01 	bl      1e8 <.main+0x1e8>
 1ec:	60 00 00 00 	nop
       exit(0);
 1f0:	38 60 00 00 	li      r3,0
 1f4:	48 00 00 01 	bl      1f4 <.main+0x1f4>
 1f8:	60 00 00 00 	nop
      break;

    if(c == 0 && long_options[i].flag == 0)
      c = long_options[i].val;

    switch(c) {
 1fc:	2f 83 00 00 	cmpwi   cr7,r3,0
 200:	41 de fe b8 	beq-    cr7,b8 <.main+0xb8>
       break;
     case '?':
       printf("Use -h or --help for help\n");
       exit(0);
     default:
       exit(1);
 204:	38 60 00 01 	li      r3,1
 208:	48 00 00 01 	bl      208 <.main+0x208>
 20c:	60 00 00 00 	nop
       break;
     case 'n':
       nb_threads = atoi(optarg);
       break;
     case 'r':
       range = atoi(optarg);
 210:	3d 22 00 00 	addis   r9,r2,0
 214:	e9 29 00 00 	ld      r9,0(r9)
 218:	38 80 00 00 	li      r4,0
 21c:	38 a0 00 0a 	li      r5,10
 220:	3e 62 00 00 	addis   r19,r2,0
 224:	e8 69 00 00 	ld      r3,0(r9)
 228:	48 00 00 01 	bl      228 <.main+0x228>
 22c:	60 00 00 00 	nop
 230:	7c 63 07 b4 	extsw   r3,r3
 234:	f8 73 00 00 	std     r3,0(r19)
       break;
 238:	4b ff fe 80 	b       b8 <.main+0xb8>
              "        Range of integer values inserted in set (default=" XSTR(DEFAULT_RANGE) ")\n"
              "  -s, --seed <int>\n"
              "        RNG seed (0=time-based, default=" XSTR(DEFAULT_SEED) ")\n"
              "  -u, --update-rate <int>\n"
              "        Percentage of update transactions (default=" XSTR(DEFAULT_UPDATE) ")\n"
         );
 23c:	3c 62 00 00 	addis   r3,r2,0
 240:	38 63 00 00 	addi    r3,r3,0
 244:	48 00 00 01 	bl      244 <.main+0x244>
 248:	60 00 00 00 	nop
       exit(0);
 24c:	38 60 00 00 	li      r3,0
 250:	48 00 00 01 	bl      250 <.main+0x250>
 254:	60 00 00 00 	nop
       break;
     case 'r':
       range = atoi(optarg);
       break;
     case 's':
       seed = atoi(optarg);
 258:	3d 22 00 00 	addis   r9,r2,0
 25c:	e9 29 00 00 	ld      r9,0(r9)
 260:	38 80 00 00 	li      r4,0
 264:	38 a0 00 0a 	li      r5,10
 268:	e8 69 00 00 	ld      r3,0(r9)
 26c:	48 00 00 01 	bl      26c <.main+0x26c>
 270:	60 00 00 00 	nop
 274:	3d 22 00 00 	addis   r9,r2,0
 278:	90 69 00 00 	stw     r3,0(r9)
       break;
 27c:	4b ff fe 3c 	b       b8 <.main+0xb8>
       exit(0);
     case 'd':
       operations = atoi(optarg);
       break;
     case 'b':
       N_BUCKETS = atoi(optarg);
 280:	3d 22 00 00 	addis   r9,r2,0
 284:	e9 29 00 00 	ld      r9,0(r9)
 288:	38 80 00 00 	li      r4,0
 28c:	38 a0 00 0a 	li      r5,10
 290:	e8 69 00 00 	ld      r3,0(r9)
 294:	48 00 00 01 	bl      294 <.main+0x294>
 298:	60 00 00 00 	nop
 29c:	90 7a 00 00 	stw     r3,0(r26)
       break;
 2a0:	4b ff fe 18 	b       b8 <.main+0xb8>
     case 'i':
       initial = atoi(optarg);
 2a4:	3d 22 00 00 	addis   r9,r2,0
 2a8:	e9 29 00 00 	ld      r9,0(r9)
 2ac:	38 80 00 00 	li      r4,0
 2b0:	38 a0 00 0a 	li      r5,10
 2b4:	e8 69 00 00 	ld      r3,0(r9)
 2b8:	48 00 00 01 	bl      2b8 <.main+0x2b8>
 2bc:	60 00 00 00 	nop
 2c0:	78 7b 00 20 	clrldi  r27,r3,32
       break;
 2c4:	4b ff fd f4 	b       b8 <.main+0xb8>
     default:
       exit(1);
    }
  }

  if (seed == 0)
 2c8:	3c 62 00 00 	addis   r3,r2,0
 2cc:	80 63 00 00 	lwz     r3,0(r3)
 2d0:	2f a3 00 00 	cmpdi   cr7,r3,0
 2d4:	41 9e 03 d8 	beq     cr7,6ac <.main+0x6ac>
    srand((int)time(0));
  else
    srand(seed);
 2d8:	48 00 00 01 	bl      2d8 <.main+0x2d8>
 2dc:	60 00 00 00 	nop

  SIM_GET_NUM_CPU(nb_threads);
  TM_STARTUP(nb_threads);
  P_MEMORY_STARTUP(nb_threads);
  thread_startup(nb_threads);
 2e0:	3c 62 00 00 	addis   r3,r2,0
 2e4:	80 63 00 00 	lwz     r3,0(r3)

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));
 2e8:	3e 62 00 00 	addis   r19,r2,0
    srand(seed);

  SIM_GET_NUM_CPU(nb_threads);
  TM_STARTUP(nb_threads);
  P_MEMORY_STARTUP(nb_threads);
  thread_startup(nb_threads);
 2ec:	48 00 00 01 	bl      2ec <.main+0x2ec>
 2f0:	60 00 00 00 	nop

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));
 2f4:	3f e2 00 00 	addis   r31,r2,0
 2f8:	83 ff 00 00 	lwz     r31,0(r31)
 2fc:	7f fc 07 b4 	extsw   r28,r31
 300:	7b 83 1f 24 	rldicr  r3,r28,3,60
 304:	48 00 00 01 	bl      304 <.main+0x304>
 308:	60 00 00 00 	nop

  for (i = 0; i < N_BUCKETS; i++) {
 30c:	2f 9f 00 00 	cmpwi   cr7,r31,0
 310:	39 20 00 00 	li      r9,0
  SIM_GET_NUM_CPU(nb_threads);
  TM_STARTUP(nb_threads);
  P_MEMORY_STARTUP(nb_threads);
  thread_startup(nb_threads);

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));
 314:	f8 73 00 00 	std     r3,0(r19)

  for (i = 0; i < N_BUCKETS; i++) {
 318:	91 21 02 30 	stw     r9,560(r1)
 31c:	40 9d 00 90 	ble     cr7,3ac <.main+0x3ac>
    bucket[i] = (List*) malloc (sizeof(List));
    bucket[i]->sentinel = (Node_HM*) malloc(sizeof(Node_HM));
    bucket[i]->sentinel->m_val = LONG_MIN;
 320:	3b a0 ff ff 	li      r29,-1
 324:	3f e2 00 00 	addis   r31,r2,0
  P_MEMORY_STARTUP(nb_threads);
  thread_startup(nb_threads);

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));

  for (i = 0; i < N_BUCKETS; i++) {
 328:	39 40 00 00 	li      r10,0
 32c:	3b ff 00 00 	addi    r31,r31,0
    bucket[i] = (List*) malloc (sizeof(List));
    bucket[i]->sentinel = (Node_HM*) malloc(sizeof(Node_HM));
    bucket[i]->sentinel->m_val = LONG_MIN;
 330:	7b bd 00 04 	rldicr  r29,r29,0,0
    bucket[i]->sentinel->m_next = NULL;
 334:	3b 40 00 00 	li      r26,0
 338:	60 00 00 00 	nop
 33c:	60 42 00 00 	ori     r2,r2,0
  thread_startup(nb_threads);

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));

  for (i = 0; i < N_BUCKETS; i++) {
    bucket[i] = (List*) malloc (sizeof(List));
 340:	38 60 00 08 	li      r3,8
 344:	79 5e 1f 24 	rldicr  r30,r10,3,60
 348:	eb 3f 00 00 	ld      r25,0(r31)
 34c:	48 00 00 01 	bl      34c <.main+0x34c>
 350:	60 00 00 00 	nop
    bucket[i]->sentinel = (Node_HM*) malloc(sizeof(Node_HM));
 354:	e9 21 02 32 	lwa     r9,560(r1)
  thread_startup(nb_threads);

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));

  for (i = 0; i < N_BUCKETS; i++) {
    bucket[i] = (List*) malloc (sizeof(List));
 358:	7c 79 f1 2a 	stdx    r3,r25,r30
    bucket[i]->sentinel = (Node_HM*) malloc(sizeof(Node_HM));
 35c:	38 60 00 90 	li      r3,144
 360:	79 29 1f 24 	rldicr  r9,r9,3,60
 364:	e9 5f 00 00 	ld      r10,0(r31)
 368:	7f ca 48 2a 	ldx     r30,r10,r9
 36c:	48 00 00 01 	bl      36c <.main+0x36c>
 370:	60 00 00 00 	nop
    bucket[i]->sentinel->m_val = LONG_MIN;
 374:	81 21 02 30 	lwz     r9,560(r1)

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));

  for (i = 0; i < N_BUCKETS; i++) {
    bucket[i] = (List*) malloc (sizeof(List));
    bucket[i]->sentinel = (Node_HM*) malloc(sizeof(Node_HM));
 378:	f8 7e 00 00 	std     r3,0(r30)
    bucket[i]->sentinel->m_val = LONG_MIN;
 37c:	7d 28 07 b4 	extsw   r8,r9
  P_MEMORY_STARTUP(nb_threads);
  thread_startup(nb_threads);

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));

  for (i = 0; i < N_BUCKETS; i++) {
 380:	39 29 00 01 	addi    r9,r9,1
    bucket[i] = (List*) malloc (sizeof(List));
    bucket[i]->sentinel = (Node_HM*) malloc(sizeof(Node_HM));
    bucket[i]->sentinel->m_val = LONG_MIN;
 384:	79 08 1f 24 	rldicr  r8,r8,3,60
  P_MEMORY_STARTUP(nb_threads);
  thread_startup(nb_threads);

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));

  for (i = 0; i < N_BUCKETS; i++) {
 388:	7f 9c 48 00 	cmpw    cr7,r28,r9
 38c:	7d 2a 07 b4 	extsw   r10,r9
    bucket[i] = (List*) malloc (sizeof(List));
    bucket[i]->sentinel = (Node_HM*) malloc(sizeof(Node_HM));
    bucket[i]->sentinel->m_val = LONG_MIN;
 390:	e8 ff 00 00 	ld      r7,0(r31)
 394:	7d 07 40 2a 	ldx     r8,r7,r8
 398:	e9 08 00 00 	ld      r8,0(r8)
 39c:	fb a8 00 00 	std     r29,0(r8)
    bucket[i]->sentinel->m_next = NULL;
 3a0:	fb 48 00 88 	std     r26,136(r8)
  P_MEMORY_STARTUP(nb_threads);
  thread_startup(nb_threads);

  bucket = (List**) malloc(N_BUCKETS*sizeof(List*));

  for (i = 0; i < N_BUCKETS; i++) {
 3a4:	91 21 02 30 	stw     r9,560(r1)
 3a8:	41 9d ff 98 	bgt     cr7,340 <.main+0x340>
    bucket[i]->sentinel->m_val = LONG_MIN;
    bucket[i]->sentinel->m_next = NULL;
  }

  /* Populate set */
  for (i = 0; i < initial; i++) {
 3ac:	2f bb 00 00 	cmpdi   cr7,r27,0
 3b0:	39 20 00 00 	li      r9,0
 3b4:	3f e2 00 00 	addis   r31,r2,0
 3b8:	3b ff 00 00 	addi    r31,r31,0
 3bc:	91 21 02 30 	stw     r9,560(r1)
 3c0:	41 9e 00 44 	beq     cr7,404 <.main+0x404>
 3c4:	60 00 00 00 	nop
 3c8:	60 00 00 00 	nop
 3cc:	60 42 00 00 	ori     r2,r2,0
    val = (rand() % range) + 1;
 3d0:	48 00 00 01 	bl      3d0 <.main+0x3d0>
 3d4:	60 00 00 00 	nop
 3d8:	e9 3f 00 20 	ld      r9,32(r31)
 3dc:	7d 43 4b 92 	divdu   r10,r3,r9
 3e0:	7d 2a 49 d2 	mulld   r9,r10,r9
 3e4:	7c 69 18 50 	subf    r3,r9,r3
    return hm_remove_htm(TM_ARG (bucket[val % N_BUCKETS]), val);
}


long set_add_seq(long val) {
	priv_insert_seq(val);
 3e8:	38 63 00 01 	addi    r3,r3,1
 3ec:	48 00 00 01 	bl      3ec <.main+0x3ec>
    bucket[i]->sentinel->m_val = LONG_MIN;
    bucket[i]->sentinel->m_next = NULL;
  }

  /* Populate set */
  for (i = 0; i < initial; i++) {
 3f0:	81 21 02 30 	lwz     r9,560(r1)
 3f4:	39 29 00 01 	addi    r9,r9,1
 3f8:	7f 89 d8 40 	cmplw   cr7,r9,r27
 3fc:	91 21 02 30 	stw     r9,560(r1)
 400:	41 9c ff d0 	blt     cr7,3d0 <.main+0x3d0>
    val = (rand() % range) + 1;
    set_add_seq(val);
  }
  printf("Added %d entries to set\n", initial);
 404:	3c 62 00 00 	addis   r3,r2,0
 408:	7f 64 db 78 	mr      r4,r27
 40c:	38 63 00 00 	addi    r3,r3,0
  TIMER_READ(stop);

  puts("done.");
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 410:	3a 60 00 50 	li      r19,80
  /* Populate set */
  for (i = 0; i < initial; i++) {
    val = (rand() % range) + 1;
    set_add_seq(val);
  }
  printf("Added %d entries to set\n", initial);
 414:	48 00 00 01 	bl      414 <.main+0x414>
 418:	60 00 00 00 	nop
  TIMER_READ(stop);

  puts("done.");
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 41c:	3b e0 00 00 	li      r31,0
 420:	3a 80 00 00 	li      r20,0
 424:	3a a0 00 00 	li      r21,0
 428:	3a c0 00 00 	li      r22,0
    val = (rand() % range) + 1;
    set_add_seq(val);
  }
  printf("Added %d entries to set\n", initial);

  seed = rand();
 42c:	48 00 00 01 	bl      42c <.main+0x42c>
 430:	60 00 00 00 	nop
 434:	3d 02 00 00 	addis   r8,r2,0
  TIMER_READ(start);
 438:	38 80 00 00 	li      r4,0
  TIMER_READ(stop);

  puts("done.");
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 43c:	3a e0 00 00 	li      r23,0
 440:	3b c0 00 00 	li      r30,0
    val = (rand() % range) + 1;
    set_add_seq(val);
  }
  printf("Added %d entries to set\n", initial);

  seed = rand();
 444:	90 68 00 00 	stw     r3,0(r8)
  TIMER_READ(start);
 448:	38 61 02 20 	addi    r3,r1,544
  TIMER_READ(stop);

  puts("done.");
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 44c:	3b a0 00 00 	li      r29,0
    set_add_seq(val);
  }
  printf("Added %d entries to set\n", initial);

  seed = rand();
  TIMER_READ(start);
 450:	48 00 00 01 	bl      450 <.main+0x450>
 454:	60 00 00 00 	nop
  GOTO_SIM();

  thread_start(test, NULL);
 458:	3c 62 00 00 	addis   r3,r2,0
 45c:	38 80 00 00 	li      r4,0
 460:	38 63 00 00 	addi    r3,r3,0
  TIMER_READ(stop);

  puts("done.");
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 464:	3b 00 00 00 	li      r24,0

  seed = rand();
  TIMER_READ(start);
  GOTO_SIM();

  thread_start(test, NULL);
 468:	48 00 00 01 	bl      468 <.main+0x468>
 46c:	60 00 00 00 	nop

  GOTO_REAL();
  TIMER_READ(stop);
 470:	38 80 00 00 	li      r4,0
 474:	38 61 02 10 	addi    r3,r1,528

  puts("done.");
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 478:	3b 20 00 00 	li      r25,0
 47c:	3b 40 00 00 	li      r26,0
  GOTO_SIM();

  thread_start(test, NULL);

  GOTO_REAL();
  TIMER_READ(stop);
 480:	48 00 00 01 	bl      480 <.main+0x480>
 484:	60 00 00 00 	nop

  puts("done.");
 488:	3c 62 00 00 	addis   r3,r2,0
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 48c:	3b 60 00 00 	li      r27,0
  thread_start(test, NULL);

  GOTO_REAL();
  TIMER_READ(stop);

  puts("done.");
 490:	38 63 00 00 	addi    r3,r3,0
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
PRINT_STATS();
  fflush(stdout);
 494:	3b 80 00 00 	li      r28,0
  thread_start(test, NULL);

  GOTO_REAL();
  TIMER_READ(stop);

  puts("done.");
 498:	48 00 00 01 	bl      498 <.main+0x498>
 49c:	60 00 00 00 	nop
  printf("\nTime = %0.6lf\n", TIMER_DIFF_SECONDS(start, stop));
 4a0:	c9 01 02 18 	lfd     f8,536(r1)
 4a4:	c9 41 02 28 	lfd     f10,552(r1)
 4a8:	3d 22 00 00 	addis   r9,r2,0
 4ac:	3c 62 00 00 	addis   r3,r2,0
 4b0:	38 63 00 00 	addi    r3,r3,0
 4b4:	c1 89 00 00 	lfs     f12,0(r9)
 4b8:	f1 60 55 e0 	xscvsxddp vs11,vs10
 4bc:	f0 00 45 e0 	xscvsxddp vs0,vs8
 4c0:	c9 01 02 10 	lfd     f8,528(r1)
 4c4:	f1 20 45 e0 	xscvsxddp vs9,vs8
 4c8:	c9 01 02 20 	lfd     f8,544(r1)
 4cc:	fc 00 60 24 	fdiv    f0,f0,f12
 4d0:	fd 8b 60 24 	fdiv    f12,f11,f12
 4d4:	f1 40 45 e0 	xscvsxddp vs10,vs8
 4d8:	fc 09 00 2a 	fadd    f0,f9,f0
 4dc:	fd 8a 60 2a 	fadd    f12,f10,f12
 4e0:	fc 00 60 28 	fsub    f0,f0,f12
 4e4:	fc 20 00 90 	fmr     f1,f0
 4e8:	7c 04 00 66 	mfvsrd  r4,vs0
 4ec:	48 00 00 01 	bl      4ec <.main+0x4ec>
 4f0:	60 00 00 00 	nop
PRINT_STATS();
  fflush(stdout);
 4f4:	3d 22 00 00 	addis   r9,r2,0
 4f8:	e9 29 00 00 	ld      r9,0(r9)
 4fc:	e8 69 00 00 	ld      r3,0(r9)
 500:	48 00 00 01 	bl      500 <.main+0x500>
 504:	60 00 00 00 	nop
 508:	7e 69 03 a6 	mtctr   r19
 50c:	3d 22 00 00 	addis   r9,r2,0
 510:	e9 29 00 00 	ld      r9,0(r9)
 514:	39 00 00 00 	li      r8,0
 518:	38 80 00 00 	li      r4,0
 51c:	38 60 00 00 	li      r3,0
 520:	39 60 00 00 	li      r11,0
 524:	38 e0 00 00 	li      r7,0
 528:	39 40 00 00 	li      r10,0
 52c:	38 c0 00 00 	li      r6,0
 530:	38 a0 00 00 	li      r5,0
 534:	60 00 00 00 	nop
 538:	60 00 00 00 	nop
 53c:	60 42 00 00 	ori     r2,r2,0

  TM_SHUTDOWN();
 540:	ea 69 00 00 	ld      r19,0(r9)
 544:	e9 89 00 90 	ld      r12,144(r9)
 548:	e8 09 00 98 	ld      r0,152(r9)
 54c:	39 29 01 80 	addi    r9,r9,384
 550:	7c a5 9a 14 	add     r5,r5,r19
 554:	ea 69 fe 88 	ld      r19,-376(r9)
 558:	7f ff 62 14 	add     r31,r31,r12
 55c:	7d 08 02 14 	add     r8,r8,r0
 560:	7c c6 9a 14 	add     r6,r6,r19
 564:	ea 69 fe 90 	ld      r19,-368(r9)
 568:	7d 4a 9a 14 	add     r10,r10,r19
 56c:	ea 69 fe b0 	ld      r19,-336(r9)
 570:	7f 9c 9a 14 	add     r28,r28,r19
 574:	ea 69 fe 98 	ld      r19,-360(r9)
 578:	7f 7b 9a 14 	add     r27,r27,r19
 57c:	ea 69 fe a0 	ld      r19,-352(r9)
 580:	7f 5a 9a 14 	add     r26,r26,r19
 584:	ea 69 fe a8 	ld      r19,-344(r9)
 588:	7f 39 9a 14 	add     r25,r25,r19
 58c:	ea 69 fe c0 	ld      r19,-320(r9)
 590:	7f 18 9a 14 	add     r24,r24,r19
 594:	ea 69 fe b8 	ld      r19,-328(r9)
 598:	7f bd 9a 14 	add     r29,r29,r19
 59c:	ea 69 fe c8 	ld      r19,-312(r9)
 5a0:	7f de 9a 14 	add     r30,r30,r19
 5a4:	ea 69 fe d0 	ld      r19,-304(r9)
 5a8:	7c e7 9a 14 	add     r7,r7,r19
 5ac:	ea 69 fe d8 	ld      r19,-296(r9)
 5b0:	7d 6b 9a 14 	add     r11,r11,r19
 5b4:	ea 69 fe f8 	ld      r19,-264(r9)
 5b8:	7c 63 9a 14 	add     r3,r3,r19
 5bc:	ea 69 fe e0 	ld      r19,-288(r9)
 5c0:	7e f7 9a 14 	add     r23,r23,r19
 5c4:	ea 69 fe e8 	ld      r19,-280(r9)
 5c8:	7e d6 9a 14 	add     r22,r22,r19
 5cc:	ea 69 fe f0 	ld      r19,-272(r9)
 5d0:	7e b5 9a 14 	add     r21,r21,r19
 5d4:	ea 69 ff 00 	ld      r19,-256(r9)
 5d8:	7e 94 9a 14 	add     r20,r20,r19
 5dc:	ea 69 ff 08 	ld      r19,-248(r9)
 5e0:	7c 84 9a 14 	add     r4,r4,r19
 5e4:	42 00 ff 5c 	bdnz    540 <.main+0x540>
 5e8:	7d 24 fa 14 	add     r9,r4,r31
 5ec:	f9 61 00 a8 	std     r11,168(r1)
 5f0:	fb 61 00 70 	std     r27,112(r1)
 5f4:	7d 29 1a 14 	add     r9,r9,r3
 5f8:	fb 41 00 78 	std     r26,120(r1)
 5fc:	fb 21 00 80 	std     r25,128(r1)
 600:	7d 29 5a 14 	add     r9,r9,r11
 604:	fb 81 00 88 	std     r28,136(r1)
 608:	fb a1 00 90 	std     r29,144(r1)
 60c:	fb 01 00 98 	std     r24,152(r1)
 610:	fb c1 00 a0 	std     r30,160(r1)
 614:	7d 29 f2 14 	add     r9,r9,r30
 618:	fa e1 00 b0 	std     r23,176(r1)
 61c:	fa c1 00 b8 	std     r22,184(r1)
 620:	7d 29 ea 14 	add     r9,r9,r29
 624:	fa a1 00 c0 	std     r21,192(r1)
 628:	f8 61 00 c8 	std     r3,200(r1)
 62c:	7d 29 e2 14 	add     r9,r9,r28
 630:	3c 62 00 00 	addis   r3,r2,0
 634:	f8 81 00 d0 	std     r4,208(r1)
 638:	fa 81 00 d8 	std     r20,216(r1)
 63c:	7c 87 42 14 	add     r4,r7,r8
 640:	7d 29 52 14 	add     r9,r9,r10
 644:	fb e1 00 e0 	std     r31,224(r1)
 648:	7c 84 32 14 	add     r4,r4,r6
 64c:	38 63 00 00 	addi    r3,r3,0
 650:	7c 84 2a 14 	add     r4,r4,r5
 654:	48 00 00 01 	bl      654 <.main+0x654>
 658:	60 00 00 00 	nop
  P_MEMORY_SHUTDOWN();
  GOTO_SIM();
  thread_shutdown();
 65c:	48 00 00 01 	bl      65c <.main+0x65c>
 660:	60 00 00 00 	nop
  MAIN_RETURN(0);
}
 664:	38 21 02 b0 	addi    r1,r1,688
 668:	38 60 00 00 	li      r3,0
 66c:	e8 01 00 10 	ld      r0,16(r1)
 670:	ea 61 ff 98 	ld      r19,-104(r1)
 674:	ea 81 ff a0 	ld      r20,-96(r1)
 678:	ea a1 ff a8 	ld      r21,-88(r1)
 67c:	ea c1 ff b0 	ld      r22,-80(r1)
 680:	ea e1 ff b8 	ld      r23,-72(r1)
 684:	eb 01 ff c0 	ld      r24,-64(r1)
 688:	eb 21 ff c8 	ld      r25,-56(r1)
 68c:	eb 41 ff d0 	ld      r26,-48(r1)
 690:	eb 61 ff d8 	ld      r27,-40(r1)
 694:	eb 81 ff e0 	ld      r28,-32(r1)
 698:	eb a1 ff e8 	ld      r29,-24(r1)
 69c:	7c 08 03 a6 	mtlr    r0
 6a0:	eb c1 ff f0 	ld      r30,-16(r1)
 6a4:	eb e1 ff f8 	ld      r31,-8(r1)
 6a8:	4e 80 00 20 	blr
       exit(1);
    }
  }

  if (seed == 0)
    srand((int)time(0));
 6ac:	48 00 00 01 	bl      6ac <.main+0x6ac>
 6b0:	60 00 00 00 	nop
 6b4:	78 63 00 20 	clrldi  r3,r3,32
 6b8:	48 00 00 01 	bl      6b8 <.main+0x6b8>
 6bc:	60 00 00 00 	nop
 6c0:	4b ff fc 20 	b       2e0 <.main+0x2e0>
 6c4:	00 00 00 00 	.long 0x0
 6c8:	00 09 00 01 	.long 0x90001
 6cc:	80 0d 00 00 	lwz     r0,0(r13)
