#!/bin/bash

# test[1]="./genome/genome -g16384 -s64 -n16777216 -r1 -t" 
# test[2]="./intruder/intruder -a10 -l128 -n262144 -s1 -r1 -t" 
# test[3]="./kmeans/kmeans -m15 -n15 -t0.00001 -i kmeans/inputs/random-n65536-d32-c16.txt -r1 -p" 
# test[4]="./kmeans/kmeans -m40 -n40 -t0.00001 -i kmeans/inputs/random-n65536-d32-c16.txt -r1 -p" 
# test[5]="./labyrinth/labyrinth  -i labyrinth/inputs/random-x512-y512-z7-n512.txt -t" 
# test[6]="./ssca2/ssca2 -s20 -i1.0 -u1.0 -l3 -p3 -r1 -t" 
# test[7]="./vacation/vacation -n4 -q60 -u90 -r1048576 -t4194304 -a1 -c" 
# test[8]="./vacation/vacation  -n2 -q90 -u98 -r1048576 -t4194304 -a1 -c" 
# test[9]="./yada/yadome -g16384 -s64 -n16777216 -r1 -t" 
# test[2]="./genome/genome -n100000 -g3000 -s2000 -t" 
# test[3]="./vacation/vacation -n1000 -q90 -u100 -r2 -t10000 -a1 -c" 
# test[4]="./yada/yada -a15 -i yada/inputs/ttimeu10000.2 -r2 -t"
test[1]="./hashmap-static/hashmap -r32768 -u10 -i32768 -b256 -d1000000 -n" 
# test[2]="./hashmap-static/hashmap -r10000 -u90 -i1000 -d1000000 -n"
test[2]="./hashmap-static/hashmap -r32768 -u90 -i32768 -b256 -d500000 -n"
# test[3]="./hashmap/hashmap -r100000 -u50 -i1000 -d1000000 -n" 

LOG_SIZE1=1000
LOG_SIZE2=10000
LOG_SIZE3=100000
LOG_SIZE4=1000000

# 10us
PERIOD1=10
PERIOD2=100

# 1ms
PERIOD3=1000
PERIOD4=10000
PERIOD5=100000

THRESHOLD1=0.01
THRESHOLD2=0.10
THRESHOLD3=0.50
THRESHOLD4=0.90
THRESHOLD5=0.99

SAMPLES=10

function run_bench {

	rm -f $1

	for i in 1 2 
	do
		for t in 2 4 8 # 2 4 8 10 12 14 28 56 # 2 4 8
		do
			rm -f /tmp/*.socket
			ipcrm -M 0x00054321
			timeout 1m ${test[$i]} $t
		done
		echo "# ${test[$i]} COMPLETE!\n" >> $1
	done
}

# FORK
for l in $LOG_SIZE1 $LOG_SIZE2 $LOG_SIZE3 $LOG_SIZE4
do
	for p in $THRESHOLD1 $THRESHOLD2 $THRESHOLD3 $THRESHOLD4 $THRESHOLD5
	do
		for a in `seq $SAMPLES`
		do
			MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=5 LOG_SIZE=$l THRESHOLD=$p" ./build-datastructures.sh htm-sgl-nvm fork_a"$a"_l"$l"_p"$p".txt
			ipcrm -M 0x00054321 # kills the shared memory log segment
			run_bench fork_a"$a"_l"$l"_p"$p".txt
			sleep 0.1
		done
	done
done

# REACTIVE
for l in $LOG_SIZE1 $LOG_SIZE2 $LOG_SIZE3 $LOG_SIZE4
do
	for p in $THRESHOLD1 $THRESHOLD2 $THRESHOLD3 $THRESHOLD4 $THRESHOLD5
	do
		for a in `seq $SAMPLES`
		do
			MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=2 LOG_SIZE=$l THRESHOLD=$p" ./build-datastructures.sh htm-sgl-nvm reactive_a"$a"_l"$l"_p"$p".txt
			ipcrm -M 0x00054321 # kills the shared memory log segment
			run_bench reactive_a"$a"_l"$l"_p"$p".txt
			sleep 0.1
		done
	done
done
	
# WRAP
for l in $LOG_SIZE1 $LOG_SIZE2 $LOG_SIZE3 $LOG_SIZE4
do
	for a in `seq $SAMPLES`
	do
		MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=4 LOG_SIZE=$l" ./build-datastructures.sh htm-sgl-nvm no_manager_a"$a"_l"$l".txt
		ipcrm -M 0x00054321 # kills the shared memory log segment
		run_bench no_manager_a"$a"_l"$l".txt
		sleep 0.1
	done
done

# PERIODIC
for p in $PERIOD1 $PERIOD2 $PERIOD3 $PERIOD4 $PERIOD5
do
	for l in $LOG_SIZE1 $LOG_SIZE2 $LOG_SIZE3 $LOG_SIZE4
	do
		for a in `seq $SAMPLES`
		do
			MAKEFILE_ARGS="SOLUTION=4 DO_CHECKPOINT=1 LOG_SIZE=$l PERIOD=$p" ./build-datastructures.sh htm-sgl-nvm periodic_a"$a"_l"$l"_p"$p".txt
			ipcrm -M 0x00054321 # kills the shared memory log segment
			run_bench periodic_a"$a"_l"$l"_p"$p".txt
			sleep 0.1
		done
	done
done
