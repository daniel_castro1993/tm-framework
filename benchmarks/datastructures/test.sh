backends[1]="htm-sgl-expbf 10 0"
backends[2]="htm-rot-expbf 10 5"
backends[3]="norec 0 0"


for b in 3
do
	bash build-datastructures.sh ${backends[$b]}
	for i in 1 2 3 4 5 6 7 8 9 10
	do
		for t in 64 80
		do 
			echo "${backends[$b]} | $i | $t"
			mkdir -p out/50/${i}
			./hashmap/hashmap -u50 -i${i}00000 -b1000 -r${i}00000 -d2000000 -n $t > out/50/${i}/${b}-${t}
		done
	done
done
